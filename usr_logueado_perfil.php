<section class="content">
<div class="row">
<div class="col-md-4">
<div class="box box-success">
<div class="box-body box-profile">
<img class="profile-user-img img-responsive img-circle" src="dist/img/usr_logueado.png" alt="User profile picture">
<h3 class="profile-username text-center">Mariano Pessina</h3>
<p class="text-muted text-center">Coordinador - Sistemas App Colaborativas</p>
<ul class="list-group list-group-unbordered">
<li class="list-group-item">
<b>Documento</b> <a class="pull-right">329873646</a>
</li>
<li class="list-group-item">
<b>Cuil</b> <a class="pull-right">20-329873646-9</a>
</li>
<li class="list-group-item">
<b>Legajo</b> <a class="pull-right">29587</a>
</li>
<li class="list-group-item">
<b>Puesto</b> <a class="pull-right">Coordinador de desarrollo</a>
</li>
<li class="list-group-item">
<b>Gerencia</b> <a class="pull-right">Sistemas</a>
</li>
</ul>
</div>
</div>
</div>
<div class="col-md-8">
<div class="nav-tabs-custom">
<ul class="nav nav-tabs">
<li class="active"><a href="#tab_seguridad" data-toggle="tab">Seguridad</a></li>
<li><a href="#tab_info_profecional" data-toggle="tab">Informacion profecional</a></li>
</ul>
<div class="tab-content">
<div class="active tab-pane" id="tab_seguridad">
<div class="post">
<h2 class="page-header" style="font-size: 1.3em;">
<i class="fa fa-check-square-o"></i> Mis Roles
<small class="pull-right">esto define los perfiles</small>
</h2>
<div class="row invoice-info">
<div class="col-sm-4 invoice-col">
<label><input type="checkbox" class="flat-red"> Psicologia </label><br>  <!--  or  -->
<label><input type="checkbox" class="flat-red"> Seguridad </label>
</div>
<div class="col-sm-4 invoice-col">
<label><input type="checkbox" class="flat-red"> Mediciana </label><br>
<label><input type="checkbox" class="flat-red"> Administrativo </label>
</div>
<div class="col-sm-4 invoice-col">
<label><input type="checkbox" class="flat-red"> Terapista </label><br>
<label><input type="checkbox" class="flat-red" checked="checked"> Administrador</label>
</div>
</div>
<br /><br />
<h2 class="page-header" style="font-size: 1.3em;">
<i class="fa fa-check-square-o"></i> Mis Permisos
<small class="pull-right">esto define que puedo ver y hacer</small>
</h2>
<select class="form-control select2" multiple="multiple" data-placeholder="seleccione el entecedente" disabled="disabled" style="width: 100%;">
<option selected="selected">Rol 11</option>
<option>Rol 2</option>
<option selected="selected">Rol 13</option>
<option selected="selected">Ejecucion de reporteria</option>
<option>Rol 5</option>
<option>Rol 6</option>
<option>Rol 7</option>
<option selected="selected">Rol 18</option>
<option>Rol 9</option>
<option>Rol 10</option>
<option>Rol 11</option>
<option>Rol 12</option>
<option>Rol 13</option>
<option selected="selected">alta de usuarios</option>
<option selected="selected">Rol 25</option>
<option selected="selected">alta de insumos</option>
<option selected="selected">Rol 27</option>
<option>Rol 18</option>
<option>Rol 19</option>
<option>Rol 20</option>
<option>Rol 21</option>
<option selected="selected">Rol 32</option>
<option>Rol 23</option>
<option selected="selected">administrador full back-end</option>
</select>
</div> <!-- FIN: [div class="post"] -->
</div> <!-- FIN: [div class="active tab-pane" id="tab_seguridad"] -->
<div class="tab-pane" id="tab_info_profecional">
<div class="box-body">
<strong><i class="fa fa-mortar-board margin-r-5"></i> Matricula</strong>
<p class="text-muted">
<input class="form-control input-sm pull-right"  value="M.N. 125251">
</p>
<hr>
<strong><i class="fa fa-key margin-r-5"></i> Firma digital </strong><a class="btn btn-danger btn-xs">VENCIDA!</a>
<p><input class="form-control input-sm pull-right"  value=""></p>
</div> <!-- [div class="box-body"] -->
</div> <!-- [div class="tab-pane" id="tab_info_profecional] -->
</div> <!-- FIN: [div class="tab-content"] -->
</div> <!-- FIN: [div class="nav-tabs-custom"] -->
</div> <!-- [div class="col-md-8"] -->
</div>
</section>
<script>
$(function () {
$(".select2").select2();
$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
checkboxClass: 'icheckbox_minimal-blue',
radioClass: 'iradio_minimal-blue'
});
$('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
checkboxClass: 'icheckbox_minimal-red',
radioClass: 'iradio_minimal-red'
});
$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
checkboxClass: 'icheckbox_flat-green',
radioClass: 'iradio_flat-green'
});
});
</script>