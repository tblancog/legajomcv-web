<div class="box box-solid">
  <div class="box-header with-border">
    <i class="fa fa-bell-o"></i>
    <h3 class="box-title" style="padding-right: 15px;">Alta SAP</h3>
    <small class="text-muted" style="padding-right: 7px;"><i class="fa fa-calendar"></i> dd/mm/yyyy </small>
    <small class="text-muted"><i class="fa fa-clock-o"></i> hh:mm </small>
    <div class="box-tools pull-right">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    </div> <!-- [div class="box-tools pull-right"] -->
  </div> <!-- [div class="box-header with-border"] -->
  <div class="box-body">
    <dl>
      <dt>Descripcion</dt>
      <dd>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis risus tristique, varius diam ut, maximus diam. Cras tincidunt molestie augue, a lacinia diam malesuada nec. Vivamus in elit non purus gravida iaculis non id nisi. Quisque vel nisi eu metus bibendum blandit. Nullam malesuada gravida magna, eget finibus orci dignissim non.</dd>
    </dl>
  </div> <!-- [div class="box-body"] -->
</div> <!-- [div class="box box-solid"] -->