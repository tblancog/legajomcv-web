<html>
<head>
</head>
	<body>
<?php
session_start();

include("config.php");

if($debugger==1){
echo 
	"<hr /><strong><u>Configuracion</u></strong><br />".
	"debugger: <strong>".$debugger."</strong><br />".
	"LoginSQL: <strong>".$LoginSQL."</strong><br />".
	"ClaveSQL: <strong>".$ClaveSQL."</strong><br />".
	"id_apps: <strong>".$id_apps."</strong><br />".
	"LoginAD: <strong>".$LoginAD."</strong><br />".
	"host: <strong>".$host."</strong><br />".
	"user: <strong>".$user."</strong><br />".
	"LdapSetOptionVersion: <strong>".$LdapSetOptionVersion."</strong><br />".
	"LdapSetOptionReferrals: <strong>".$LdapSetOptionReferrals."</strong><br />".
	"dn: <strong>".$dn."</strong><br />".
	"attrs: <strong>".$attrs."</strong><br />".
	"attr: <strong>".$attr."</strong><br />".
	"Captcha: <strong>".$Captcha."</strong><br />".
	"txtNomUsr: <strong>".$txtNomUsr."</strong><br />".
	"txtClaveUsr: <strong>".$txtClaveUsr."</strong><hr />"
;
} //FIN: if($debugger==1...

//CAPTCHA ------------------------------------------------------------------------------------------------------------->
if ($Captcha==true){
	if ($_POST['captcha'] == $_SESSION['cap_code']){
		//echo "CORRECTO";
		$captchaValidate==true;
	} else {
		if($debugger!=1){
			header('Location: ../index.php?Error_Msg=<strong>Usuario y/o contraseña invalidos</strong>:  Validador incorrecto');
			exit;
		}else{
			echo "<hr /><strong><u>CAPTCHA</u></strong><br />".
			"<strong>_SERVER['REQUEST_METHOD']: </strong>".$_SERVER['REQUEST_METHOD']."<br />".
			"<strong>_POST['captcha']: </strong>".$_POST['captcha']."<br />".
			"<strong>_SESSION['cap_code']: </strong>".$_SESSION['cap_code']."<br />".
			"header<Location: ../index.php?Error_Msg=<strong>Usuario y/o contraseña invalidos</strong>:  Validador incorrecto'>";
			exit;
		}//FIN: if($debugger!=1...
	}//FIN: if ($_POST['captcha'] == $_SESSION['cap_code']...
} //FIN: if ($Captcha==true...

//---------------------------------------------------------------------------------------------------------------------<

//Si el campo de nombre de usuario y/o contraseña esta vacio vuelve al login
if($txtNomUsr=="" or $txtClaveUsr==""){
	header('Location: ../index.php?Error_Msg=<strong>Usuario y/o contraseña invalidos</strong>: no pueden quedar campos vacios');
	exit;
} //FIN: if($txtNomUsr=="" or $txtClaveUsr=="...

if($LoginSQL==true){

	if($debugger==1){
		echo "<hr /><strong><u>Hago consulta SQL</u></strong><br />";		
	} //FIN: if($debugger==1...

	//Si la aplicacion requiere clave en SQL modifico la consulta ---->
	if($txtClaveUsr!="" and $ClaveSQL==true){
		$claveTrue="And
  consolidado.clave_conso = '$txtClaveUsr' ";
	}else{
		$claveTrue="";
	}
	//----------------------------------------------------------------<
	
	include_once("conecta.php");

	$link=conectLogin();//abre la conexion

	//Consulta que trae solo los ganadores del mes anterior al actual
	$query="
	Select consolidado.id_conso,
	  consolidado.id_usuario_conso,
	  consolidado.id_apps_conso,
	  consolidado.id_perfil_conso,
	  consolidado.estado_conso,
	  consolidado.clave_conso,
	  usuarios.id_usr,
	  usuarios.nombre_usr,
	  usuarios.estado_usr,
	  apps.id_apps,
	  apps.nombre_apps,
	  perfiles.id_perfil,
	  perfiles.nombre_perfil
	From consolidado Inner Join
	  usuarios On usuarios.id_usr = consolidado.id_usuario_conso Inner Join
	  apps On apps.id_apps = consolidado.id_apps_conso Inner Join
	  perfiles On perfiles.id_perfil = consolidado.id_perfil_conso
	Where
	(
	  consolidado.estado_conso = 'activo'
	And
	  usuarios.nombre_usr = '$txtNomUsr'
	And
	  usuarios.estado_usr = 'activo'
	And 
	  apps.id_apps = $id_apps
	$claveTrue
	);
	";

	if($debugger==1){
		echo "<br /><pre>".$query."</pre><br />";
	} //FIN: if($debugger==1...

	$rs = mysql_query($query, $link)
	or die("<br /><b>Error 3 en control.php al ejecutar la consulta</b><br />");

	if(@mysql_num_rows($rs)!=0){
		while ($row = mysql_fetch_array ($rs)){  
			$nombre_usr = $row["nombre_usr"];
			$nombre_perfil = $row["nombre_perfil"];
			$estado_conso = $row["estado_conso"];
			$estado_usr = $row["estado_usr"];
		} //FIN: while ($row = mysql_fetch_array ($rs...
	}else{
		header('Location: ../index.php?Error_Msg=<strong>Usuario y/o contraseña invalidos</strong>: usuario no encontrado en la base');
		exit;
	} //FIN: if(@mysql_num_rows($rs)!=0...

	mysql_free_result($rs);
	mysql_close($link);//cierra la conexion

	$sqlValidate=true;


	if($debugger==1){
		echo
			"<br />sqlValidate: <strong>".$sqlValidate."</strong><br />".
			"nombre_usr: <strong>".$nombre_usr."</strong><br />".
			"nombre_perfil: <strong>".$nombre_perfil."</strong><br />".
			"estado_conso: <strong>".$estado_conso."</strong><br />".
			"estado_usr: <strong>".$estado_usr."</strong><hr />"
		;
	} //FIN: if($debugger==1...

} //FIN: if($LoginSQL==true...

if($LoginAD==true){
	if($debugger==1){
		echo "<hr /><strong><u>Hago consulta AD</u></strong><br />";
	} //FIN: if($debugger==1...


//LDAP ------------------------------------------------------------------------>
	//Conecto al host
	$ad = ldap_connect($host) 
	or die("Imposible Conectar a ".$host);
	
	// Especifico la versión del protocolo LDAP
	//ldap_set_option($ad, LDAP_OPT_PROTOCOL_VERSION, $LdapSetOptionVersion)
	//or die ("Imposible asignar el Protocolo Version");

	//Defino el set de referencia
	//ldap_set_option($ad, LDAP_OPT_REFERRALS, $LdapSetOptionReferrals)
	//or die ("Imposible asignar el Protocolo Referrals");
	
	// Valido las credenciales para accesar al servidor LDAP
	$bd = ldap_bind($ad, $user, $txtClaveUsr)
	or die (header('Location: ../index.php?Error_Msg=<strong>Usuario y/o contraseña invalidos</strong>: usuario de dominio inexistente'));
	$adValidate=true;
	
/* 
// comparar el valor
$r=ldap_compare($ad, $dn, $attr, $txtClaveUsr);

echo 
"ad: <strong>ldap_connect(".$host.")</strong> <i>(".$ad.")</i><br />".
"bd: <strong>ldap_bind(ldap_connect(".$host."), ".$user.", ".$txtClaveUsr.")</strong> <i>(".$bd.")</i><br />".
"dn: ".$dn."<br />".
"r: <strong>ldap_compare(ldap_connect(".$host."), ".$dn.", ".$attr.", ".$txtClaveUsr.")</strong> <i>(".$r.")</i><br />"
;

if ($r === -1) {
	echo "Error: " . ldap_error($ad);
} elseif ($r === true) {
	echo "<br />CLAVE CORRECTO<br />";
	$adValidate=true
} elseif ($r === false) {
	echo "<br />CLAVE INCORRECTA<br />";
}	
*/
}

	if($debugger==1){
		echo
			"<hr />LoginAD: <strong>".$LoginAD."</strong><br />".
			"adValidate: <strong>".$adValidate."</strong><br />".
			"LoginSQL: <strong>".$LoginSQL."</strong><br />".
			"sqlValidate: <strong>".$sqlValidate."</strong><br />"
		;
	} //FIN: if($debugger==1...




if($adValidate==true or $sqlValidate==true){
	if($debugger==0){
		$_SESSION["sqlValidate"]=$sqlValidate;
		$_SESSION["adValidate"]=$adValidate;
		$_SESSION["nombreUsr"]=$nombre_usr;
		$_SESSION["nombrePerfil"]=$nombre_perfil;
		$_SESSION["estadoConso"]=$estado_conso;
		$_SESSION["estadoUsr"]=$estado_usr;
		header('Location: ../saludo_send.php');
		exit;
	}else{
		echo "AD y SQL ACTIVO Y VALIDADO";
		echo "header['Location: ../saludo_send.php']";
		exit;
	} //FIN: if($debugger==1...
} //FIN: if($adValidate==true or $sqlValidate==true...

?>
</body>
</html>
