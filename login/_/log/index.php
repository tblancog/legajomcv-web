<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cablevision - Foto Corporativa</title>
<script>
//Hace foco en el imput del nombre al cargar la pagina
window.onload = function() {document.getElementById('txtNomUsr').focus();}


function valida_envia(){
	//valido el nombre de usuario
	if (document.FormLogin.txtNomUsr.value.length==0){
		alert("Tiene que escribir un nombre de usuario \n Solo admite de la A-Z, a-z, y del 0-9")
		document.FormLogin.txtNomUsr.focus()
		return 0;
	}


	//valido el campo contraseña
	if (document.FormLogin.txtClaveUsr.value.length==0){
		alert("Tiene que escribir una contraseña")
		document.FormLogin.txtClaveUsr.focus()
		return 0;
	}	

	//el formulario se envia
	//alert("Muchas gracias por enviar el formulario");
	document.FormLogin.submit();

}

function validar(e,textbox){
	tecla = (document.all) ? e.keyCode : e.which;

	if (tecla==13){
		if (document.FormLogin.txtNomUsr.value.length==0){
			alert("Tiene que escribir un nombre de usuario \n Solo admite de la A-Z, a-z, y del 0-9")
			document.FormLogin.txtNomUsr.focus()
			return 0;
		}
		
		//valido el campo contraseña
		if (document.FormLogin.txtClaveUsr.value.length==0){
			alert("Tiene que escribir una contraseña")
			document.FormLogin.txtClaveUsr.focus()
			return 0;
		}	
		document.FormLogin.submit();
	}
}

</script>

<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>

<div class="centrarLogin">

        <?php 
		include("login/config.php");
		        //Destruyo las variables de session y muestro mensajes de error
        if($_GET["Error_Msg"]!=""){
            session_start();
            $_SESSION = array();
            session_destroy();
            echo "<div class='errorMsg'>".$_GET["Error_Msg"]."<br /></div>";
        }
        ?>

    <div class="bloque_titulo_apps">
        SALUDO POR ANTIGUEDAD
    </div>
    <div class="bloque_izq">
        <div class="textLogin">
            LOGIN&nbsp;&nbsp;
        </div> <!-- |div class="login"| -->
    </div> <!-- |div class="bloque_izq"| -->
    
    <div class="bloque_med">
        &nbsp;
    </div> <!-- |div class="bloque_med"| -->
    
    <div class="bloque_der">
    	<div class="objLogin">
        	<form id="FormLogin" name="FormLogin" method="POST" action="login/control.php">

                USUARIO <input name="txtNomUsr" type="text" id="txtNomUsr" size="30" class="textBoxA"   />
                <br />
                CONTRASEÑA <input name="txtClaveUsr" type="password" id="txtClaveUsr" size="30" class="textBoxB" onKeyPress="validar(event,'btnEntrar')" />
                <br />
				<?php if($Captcha==true){ ?>
                <input type="text" name="captcha" id="captcha" maxlength="6" size="6"/>
                <img src="login/captcha.php"/>
                <?php } //FIN:if($Captcha==true... ?>

				<div style=" padding:10px 5px 5px 300px;">
                <input  type="button" name="btnEntrar" id="btnEntrar" value="ENTRAR"  onClick="valida_envia()" class="botonEntrar"/>
				</div>
			</form>
    	</div> <!-- |div class="objLogin"| -->

  </div> <!-- |div class="bloque_der"| -->
	
    <div class="lineaHorizontal"></div>
    
    <!-- <div style="height:30px;">
        &nbsp;<hr style="color:#b3252a; margin-top:-15px;"  />&nbsp;
    </div> -->
    <div class="textoNota">
    <img src="sysImg/punto.png" width="12" height="12" />&nbsp;&nbsp;Ingresa con tu usuario y clave de windows </div>
    <br />
    <div class="textoNota">
        <img src="sysImg/punto.png" width="12" height="12" />&nbsp;&nbsp;NOTA: El usuario de Windows se bloqueará si ingresás erróneamente los datos 
más de tres veces consecutivas. En este caso comunicate con la mesa de ayuda centralizada
al interno 18797 o (011)5295-8797.
    </div>

</div> <!-- |div class="centrarLogin"| -->

</body>
</html>