<?php
include_once('config.php');
//tomo la variable de error
$msg_get_err=$_GET["err"];

// Inicializar la sesión.
session_start();

// Destruir todas las variables de sesión.
$_SESSION = array();

// destruyo la cookie de sesión. (¡Esto destruirá la sesión, y no la información de la sesión!)
if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}

session_destroy(); // destruyo la sesión.

header("Location: http://$page_login?err=$msg_get_err"); //vuelvo al login
?>
