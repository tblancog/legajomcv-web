<div class="modal fade" id="evento_nuevo" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
<!--
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Modal Header</h4>
</div>
<div class="modal-body">
<p>This is a large modal.</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div> -->
</div>
</div>
</div>
<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- Columna izquierda -->
    <div class="col-md-3">
      <!-- Profile Image -->
      <div class="box box-success">
        <div class="box-body box-profile">
          <img class="profile-user-img img-responsive img-circle" src="dist/img/usr_logueado.png" alt="User profile picture">
          <h3 class="profile-username text-center">Mariano Pessina</h3>
          <p class="text-muted text-center">Coordinador - Sistemas App Colaborativas</p>
          <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
              <b>Documento</b> <a class="pull-right">329873646</a>
            </li>
            <li class="list-group-item">
              <b>Cuil</b> <a class="pull-right">20-329873646-9</a>
            </li>
            <li class="list-group-item">
              <b>Legajo</b> <a class="pull-right">29587</a>
            </li>
            <li class="list-group-item">
              <b>Puesto</b> <a class="pull-right">Coordinador de desarrollo</a>
            </li>
            <li class="list-group-item">
              <b>Gerencia</b> <a class="pull-right">Sistemas</a>
            </li>
          </ul>
          <a href="events/evento_nuevo.php" data-toggle="modal" data-target="#evento_nuevo" class="btn btn-success btn-block"><b><i class="fa fa-plus"></i> Nuevo Evento</b></a>
          <a href="events/new_event.php" data-toggle="modal" data-target="#new_event" class="btn btn-success btn-block"><b><i class="fa fa-plus"></i> Dev Evento</b></a>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>  <!-- FIN: [div class="col-md-3"] -->
    <!-- Columna derecha -->
    <div class="col-md-9">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab_datos_personales" data-toggle="tab">Datos Personales</a></li>
          <li><a href="#tab_datos_Medicos" data-toggle="tab">Datos  Medicos</a></li>
          <li><a href="#tab_historial" data-toggle="tab">Historial Medico</a></li>
          <li><a href="#tab_examenes_medicos" data-toggle="tab">Examenes Medicos</a></li>
        </ul>
        <div class="tab-content">
          <!-- DATOS PERSONALES -->
          <div class="active tab-pane" id="tab_datos_personales">
            <div class="post">
              <div class="row invoice-info">
                <div class="col-sm-12 invoice-col">
                  <dl class="dl-horizontal">
                    <dt>Genero</dt>
                    <dd>Masculino</dd>
                    <dt>Fecha nacimiento</dt>
                    <dd>01/02/1979  (38 años)</dd>
                    <dt>Nacionalidad</dt>
                    <dd>Argentino</dd>
                    <dt>Estado Civil</dt>
                    <dd>Soltero</dd>
                    <dt style="margin-top: 7px !important; margin-bottom: 7px !important;">Direccion Personal</dt>
                    <dd>
                    <div class="box box-sm collapsed-box" style="border-top: 1px solid #ededed !important; width: 70% !important; margin-top: 7px !important;  margin-bottom: 7px !important;">
                        Coornelio Saavedra 360 P11
                        <div class="pull-right">
                          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                        </div>
                        <div class="box-body">
                          <div class="form-group margin-bottom-none">
                            <div class="col-sm-9">
                              <input type="text" class="form-control input-sm" placeholder="direccion alternativa">
                            </div>
                            <div class="col-sm-3">
                              <button type="submit" class="btn btn-danger pull-right btn-block btn-sm"><i class="fa fa-plus"></i> Agregar</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </dd>
                    <dd>Viamonte 349 <small><span class="text-red"> alternativa</span></small></dd>
                    <dt style="margin-top: 7px !important; margin-bottom: 7px !important;">Telefono  Personal</dt>
                    <dd>
                      <div class="box box-sm collapsed-box" style="border-top: 1px solid #ededed !important; width: 70% !important; margin-top: 7px !important;  margin-bottom: 7px !important;">
                        +54911-3690-8644
                        <div class="pull-right">
                          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                        </div>
                        <div class="box-body">
                          <div class="form-group margin-bottom-none">
                            <div class="col-sm-9">
                              <input type="text" class="form-control input-sm" placeholder="direccion alternativa">
                            </div>
                            <div class="col-sm-3">
                              <button type="submit" class="btn btn-danger pull-right btn-block btn-sm"><i class="fa fa-plus"></i> Agregar</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </dd>
                    <dd>4313-2410 <small><span class="text-red"> alternativa</span></small></dd>
                    <dd>15-59005925 <small><span class="text-red"> alternativa</span></small></dd>
                    <dd>40015728 <small><span class="text-red"> alternativa</span></small></dd>

                    <dt>ART</dt>
                    <dd>La Meridional</dd>
                    <dt>Empresa</dt>
                    <dd>Cablevision</dd>
                  </dl>
                  <hr />
                  <dl class="dl-horizontal">
                    <dt>Fecha Ingreso</dt>
                    <dd>01/01/2001</dd>
                    <dt>Area-Perfil</dt>
                    <dd>Aplicaciones Colaborativas</dd>
                    <dt>Jefe Directo</dt>
                    <dd>Fernando Zarate</dd>
                    <dt>Referente RRHH</dt>
                    <dd>Jorjelina Chiriello</dd>
                    <dt>Encuadre</dt>
                    <dd>Fuera de Convenio</dd>
                    <dt>Direccion Laboral</dt>
                    <dd>General Hornos 690</dd>
                    <dt>Telefono Laboral</dt>
                    <dd>15728</dd>
                  </dl>
                  <hr />
                  <dl class="dl-horizontal">
                    <dt>Hijos (2)</dt>
                    <dd>Gonzalo Pessina</dd>
                    <dd>Vade Retro Satana Pessina</dd>
                    <dt>Carga de Familia</dt>
                    <dd>si</dd>
                  </dl>
                </div> <!-- FIN: [div class="col-sm-12 invoice-col] -->
              </div> <!-- FIN: [div class="row invoice-info] -->
            </div> <!-- FIN: [div class="post"] -->
          </div> <!-- FIN: [div class="active tab-pane" id="tab_datos_personales"] -->
          <!-- DATOS MEDICOS -->
          <div class="tab-pane" id="tab_datos_Medicos">
            <div class="post">
              <div class="row invoice-info">
                <div class="col-sm-12 invoice-col">
                  <form>
                    <div class="row">
                      <div class="col-xs-6">
                        <dl class="dl-horizontal">
                          <dt>Grupo Sanguineo</dt>
                          <dd>
                            <select>
                              <option>seleccione</option>
                              <option>O-</option>
                              <option>O+</option>
                              <option>A−</option>
                              <option>A+</option>
                              <option>B−</option>
                              <option>B+</option>
                              <option>B+</option>
                              <option>AB−</option>
                              <option>AB+</option>
                            </select>
                          </dd><br />
                          <dt>Miembro SuperiorHabil</dt>
                          <dd>
                            <select>
                              <option>seleccione</option>
                              <option>Diestro</option>
                              <option>Zurdo</option>
                              <option>Ambidiestro</option>
                            </select>
                          </dd><br />
                          <dt>Peso</dt>
                          <dd>
                            <input type="number"  min="30" max="400" step="1"  value="0">,
                            <input type="number"  min="0" max="99" step="1"  value="0">kg
                          </dd>
                        </dl>
                      </div> <!-- [div class="col-xs-6"] -->
                      <div class="col-xs-6">
                        <dl class="dl-horizontal">
                          <dt>Dador de Sangre</dt>
                          <dd>
                            <select>
                              <option>seleccione</option>
                              <option>SI</option>
                              <option>NO</option>
                            </select>
                          </dd><br />
                          <dt>Donante de organos</dt>
                          <dd>
                            <select>
                              <option>seleccione</option>
                              <option>SI</option>
                              <option>NO</option>
                            </select>
                          </dd><br />
                          <dt>Altura</dt>
                          <dd>
                            <input type="number"  min="1" max="2"  step="1" value="0">,
                            <input type="number"  min="0" max="99" step="1" value="0">mt
                          </dd>
                        </dl>
                      </div> <!-- [div class="col-xs-6"] -->
                    </div><!-- [div class="row"] -->
                    <hr />
                    <div class="row">
                      <div class="col-xs-4">
                        <dl class="dl-horizontal">
                          <dt>Cert. Discapacidad</dt>
                          <dd>
                            <select>
                              <option>seleccione</option>
                              <option>SI</option>
                              <option>NO</option>
                            </select>
                          </dd>
                        </dl>
                      </div> <!-- [div class="col-xs-4"] -->
                      <div class="col-xs-8">
                        <dl class="dl-horizontal">
                          <dt>Tipo Discapacidad</dt>
                          <dd>
                            <select class="form-control select2" multiple="multiple" data-placeholder="seleccione..." style="width: 100%;">
                              <option>Dificiencia fisica de origen motor</option>
                              <option>Dificiencia fisica de origen viseral</option>
                              <option>Dificiencia sensorial de origen visual</option>
                            </select>
                          </dd>
                        </dl>
                      </div> <!-- [div class="col-xs-8"] -->
                    </div><!-- [div class="row"] -->
                    <hr />
                    <div class="row">
                      <div class="col-xs-6">
                        <dl class="dl-horizontal">
                          <dt>Exposicion riesgos laborales</dt>
                          <dd>
                            <select>
                              <option>seleccione</option>
                              <option>SI</option>
                              <option>NO</option>
                            </select>
                          </dd><br />
                          <dt>Codigo de Puesto</dt>
                          <dd><input type="text" class="form-control" placeholder="" value="456789009"></dd>
                        </dl>
                      </div> <!-- [div class="col-xs-6"] -->
                    </div><!-- [div class="row"] -->
                  </form>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-danger pull-right"><i class="fa fa-edit"></i> Actualizar</button>
                    </div>
                  </div>
                </div> <!-- FIN: [div class="col-sm-12 invoice-col] -->
              </div> <!-- FIN: [div class="row invoice-info] -->
            </div> <!-- FIN: [div class="post"] -->
          </div> <!-- FIN: [div class="active tab-pane" id="tab_datos_Medicos"] -->
          <!-- HISTORIAL MEDICO -->
          <div class="tab-pane" id="tab_historial">
            <div class="post">
              <div class="row invoice-info">
                <div class="col-sm-9 invoice-col">
                  <ul class="timeline timeline-inverse">
                    <li class="time-label">
                      <span class="bg-yellow">5 Feb. 2017</span>
                    </li>
                    <li>
                      <i class="fa fa-calendar-check-o bg-red"></i>
                      <div class="timeline-item">
                        <span class="time"><i class="fa fa-clock-o"></i> 15:25</span>
                        <h3 class="timeline-header"><a href="#">Proxima Consulta</a> cargada por Dr. Anastacio Medicoso</h3>
                        <div class="timeline-body">
                          Etsy doostang zoodles disqjknkb khkldaje waihdila hoowqeq... <a class="pull-right">Abrir Consulta</a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <i class="fa fa-medkit bg-purple"></i>
                      <div class="timeline-item">
                        <span class="time"><i class="fa fa-clock-o"></i> 13:00</span>
                        <h3 class="timeline-header"><a href="#">Derivacion</a> cargada por Dr. Anastacio Medicoso</h3>
                        <div class="timeline-body">
                          Etsy doostang zoodles disqjknkb khkh ihkhlkjhoo jie... <a class="pull-right">Abrir Consulta</a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <i class="fa fa-user-md bg-green"></i>
                      <div class="timeline-item">
                        <span class="time"><i class="fa fa-clock-o"></i> 12:15</span>
                        <h3 class="timeline-header"><a href="#">Consulta</a> cargada por Dr. Anastacio Medicoso</h3>
                        <div class="timeline-body">
                          Etsy doostang zoodwf 3223jk h ihkhl hhhoius lihoo... <a class="pull-right">Abrir Consulta</a>
                        </div>
                      </div>
                    </li>
                    <li class="time-label">
                      <span class="bg-yellow">3 Febrero 2017</span>
                    </li>
                    <li>
                      <i class="fa fa-ambulance bg-maroon"></i>
                      <div class="timeline-item">
                        <span class="time"><i class="fa fa-clock-o"></i> 9:30</span>
                        <h3 class="timeline-header"><a href="#">Medico a Domicilio</a> cargada por Adrian Tadminis</h3>
                        <div class="timeline-body">
                          Etsy doostang zoodles disqjknkb khkh ihkhl hhhaea jhadguaww dadjhe  oo... <a class="pull-right">Abrir Consulta</a>
                        </div>
                      </div>
                    </li>
                    <li class="time-label">
                      <span class="bg-yellow">17 Noviembre 2016</span>
                    </li>
                    <li>
                      <i class="fa fa-hospital-o bg-aqua"></i>
                      <div class="timeline-item">
                        <span class="time"><i class="fa fa-clock-o"></i> 9:30</span>
                        <h3 class="timeline-header"><a href="#">Visita Externa</a> cargada por Dr. Victorina Pharma</h3>
                        <div class="timeline-body">
                          Etsy doostang zoodvsfsnkb khkh ihkafs lihoo... <a class="pull-right">Abrir Consulta</a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <i class="fa fa-clock-o bg-gray"></i>
                    </li>
                  </ul>
                </div> <!-- FIN: [div class="col-md-9"] -->
                <div class="col-sm-3 invoice-col">
                  <div class="box box-default box-solid">
                    <div class="box-header with-border">
                      <h3 class="box-title">Filtros</h3>
                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                      </div> <!-- FIN:[div class="box-tools pull-right"] -->
                    </div> <!-- FIN:[div class="box-header with-border"] -->
                    <div class="box-body">
                      <label><input type="checkbox" class="flat-red" checked> Proxima Consulta</label><br>  <!-- diseable  or  checked  -->
                      <label><input type="checkbox" class="flat-red" checked> Derivacion</label><br>
                      <label><input type="checkbox" class="flat-red" checked> Consulta</label><br>
                      <label><input type="checkbox" class="flat-red" checked> Medico a domicilio</label><br>
                      <label><input type="checkbox" class="flat-red" checked> Visita Externa</label>
                    </div> <!-- FIN:[div class="box-body"] -->
                  </div> <!-- FIN:[div class="box box-default collapsed-box] -->
                </div> <!-- FIN: [div class="col-md-3"] -->
              </div> <!-- FIN: [div class="row invoice-info] -->
            </div> <!-- FIN: [div class="post"] -->
          </div> <!-- [div class="tab-pane" id="tab_historial"] -->
          <!-- EXAMENES MEDICOS -->
          <div class="tab-pane" id="tab_examenes_medicos">
            <form class="form-horizontal">
              <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Tipo de examen</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="inputName" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="inputExperience" class="col-sm-2 control-label">Detalle sobre archivo</label>
                <div class="col-sm-10">
                  <textarea class="form-control" id="inputExperience" placeholder=""></textarea>
                </div>
              </div>
              <div class="form-group">
                <label for="inputSkills" class="col-sm-2 control-label">Subir archivo</label>
                <div class="col-sm-10">
                  <input type="file" class="form-control" id="inputSkills" placeholder="Skills">
                </div>
              </div>
              <div class="form-group">
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-danger pull-right"><i class="fa fa-upload"></i> Subir</button>
                </div>
              </div>
            </form>
          </div> <!-- FIN:[div class="tab-pane" id="tab_examenes_medicos"] -->
        </div> <!-- FIN:[div class="tab-content"] -->
      </div> <!-- FIN:[div class="nav-tabs-custom"] -->
    </div> <!-- FIN:[div class="col-md-9"] -->
    <div class="col-sm-12">
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">Informacion Importante</h3>
        </div>
        <div class="box-body">
          <strong><i class="fa fa-heartbeat margin-r-5"></i> Grupos de Riesgo</strong>
          <p class="text-muted">
            <ul>
              <li>Cardiovascular</li>
            </ul>
          </p>
          <hr>
          <strong><i class="fa fa-eyedropper margin-r-5"></i> Vacunas</strong>
          <p class="text-muted">
            <ul>
              <li>Fiebre Amarilla</li>
            </ul>
          </p>
          <hr>
          <strong><i class="fa fa-book margin-r-5"></i> Antecedentes Medicos</strong>
          <p class="text-muted">
            <ul>
              <li>Presion Arterial</li>
              <li>Hipercolesterolemia</li>
              <li>Hipertrigliseridemia</li>
            </ul>
          </p>
        </div>
      </div>
    </div>
  </div>
</section>
<script>
  $(function () {
    $(".select2").select2();
    $('#evento_nuevo').on('hide.bs.modal', function () {
      $('#evento_nuevo').removeData();
    })
//iCheck for checkbox and radio inputs
$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
  checkboxClass: 'icheckbox_minimal-blue',
  radioClass: 'iradio_minimal-blue'
});
//Red color scheme for iCheck
$('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
  checkboxClass: 'icheckbox_minimal-red',
  radioClass: 'iradio_minimal-red'
});
//Flat red color scheme for iCheck
$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
  checkboxClass: 'icheckbox_flat-green',
  radioClass: 'iradio_flat-green'
});
});
</script>
