<!-- Ruta -->
<section class="content-header">
  <h1>Home<small></small></h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
    <li class="active">Agenda</li>
  </ol>
</section>

<!-- Main content -->
<section class="invoice">
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <i class="fa fa-calendar"></i> Agenda
        <small class="pull-right">Fecha: <?php echo date("d/m/Y"); ?></small>
      </h2>
    </div>
  </div>
  <div class="row invoice-info">


    <div class="alert alert-warning alert-dismissible no-shadow">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>


    <h4 class="text-red"><i class="icon fa fa-warning"></i> Atencion!</h4>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis risus tristique, varius diam ut, maximus diam. Cras tincidunt molestie augue, a lacinia diam malesuada nec. Vivamus in elit non purus gravida iaculis non id nisi. Quisque vel nisi eu metus bibendum blandit. Nullam malesuada gravida magna, eget finibus orci dignissim non.
    </div>

    <div class="col-sm-8 invoice-col">
      <section class="content">
        <div class="row">
          <div class="col-md-12">
            <ul class="timeline">
              <li class="time-label">
                <span class="bg-yellow">
                  10 Mayo 2017
                </span>
              </li>
              <li>
                <i class="fa fa-user-md bg-green"></i>
                <div class="timeline-item">
                  <span class="time"><i class="fa fa-clock-o"></i> 17:00</span>
                  <h3 class="timeline-header"><a href="#">Conosulta realizada</a> a Rosa Panzeri</h3>
                  <div class="timeline-body">
                    Etsy doostang zoodles disqjknkb khkh ihkhl hhhaea jhadguaww dadjhe  oo... <a class="pull-right">Abrir Consulta</a>
                  </div>
                </div>
              </li>
              <li>
                <i class="fa fa-calendar-check-o bg-red"></i>
                <div class="timeline-item">
                  <span class="time"><i class="fa fa-clock-o"></i> 16:30</span>
                  <h3 class="timeline-header"><a href="#">Consulta programada</a> para Mariano Blanco</h3>
                  <div class="timeline-body">
                    Etsy doostang zoodles disqjknkb khkh ihkhl hhhaea jhadguaww dadjhe  oo... <a class="pull-right">Abrir Consulta</a>
                  </div>
                </div>
              </li>
              <li>
                <i class="fa fa-user-md bg-green"></i>
                <div class="timeline-item">
                  <span class="time"><i class="fa fa-clock-o"></i> 15:00</span>
                  <h3 class="timeline-header"><a href="#">Conosulta realizada</a> a Alejo Sanfilipo</h3>
                  <div class="timeline-body">
                    Etsy doostang zoodles disqjknkb khkh ihkhl hhhaea jhadguaww dadjhe  oo... <a class="pull-right">Abrir Consulta</a>
                  </div>
                </div>
              </li>
              <li class="time-label">
                <span class="bg-yellow">
                  9 Mayo 2017
                </span>
              </li>
              <li>
                <i class="fa fa-user-md bg-green"></i>
                <div class="timeline-item">
                  <span class="time"><i class="fa fa-clock-o"></i> 19:00</span>
                  <h3 class="timeline-header"><a href="#">Conosulta realizada</a> a Maximiliano Pessina</h3>
                  <div class="timeline-body">
                    Nulla at nibh non metus effper pulvinar quis quis quam.
                  </div>
                  <div class="timeline-footer">
                    <a class="btn btn-primary btn-xs">Abrir legajo</a>
                  </div>
                </div>
              </li>
              <li>
                <i class="fa fa-clock-o bg-gray"></i>
              </li>
            </ul>
          </div>
        </div>
      </section>
    </div>
    <div class="col-sm-4 invoice-col">
          <div class="box box-success">
            <div class="box-header">
              <h3 class="box-title">Filtros</h3>
            </div>
            <div class="box-body">
              <!-- CALENDARIO -->
              <div class="input-group">
                <button type="button" class="btn btn-default pull-right" id="daterange-btn">
                  <span>
                    <i class="fa fa-calendar"></i> intervalo de fechas
                  </span>
                  <i class="fa fa-caret-down"></i>
                </button>
              </div>
              <hr />
              <!-- FILTROS -->
              <div class="input-group">
                <label><input type="checkbox" class="flat-red" checked> Proxima Consulta</label><br>  <!-- diseable  or  checked  -->
                <label><input type="checkbox" class="flat-red" checked> Derivacion</label><br>
                <label><input type="checkbox" class="flat-red" checked> Consulta</label><br>
                <label><input type="checkbox" class="flat-red" checked> Medico a domicilio</label><br>
                <label><input type="checkbox" class="flat-red" checked> Visita Externa</label>
              </div>
            </div>
          </div>
    </div>
  </section> <!-- [section class="invoice"] -->
  <div class="clearfix"></div>
