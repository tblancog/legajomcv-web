<div class="modal fade" id="suscripcion_alertas_ver" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content"> 
<!-- 
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Modal Header</h4>
</div>
<div class="modal-body">
<p>This is a large modal.</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div> -->
</div>
</div>
</div> 
<section class="content-header">
	<h1>
		<i class="fa fa-bell"></i> Suscripcion de alertas
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="#">alertas</a></li>
		<li class="active">suscripcion</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title pull-right">Informacion obtenida a las <?php echo date("H:i:s"); ?></h3><br /><br />
					<div class="col-md-3"><label><input type="checkbox" class="flat-red" checked> Articulo 211</label></div>
					<div class="col-md-3"><label><input type="checkbox" class="flat-red" checked> Altas SAP</label></div>
					<div class="col-md-3"><label><input type="checkbox" class="flat-red" checked> Bajas SAP</label></div>
					<div class="col-md-3"><label><input type="checkbox" class="flat-red" checked> Derivaciones</label></div>
				</div>
				<div class="box-body" id="boxBody">
					<table id="t_appUsr" class="table table-bordered table-hover"> <!--  table-striped / table-hover -->
						<thead>
							<tr>
								<th>Tipo</th>
								<th>Hora</th>
								<th>Fecha</th>
								<th>Descripcion</th>
								<th>Estado</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<a href='suscripcion_alertas_ver.php' data-toggle='modal' data-target='#suscripcion_alertas_ver'>Articulo 211</a>
								</td>
								<td>dd/mm/yyyy</td>
								<td>hh:mm</td>
								<td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis risus tristique, varius diam ut, maximus diam. Cras tincidunt molestie augue, a lacinia diam</td>
								<td><span class="badge bg-red">IMPORTANTE</span></td>
							</tr>
							<tr>
								<td>
									<a href='suscripcion_alertas_ver.php' data-toggle='modal' data-target='#suscripcion_alertas_ver'>Bajas SAP</a>
								</td>
								<td>dd/mm/yyyy</td>
								<td>hh:mm</td>
								<td>Nullam malesuada gravida magna, eget finibus orci dignissim non. Cras tincidunt molestie augue, a lacinia diam</td>
								<td><span class="badge bg-yellow">PENDIENTE</span></td>
							</tr>
							<tr>
								<td>
									<a href='suscripcion_alertas_ver.php' data-toggle='modal' data-target='#suscripcion_alertas_ver'>Alta SAP</a>
								</td>
								<td>dd/mm/yyyy</td>
								<td>hh:mm</td>
								<td> Nulla quis risus tristique lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis risus tristique, varius diam ut, maximus diam. Cras tincidunt molestie augue, a lacinia diam idunt molestie augue</td>
								<td><span class="badge bg-green">LEIDA</span></td>
							</tr>
							<tr>
								<td>
									<a href='suscripcion_alertas_ver.php' data-toggle='modal' data-target='#suscripcion_alertas_ver'>Derivacion</a>
								</td>
								<td>dd/mm/yyyy</td>
								<td>hh:mm</td>
								<td>Nulla quis risus tristique, varius diam ut, maximus diam. Cras tincidunt molestie augue, a lacinia diam</td>
								<td><span class="badge bg-light-blue">AVISO</span></td>
							</tr>
						</tbody>
					</table>
					<script>
						$(function () {
							$("#t_appUsr").DataTable();
							/*$('#example2').DataTable({
							"paging": true,
							"lengthChange": false,
							"searching": false,
							"ordering": true,
							"info": true,
							"autoWidth": false
							});*/
						});
					</script>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	$('#suscripcion_alertas_ver').on('hide.bs.modal', function () {
		$('#suscripcion_alertas_ver').removeData();
	})
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$.ajaxSetup({
			beforeSend:function(){
				$("#loader").show();
			},
			success:function(res){
				$("#loader").hide();
			}
		}); 
		$("#_home").click(function(event) {
			$("#contentWrapper").load('home.php'); 
		});
	});
</script>
<script>
	$(function () {
		$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
			checkboxClass: 'icheckbox_minimal-blue',
			radioClass: 'iradio_minimal-blue'
		});
		$('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
			checkboxClass: 'icheckbox_minimal-red',
			radioClass: 'iradio_minimal-red'
		});
		$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
			checkboxClass: 'icheckbox_flat-green',
			radioClass: 'iradio_flat-green'
		});
	});
</script>