<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Atencion!<small class="pull-right alert-danger ">¡ES MUY IMPORTANTE QUE ANTES DE PROCEDER HAYA CORROBORADO LOS DATOS!</small></h4>
        El dato buscado no coincidio con ningun empleado de la nomina de SAP, ni de personal ya cargado en este sistema.<br /><br />
        <center><strong>¡Por favor corrobore los datos e intente nuevamente!</strong></center><br />
        Si se trata de un empleado tercerizado, un postulante o un ingreso que aun no se dio de alta en SAP y corroboro que los datos ingresados son correctos, debe generar el alta del mismo mediante el formulario que encontrara a continuacion.<br />
      </div>
      <div class="box">
        <div class="box-header with-border">
          <div class="col-md-4">
            <div class="alta-user">
              <h3 class="box-title">Alta de </h3>
              <select class="form-control">
                <option selected="">selecciona</option>
                <option>Empleado externo</option>
                <option>Postulante</option>
              </select>
            </div>
          </div>
        </div>
        <form id="alta_usrs" method="post" class="form-horizontal" action="ajaxSubmit.php">
          <div class="box-body">
            <div class="col-md-4">
              <div class="box box-gray">
                <div class="box-body box-profile">
                  <div class="form-group">
                    <strong>Documento</strong>
                    <div class="input-group">
                      <input type="text" name="documento" class="form-control" placeholder="ingrese documento">
                      <span class="input-group-btn">
                        <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                      </span>
                    </div>
                  </div> <!-- [div class="form-group"] -->
                  <div  class="form-group">
                    <strong>Nombres</strong>
                    <div class="text-muted"><input  type="text" name="nombres" class="form-control input-sm" value="" placeholder="ingrese nombre"></div>
                  </div>
                  <div  class="form-group">
                    <strong>Apellidos</strong>
                    <div class="text-muted"><input type="text" name="apellidos" class="form-control input-sm" value="" placeholder="ingrese apellido">
                    </div>
                  </div>
                  <div  class="form-group">
                    <strong>Sector</strong>
                    <div class="text-muted"><input type="text" name="sector" class="form-control input-sm" value="" placeholder="ingrese sector">
                    </div>
                  </div>
                  <div  class="form-group">
                    <strong>Puesto</strong>
                    <div class="text-muted"><input type="text" name="puesto" class="form-control input-sm" value="" placeholder="ingrese puesto"></div>
                  </div>
                  <div  class="form-group">
                    <strong>Gerencia</strong>
                    <div class="text-muted"><input type="text" name="gerencia" class="form-control input-sm" value="" placeholder="ingrese gerencia"></div>
                  </div>
                </div> <!-- [div class="box-body box-profile"] --> 
              </div> <!-- [div class="box box-success"] -->
            </div> <!-- [div class="col-md-4"] -->
            <div class="col-md-8">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_examenes_medicos" data-toggle="tab">Examenes Medicos</a></li>
                </ul>
                <div class="tab-content">
                  <!-- EXAMENES MEDICOS -->
                  <div class="active tab-pane" id="tab_examenes_medicos">
                    <form class="form-horizontal">
                      <label for="inputName" class="col-sm-2 control-label">Tipo de examen</label>
                      <div class="form-group">
                        <div class="col-sm-10">
                          <select name="examen" class="form-control">
                            <option>seleccione</option>
                            <option>Preocupacional</option>
                            <option>otro</option>
                          </select>
                          <input type="text" class="form-control" id="inputName" placeholder="">
                        </div>
                      </div>
                      <label for="inputExperience" class="col-sm-2 control-label">Detalle sobre archivo</label>
                      <div class="form-group">
                        <div class="col-sm-10">
                          <textarea name="detalle" class="form-control" id="inputExperience" placeholder=""></textarea>
                        </div>
                      </div>
                      <label for="inputSkills" class="col-sm-2 control-label">Subir archivo</label>
                      <div class="form-group">
                        <div class="col-sm-10">
                          <input name="file" type="file" class="form-control" id="inputSkills" placeholder="Skills">
                        </div>
                      </div>
                    </form>
                  </div> <!-- FIN:[div class="tab-pane" id="tab_examenes_medicos"] -->
                </div> <!-- FIN:[div class="tab-content"] -->
              </div> <!-- FIN:[div class="nav-tabs-custom"] -->
            </div> <!-- FIN:[div class="col-md-8"] -->
            <div class="col-md-12">
              <div class="pull-right">
                <button type="submit" class="btn btn-block btn-sm btn-danger"><i class="fa fa-plus"></i>  Guardar</button>
              </div> <!-- [div class="pull-right"] -->
            </div> <!-- [div class="col-md-12"] -->
          </div>  <!-- [div class="box-body"] -->
        </form>
      </div>  <!-- [div class="box box-success"] --> 
    </section>  <!-- [section class="content"] -->
  </div>  <!-- [div class="row"] -->
</div>  <!-- [div class="col-md-12"] -->
<script type="text/javascript">
  $(document).ready(function() {
    $('#alta_usrs')
    .bootstrapValidator({
      message: 'Valor invalido',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        documento: {
          message: 'Dato invalido!',
          validators: {
            notEmpty: {
              message: 'El campo no puede estar vacio'
            },
            stringLength: {
              min: 7,
              max: 10,
              message: 'Minimo 7 maximo 10 caracteres'
            },
            regexp: {
              regexp: /^[0-9]+$/,
              message: 'Solo se permiten numeros sin espacios ni puntos'
            }
          }
        },
        nombres: {
          message: 'Dato invalido!',
          validators: {
            notEmpty: {
              message: 'El campo no puede estar vacio'
            },
            stringLength: {
              min: 3,
              max: 70,
              message: 'Minimo 3 maximo 70 caracteres'
            },
            regexp: {
              regexp: /^[a-zA-Z\ ]+$/,
              message: 'Solo se caracteres alfanumericos y espacios'
            }
          }
        },
        apellidos: {
          message: 'Dato invalido!',
          validators: {
            notEmpty: {
              message: 'El campo no puede estar vacio'
            },
            stringLength: {
              min: 3,
              max: 70,
              message: 'Minimo 3 maximo 70 caracteres'
            },
            regexp: {
              regexp: /^[a-zA-Z\ ]+$/,
              message: 'Solo se caracteres alfanumericos y espacios'
            }
          }
        },
        sector: {
          message: 'Dato invalido!',
          validators: {
            notEmpty: {
              message: 'El campo no puede estar vacio'
            },
            stringLength: {
              min: 3,
              max: 25,
              message: 'Minimo 3 maximo 25 caracteres'
            },
            regexp: {
              regexp: /^[a-zA-Z\ ]+$/,
              message: 'Solo se caracteres alfanumericos y espacios'
            }
          }
        },
        puesto: {
          message: 'Dato invalido!',
          validators: {
            notEmpty: {
              message: 'El campo no puede estar vacio'
            },
            stringLength: {
              min: 3,
              max: 25,
              message: 'Minimo 3 maximo 25 caracteres'
            },
            regexp: {
              regexp: /^[a-zA-Z\ ]+$/,
              message: 'Solo se caracteres alfanumericos y espacios'
            }
          }
        },
        gerencia: {
          message: 'Dato invalido!',
          validators: {
            notEmpty: {
              message: 'El campo no puede estar vacio'
            },
            stringLength: {
              min: 3,
              max: 25,
              message: 'Minimo 3 maximo 25 caracteres'
            },
            regexp: {
              regexp: /^[a-zA-Z\ ]+$/,
              message: 'Solo se caracteres alfanumericos y espacios'
            }
          }
        },
        detalle: {
          message: 'Dato invalido!',
          validators: {
            notEmpty: {
              message: 'El campo no puede estar vacio'
            },
            stringLength: {
              min: 3,
              max: 600,
              message: 'Minimo 3 maximo 600 caracteres'
            },
            regexp: {
              regexp: /^[a-zA-Z0-9\ \*\.\-\_]+$/,
              message: 'Solo se permiten letras, numeros, espacios, guion bajo, medio, asteriscos y puntos'
            }
          }
        }
      }
    })
  .on('success.form.bv', function(e) {
    e.preventDefault(); // Prevent form submission
    var form = $(e.target);
    var bv = form.data('bootstrapValidator'); // Get the BootstrapValidator instance
    var datos = form.serialize();
    var url = form.attr('action');
    var result = $.ajax({url:  url,   
      dataType: "json",
      method:"post",
      data:datos});
    result.then(
      function (result) {
        if (result.res === true) {
          //alert("ok");
          $("#close_modal").click(); // cierro el modal
          //$("#loader").hide(); //oculto el loader
          // actualizo datatable
          $("#alert_ok").show(500,"swing"); //muestro msg [.show([duracion],[efecto]) "linear", "swing", "_default"]
          $("#msg-custom").html(result.msg); 
        } else {
          //alert("error");
          $("#alert_error").show(500,"swing"); //muestro msg [.show([duracion],[efecto]) "linear", "swing", "_default"]
          $("#loader").hide(); //oculto el loader
          // los mensajes vienen en el campo result.msg
        }
      },
      function (err) {
        alert("error 2");
        $("#alert_error").show(500,"swing"); //muestro msg [.show([duracion],[efecto]) "linear", "swing", "_default"]
        $("#loader").hide(); //oculto el loader
      }
    );
  });
});
</script>