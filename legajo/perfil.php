

<div class="modal fade" id="evento_nuevo" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
<!--
<div class="modal-header">Proxi
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Modal Header</h4>
</div>
<div class="modal-body">
<p>This is a large modal.</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div> -->
</div>
</div>
</div>
<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- Columna izquierda -->
    <div class="col-md-3">
      <!-- Profile Image -->
      <div class="box box-success">
        <div class="box-body box-profile">
          <img class="profile-user-img img-responsive img-circle" src="dist/img/usr_logueado.png" alt="User profile picture">
          <h3 class="profile-username text-center"><span id='portada_nombre_apellido'></span></h3>
          <p class="text-muted text-center">Coordinador - Sistemas App Colaborativas</p>
          <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
              <b>Documento</b> <a class="pull-right"><span id='portada_documento'></span></a>
            </li>
            <li class="list-group-item">
              <b>Cuil</b> <a class="pull-right"><span id='portada_cuil'></span></a>
            </li>
            <li class="list-group-item">
              <b>Legajo</b> <a class="pull-right"><span id='portada_legajo'></span></a>
            </li>
            <li class="list-group-item">
              <b>Puesto</b> <a class="pull-right"><span id='portada_puesto'></span></a>
            </li>
            <li class="list-group-item">
              <b>Gerencia</b> <a class="pull-right"><span id='portada_gerencia'></span></a>
            </li>
          </ul>
          <a href="evento_nuevo.php" data-toggle="modal" data-target="#evento_nuevo" class="btn btn-success btn-block"><b><i class="fa fa-plus"></i> Nuevo Evento</b></a>
        </div>
        <!-- /.box-body -->
      </div>



      
      <!-- /.box -->
    </div>  <!-- FIN: [div class="col-md-3"] -->
    <!-- Columna derecha -->
    <div class="col-md-9">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab_datos_personales" data-toggle="tab">Datos Personales</a></li>
          <li><a href="#tab_datos_Medicos" data-toggle="tab">Datos  Medicos</a></li>
          <li><a href="#tab_historial" data-toggle="tab">Historial Medico</a></li>
          <li><a href="#tab_examenes_medicos" data-toggle="tab">Examenes Medicos</a></li>
        </ul>
        <div class="tab-content">
          <!-- DATOS PERSONALES -->
          <div class="active tab-pane" id="tab_datos_personales">
            <div class="post">
              <div class="row invoice-info">
                <div class="col-sm-12 invoice-col">
                  <dl class="dl-horizontal">
                    <dt>Genero</dt>
                    <dd><span id="datopersonal_genero"></span></dd>
                    <dt>Fecha nacimiento</dt>
                    <dd><span id="datopersonal_fecha_nacimiento"></span></dd>
                    <dt>Nacionalidad</dt>
                    <dd><span id="datopersonal_nacionalidad"></span></dd>
                    <dt>Estado Civil</dt>
                    <dd><span id="datopersonal_estadocivil"></span></dd>
                    <dt style="margin-top: 7px !important; margin-bottom: 7px !important;">Direccion Personal</dt>
                    <dd>
                    <div class="box box-sm collapsed-box" style="border-top: 1px solid #ededed !important; width: 70% !important; margin-top: 7px !important;  margin-bottom: 7px !important;">
                        <span id="datopersonal_domicilio_principal"></span>
                        <div class="pull-right">
                          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                        </div>
                        <div class="box-body">
                          <div class="form-group margin-bottom-none">
                            <div class="col-sm-9">
                              <input id="txt_direccion_alternativa" type="text" class="form-control input-sm" placeholder="direccion alternativa">
                            </div>
                            <div class="col-sm-3">
                              <button id='btn_agregar_direccion_alternativa' class="btn btn-danger pull-right btn-block btn-sm"><i class="fa fa-plus"></i> Agregar</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </dd>
                    <div id='lista_direcciones_alternativas'>
                      
                    </div> 

                    <dt style="margin-top: 7px !important; margin-bottom: 7px !important;">Telefono  Personal</dt>
                    <dd>
                      <div class="box box-sm collapsed-box" style="border-top: 1px solid #ededed !important; width: 70% !important; margin-top: 7px !important;  margin-bottom: 7px !important;">
                         <span id="datopersonal_telefono_principal"></span>
                        <div class="pull-right">
                          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                        </div>
                        <div class="box-body">
                          <div class="form-group margin-bottom-none">
                            <div class="col-sm-9">
                              <input id="txt_telefono_alternativo" type="text" class="form-control input-sm" placeholder="direccion alternativa">
                            </div>
                            <div class="col-sm-3">
                              <button  id='btn_agregar_telefono_alternativo' class="btn btn-danger pull-right btn-block btn-sm"><i class="fa fa-plus"></i> Agregar</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </dd>
                      <div id='lista_telefonos_alternativos'>
                      
                    </div> 

                    <dt>ART</dt>
                    <dd><span id="datopersonal_art"></span></dd>
                    <dt>Empresa</dt>
                    <dd><span id="datopersonal_empresa"></span></dd>
                  </dl>
                  <hr />
                  <dl class="dl-horizontal">
                    <dt>Fecha Ingreso</dt>
                    <dd><span id="datopersonal_fecha_ingreso"></span></dd>
                    <dt>Area-Perfil</dt>
                    <dd><span id="datopersonal_perfil"></span></dd>
                    <dt>Jefe Directo</dt>
                    <dd><span id="datopersonal_jefe_directo"></span></dd>
                    <dt>Referente RRHH</dt>
                    <dd><span id="datopersonal_referente_rrhh"></span></dd>
                    <dt>Encuadre</dt>
                    <dd><span id="datopersonal_encuadre"></span></dd>
                    <dt>Direccion Laboral</dt>
                    <dd><span id="datopersonal_domicilio_laboral"></span></dd>
                    <dt>Telefono Laboral</dt>
                    <dd><span id="datopersonal_telefono_laboral"></span></dd>
                  </dl>
                  <hr />
                  <dl class="dl-horizontal">
                    <dt>Hijos (2)</dt>
                    <dd>Gonzalo Pessina</dd>
                    <dd>Vade Retro Satana Pessina</dd>
                    <dt>Carga de Familia</dt>
                    <dd>si</dd>
                  </dl>
                </div> <!-- FIN: [div class="col-sm-12 invoice-col] -->
              </div> <!-- FIN: [div class="row invoice-info] -->
            </div> <!-- FIN: [div class="post"] -->
          </div> <!-- FIN: [div class="active tab-pane" id="tab_datos_personales"] -->
          <!-- DATOS MEDICOS -->
          <div class="tab-pane" id="tab_datos_Medicos">
            <form id="form_datos_medicos" name="form_datos_medicos" novalidate="novalidate">
            <div class="post">
              <div class="row invoice-info">
                <div class="col-sm-12 invoice-col">
                
                    <div class="row">
                      <div class="col-xs-6">
                        <dl class="dl-horizontal">
                          <dt>Grupo Sanguineo</dt>
                          <dd>
                            <select name="grupo_sanguineo" id="grupo_sanguineo">
                              <option value="">seleccione</option>
                              <option value="O-">O-</option>
                              <option value="O+">O+</option>
                              <option value="A-">A−</option>
                              <option value="A+">A+</option>
                              <option value="B-">B−</option>
                              <option value="B+">B+</option>
                              <option value="AB">AB−</option>
                              <option value="AB+">AB+</option>
                            </select>
                          </dd><br />

                          <dt>Miembro SuperiorHabil</dt>
                          <dd>
                            <select id="miembro_superhabil" name="miembro_superhabil">
                              <option value="">seleccione</option>
                              <option value="Diestro">Diestro</option>
                              <option value="Zurdo">Zurdo</option>
                              <option value="Ambidiestro">Ambidiestro</option>
                            </select>
                          </dd><br />
                          <dt>Peso</dt>
                          <dd>
                            <input id="peso_kg" name="peso_kg" type="number"  min="30" max="400" step="1"  value="0">,
                            <input id="peso_gr" name="peso_gr" type="number"  min="0" max="99" step="1"  value="0">kg
                          </dd>
                        </dl>
                      </div> <!-- [div class="col-xs-6"] -->
                      <div class="col-xs-6">
                        <dl class="dl-horizontal">
                          <dt>Dador de Sangre</dt>
                          <dd>


                            <select id="dador_sangre" name="dador_sangre">
                              <option value="">seleccione</option>
                              <option value="1">SI</option>
                              <option value="0">NO</option>
                            </select>
                          </dd><br />
                          <dt>Donante de organos</dt>
                          <dd>
                            <select id="donante_organos" name="donante_organos">
                              <option value="">seleccione</option>
                              <option value="1">SI</option>
                              <option value="0">NO</option>
                            </select>
                          </dd><br />
                          <dt>Altura</dt>
                          <dd>
                            <input id="altura_mt" name="altura_mt" type="number"  min="1" max="2"  step="1" value="0">,
                            <input id="altura_cm" name="altura_cm" type="number"  min="0" max="99" step="1" value="0">mt
                          </dd>
                        </dl>
                      </div> <!-- [div class="col-xs-6"] -->
                    </div><!-- [div class="row"] -->  
                    <hr />
                    <div class="row">
                      <div class="col-xs-4">
                        <dl class="dl-horizontal">
                          <dt>Cert. Discapacidad</dt>
                          <dd>
                            <select id="certificado_discapacidad" name="certificado_discapacidad">
                              <option value="">seleccione</option>
                              <option value="1">SI</option>
                              <option value="0">NO</option>
                            </select>
                          </dd>
                        </dl>
                      </div> <!-- [div class="col-xs-4"] -->
                      <div class="col-xs-8">
                        <dl class="dl-horizontal">
                          <dt>Tipo Discapacidad</dt>
                          <dd>
                            <select  id="tipo_discapacidad" name="tipo_discapacidad[]" class="form-control select2" multiple="multiple" data-placeholder="seleccione..." style="width: 100%;">
                              <option value="Dificiencia fisica de origen motor">Dificiencia fisica de origen motor</option>
                              <option value="Dificiencia fisica de origen viseral">Dificiencia fisica de origen viseral</option>
                              <option value="Dificiencia sensorial de origen visual">Dificiencia sensorial de origen visual</option>
                            </select>
                          </dd>
                        </dl>
                      </div> <!-- [div class="col-xs-8"] -->
                    </div><!-- [div class="row"] --> 
                    <hr />
                    <div class="row">   
                      <div class="col-xs-6">   
                        <dl class="dl-horizontal">
                          <dt>Exposicion riesgos laborales</dt>
                          <dd>
                            <select id="exposicion_riesgo_laboral" name="exposicion_riesgo_laboral">
                              <option value="">seleccione</option>
                              <option value="1">SI</option>
                              <option value="0">NO</option>
                            </select>
                          </dd><br />
                          <dt>Codigo de Puesto</dt>
                          <dd><span id="datopersonal_codigo_puesto"></span></dd>
                        </dl>
                      </div> <!-- [div class="col-xs-6"] -->
                    </div><!-- [div class="row"] -->  
                  
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="button" id="btn_actualizar_datos_medicos" class="btn btn-danger pull-right"><i class="fa fa-edit"></i> Actualizar</button>
                    </div>
                  </div>
                </div> <!-- FIN: [div class="col-sm-12 invoice-col] -->
              </div> <!-- FIN: [div class="row invoice-info] -->
            </div> <!-- FIN: [div class="post"] -->
            </form>
          </div> <!-- FIN: [div class="active tab-pane" id="tab_datos_Medicos"] -->
          <!-- HISTORIAL MEDICO -->
          <div class="tab-pane" id="tab_historial">

            <div class="post">
              <div class="row invoice-info">
                <div class="col-sm-9 invoice-col">
                  <ul class="timeline timeline-inverse">
                      {{> historiaMedica }}
                  </ul>
                </div> <!-- FIN: [div class="col-md-9"] -->
              <div class="col-sm-3 invoice-col">
                <div class="box box-success">
                  <div class="box-header">
                    <h3 class="box-title">Filtros</h3>
                  </div>
                  <div class="box-body">
                    <!-- CALENDARIO -->
                    <div class="input-group">
                      <button type="button" class="btn btn-default pull-right" id="daterange-btn">
                        <span>
                          <i class="fa fa-calendar"></i> intervalo de fechas
                        </span>
                        <i class="fa fa-caret-down"></i>
                      </button>
                    </div>
                    <hr />
                    <!-- FILTROS -->
                    <div class="input-group">
                      <label><input type="checkbox" class="flat-red" checked> Proxima Consulta</label><br>  <!-- diseable  or  checked  -->
                      <label><input type="checkbox" class="flat-red" checked> Derivacion</label><br>
                      <label><input type="checkbox" class="flat-red" checked> Consulta</label><br>
                      <label><input type="checkbox" class="flat-red" checked> Medico a domicilio</label><br>
                      <label><input type="checkbox" class="flat-red" checked> Visita Externa</label>
                    </div>
                </div>
              </div>
            </div>
              </div> <!-- FIN: [div class="row invoice-info] -->
            </div> <!-- FIN: [div class="post"] -->

          </div> <!-- [div class="tab-pane" id="tab_historial"] -->
          <!-- EXAMENES MEDICOS -->
          <div class="tab-pane" id="tab_examenes_medicos">
          <div class="form-horizontal">
           
              <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Tipo de examen</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="inputName" placeholder="">
                </div>
              </div>
              <div class="form-group">
                <label for="inputExperience" class="col-sm-2 control-label">Detalle sobre archivo</label>
                <div class="col-sm-10">
                  <textarea class="form-control" id="inputExperience" placeholder=""></textarea>
                </div>
              </div>
              <div class="form-group">
                <label for="inputSkills" class="col-sm-2 control-label">Subir archivo</label>
                <div class="col-sm-10">
                  <form class="dropzone" method="post" enctype="multipart/form-data" style="width:100% ">
                    
                  </form>
                  <!--<input type="file" class="form-control" id="inputSkills" placeholder="Skills">-->
                </div>
              </div>
              <div class="form-group">
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="button" id="btn_subir_examen" class="btn btn-danger pull-right"><i class="fa fa-upload"></i> Subir</button>
                </div>
              </div>
           </div>
          </div> <!-- FIN:[div class="tab-pane" id="tab_examenes_medicos"] -->
        </div> <!-- FIN:[div class="tab-content"] -->
      </div> <!-- FIN:[div class="nav-tabs-custom"] -->
    </div> <!-- FIN:[div class="col-md-9"] -->
    <div class="col-sm-12">
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">Informacion Importante</h3>
        </div>
        <div class="box-body">
          <strong><i class="fa fa-heartbeat margin-r-5"></i> Grupos de Riesgo</strong>
          <p class="text-muted">
            <ul>
              <li>Cardiovascular</li>
            </ul> 
          </p>
          <hr>
          <strong><i class="fa fa-eyedropper margin-r-5"></i> Vacunas</strong>
          <p class="text-muted">
          <div class="col-sm-10">
            <select id="select_vacunas" class="form-control select2" multiple="multiple" data-placeholder="seleccione..." style="width: 100%;">
           
            </select>
            </div>
            <div class="col-sm-2">
            <button id="btn_actualizar_vacunas" class="btn btn-danger pull-right"><i class="fa fa-eyedropper"></i> Actualizar</button>
            </div>
          </p>
          <hr>
          <strong><i class="fa fa-book margin-r-5"></i> Antecedentes Medicos</strong>
          <p class="text-muted">
           <div class="col-sm-10">
            <select id="select_antecedentes" class="form-control select2" multiple="multiple" data-placeholder="seleccione..." style="width: 100%;">
              
            </select>
            </div>
            <div class="col-sm-2">
            <button id="btn_actualizar_antecedentes" class="btn btn-danger pull-right"><i class="fa fa-book"></i> Actualizar</button>
            </div>
          </p>            
        </div>
      </div>
    </div>
  </div>
</section>
