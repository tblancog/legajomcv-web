<?php
session_start();
include_once('config.php');
$msg_err_session="<strong>la sesion no es valida</strong><br /><i>puede que haya superado el tiempo maximo que se permite estar autenticado en la aplicacion sin tener actiidad o que intento ingresar sin autenticarse</i>";
$debugger = false;

if($debugger==true){
  echo "<pre>";
  echo "|Server|----- --------------------------------------------------<br />";
  echo "<strong>phpversion:           </strong>".phpversion()         ."<br />";
  echo "<strong>php_sapi_name:        </strong>".php_sapi_name()      ."<br />";
  echo "<strong>session_status:       </strong>".session_status()     ."<br />";
  echo "<strong>PHP_SESSION_ACTIVE:   </strong>".PHP_SESSION_ACTIVE   ."<br />";
  echo "<strong>PHP_SESSION_NONE:     </strong>".PHP_SESSION_NONE     ."<br />";
  echo "<strong>PHP_SESSION_DISABLED: </strong>".PHP_SESSION_DISABLED ."<br />";
  echo "session_id:                   </strong>".session_id()         ."<br />";
  echo "|Post|----------------------------------------------------------<br />";
  echo "<strong>postUsername:  </strong> ".$_SESSION['postUsername']  ."<br />";
  echo "<strong>postPassword:  </strong> ".$_SESSION['postPassword']  ."<br />";
  echo "<strong>postDummis:    </strong> ".$_SESSION['postDummis']    ."<br />";
  echo "|LDAP|----------------------------------------------------------<br />";
  echo "<strong>company:       </strong> ".$_SESSION['company']       ."<br />";
  echo "<strong>cn:            </strong> ".$_SESSION['cn']            ."<br />";
  echo "<strong>thumbnailphoto:</strong> ".$_SESSION['thumbnailphoto']."<br />";
  echo "<strong>samaccountname:</strong> ".$_SESSION['samaccountname']."<br />";
  echo "|MySQL|---------------------------------------------------------<br />";
  echo "<strong>IdUsr:         </strong> ".$_SESSION['IdUsr']         ."<br />";
  echo "<strong>NomUsr:        </strong> ".$_SESSION['NomUsr']        ."<br />";
  echo "<strong>EstadoUsr:     </strong> ".$_SESSION['EstadoUsr']     ."<br />";
  echo "<strong>TipoUsr:       </strong> ".$_SESSION['TipoUsr']       ."<br />";
  echo "<strong>ad_act_ver:    </strong> ".$_SESSION['ad_act_ver']    ."<br />";
  echo "<strong>ad_act_edi:    </strong> ".$_SESSION['ad_act_edi']    ."<br />";
  echo "<strong>ad_dup_ver:    </strong> ".$_SESSION['ad_dup_ver']    ."<br />";
  echo "<strong>sap_act_per:   </strong> ".$_SESSION['sap_act_per']   ."<br />";
  echo "<strong>sap_act_lab:   </strong> ".$_SESSION['sap_act_lab']   ."<br />";
  echo "<strong>sap_repo_ver:  </strong> ".$_SESSION['sap_repo_ver']  ."<br />";
  echo "<strong>ad_sincro_ej:  </strong> ".$_SESSION['ad_sincro_ej']  ."<br />";
  echo "<strong>abm_users:     </strong> ".$_SESSION['abm_users']     ."<br />";
  echo "<strong>logs_app:      </strong> ".$_SESSION['logs_app']      ."<br />";
  echo "<strong>logs_seg: home     </strong> ".$_SESSION['logs_seg']      ."<br />";
  echo "|--------------------------------------------------------|</pre><br />";
}

if($_SESSION['postUsername']!=""){

//Convierto info de base 64 a imagen
  $s_thumbnailphoto = $_SESSION['thumbnailphoto'];
  $tempFile = tempnam(sys_get_temp_dir(), 'image');
  file_put_contents($tempFile, $s_thumbnailphoto);
  $finfo = new finfo(FILEINFO_MIME_TYPE);
  $mime  = explode(';', $finfo->file($tempFile));

//Apellido y Nombre
  $s_cn = $_SESSION['cn'];
  $ape_nom = explode(',', $s_cn);
  $apellido=$ape_nom[0];
  $nombre=$ape_nom[1];

//Epresa
  $s_company  = $_SESSION['company'];
  $s_company = ucwords($s_company);

//Tipo usuario
  $s_TipoUsr  = $_SESSION['TipoUsr'];
  $s_TipoUsr = ucwords($s_TipoUsr);

//Permisos
  $s_ad_act_ver  = $_SESSION['ad_act_ver'];
  $s_ad_act_edi  = $_SESSION['ad_act_edi'];
  $s_ad_dup_ver  = $_SESSION['ad_dup_ver'];
  $s_sap_act_per = $_SESSION['sap_act_per'];
  $s_sap_act_lab = $_SESSION['sap_act_lab'];
  $s_sap_repo_ver = $_SESSION['sap_repo_ver'];
  $s_ad_sincro_ej = $_SESSION['ad_sincro_ej'];
  $s_abm_users = $_SESSION['abm_users'];
  $s_logs_app = $_SESSION['logs_app'];
  $s_logs_seg = $_SESSION['logs_seg'];

//Harcodeado para pruebas -->
  $s_TipoUsr      = "Sysadm";
  $s_ad_act_ver   = 1;
  $s_ad_act_edi   = 1;
  $s_ad_dup_ver   = 1;
  $s_sap_act_per  = 1;
  $s_sap_act_lab  = 1;
  $s_sap_repo_ver = 1;
  $s_ad_sincro_ej = 1;
  $s_abm_users    = 1;
  $s_logs_app     = 1;
  $s_logs_seg     = 1;
//fin: Harcodeado para pruebas --<
  $permisos="permisos: ";
  if($s_ad_act_ver==1){
    $permisos .= ", ad_act_ver";
  }
  if($s_ad_act_edi==1){
    $permisos .= ", ad_act_edi";
  }
  if($s_ad_dup_ver==1){
    $permisos .= ", ad_dup_ver";
  }
  if($s_sap_act_per==1){
    $permisos .= ", sap_act_per";
  }
  if($s_sap_act_lab==1){
    $permisos .= ", sap_act_lab";
  }
  if($s_sap_repo_ver==1){
    $permisos .= ", sap_repo_ver";
  }
  if($s_ad_sincro_ej==1){
    $permisos .= ", ad_sincro_ej";
  }
  if($s_abm_users==1){
    $permisos .= ", abm_users";
  }
  if($s_logs_app==1){
    $permisos .= ", logs_app";
  }
  if($s_logs_seg==1){
    $permisos .= ", logs_seg";
  }

}else{
  header("Location: http://$page_logout?err=$msg_err_session");
  exit;
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>LMD beta</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="font/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"> -->
  <!-- daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="plugins/colorpicker/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="plugins/select2/select2.min.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
  folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  <!-- bootstrapValidator -->
  <link rel="stylesheet" href="dist/plugins/bootstrapvalidator/css/bootstrapValidator.css"/>

  <!-- Flatpickr -->
  <link rel="stylesheet" href="dist/plugins/flatpickr/dist/flatpickr.min.css"/>

  <!-- application style -->
  <link rel="stylesheet" href="dist/css/front.css"/>
  <link rel="stylesheet" href="dist/css/style.css"/>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body class="hold-transition skin-green sidebar-mini">
  <div  id="loader" style="display:none;position:absolute;z-index:99;   position: absolute; top:50%; left:50%; color:#dd4b39; ">
    <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
    <span class="sr-only">Loading...</span>
  </div>
  <div class="wrapper">
    <header class="main-header" >
      <a href="#" id="home" class="logo">
        <span class="logo-mini"><b>LM</b>D</span>
        <span class="logo-lg"><b>LegajoMedico</b>Digital</span>
      </a>
      <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li class="dropdown messages-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="label label-danger">4</span>
              </a>
              <ul class="dropdown-menu">
                <li>
                  <ul class="menu">
                    <li>
                      <a href="#">
                        <div class="pull-left text-gray">
                          <i class="fa fa-medkit fa-2x "></i>
                        </div>
                        <h4>
                          Derivacion
                          <small><i class="fa fa-clock-o"></i> 5 mins</small><button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </h4>
                        <p>Alejo Mora derivo a su grupo al paciente</p>
                        <p>Tony Sanfilippo</p>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <div class="pull-left text-gray">
                          <i class="fa fa-exclamation-triangle fa-2x "></i>
                        </div>
                        <h4>
                          Articulo 211
                          <small><i class="fa fa-clock-o"></i> 2 hours</small>
                        </h4>
                        <p>David Blanco entro en 211 el dia <br />23/05/2017</p>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <div class="pull-left text-gray">
                          <i class="fa fa-user-times fa-2x "></i>
                        </div>
                        <h4>
                          Cierre de legajo
                          <small><i class="fa fa-clock-o"></i> Hoy</small>
                        </h4>
                        <p>Alejo Sanfilippo dejo de pertenes a la </p>
                        <p>compania el 21/05/2011</p>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <div class="pull-left text-gray">
                          <i class="fa fa-user-plus fa-2x "></i>
                        </div>
                        <h4>
                          Alerta de ingreso
                          <small><i class="fa fa-clock-o"></i> Ayer</small>
                        </h4>
                        <p>Maximiliano Francomano es empleado de </p>
                        <p>cablevision desde el 12/05/2015</p>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="footer"><a href="#"  id="suscripcion_alertas_notice">Ver todos los mensajes</a></li>
              </ul>
            </li>
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <?php
                if($_SESSION['thumbnailphoto']==""){
                  echo "<img src='dist/img/no_user_foto.png' class='user-image' alt=''>";
                }else{
                  echo "<img src='data:".$mime[0].";base64,".base64_encode($s_thumbnailphoto)."' class='user-image' alt=''>";
                }
                ?>
                <span class="hidden-xs"><?php echo "Hola ".$nombre."!"; ?></span>
              </a>
              <ul class="dropdown-menu">
                <li class="user-header">
                  <?php
                  if($_SESSION['thumbnailphoto']==""){
                    echo "<img src='dist/img/no_user_foto.png' class='img-circle' alt=''>";
                  }else{
                    echo "<img src='data:".$mime[0].";base64,".base64_encode($s_thumbnailphoto)."' class='img-circle' alt=''>";
                  }
                  ?>
                  <p>
                    <?php echo $nombre." ".$apellido; ?>
                    <small data-toggle="tooltip" title="<?php echo $permisos; ?>" data-placement="bottom"><?php echo $s_TipoUsr; ?></small>
                  </p>
                </li>
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="#" id="usr_logueado_perfil" class="btn btn-default btn-flat">Ampliar Perfil</a>
                  </div>
                  <div class="pull-right">
                    <a href="logout.php" class="btn btn-default btn-flat">Salir</a>
                  </div>
                </li>
              </ul>
            </li>
            <li>
              <a href="#" data-controlsidebar='control-sidebar-open'><i class="fa fa-angle-double-left fa-1x"></i></a>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <aside class="main-sidebar">
      <section class="sidebar">
        <div class="user-panel">
        </div>
        <form action="#" method="get" class="sidebar-form">
          <div>

           <select class="form-control" name="buscar_legajo" id="buscar_legajo" style="width: 100%" data-placeholder="Buscar empleado">
                    <option></option>

            </select>
            <!--<input type="text" name="q" class="form-control" placeholder="buscar empleado">
            <span class="input-group-btn">
              <button type="submit" name="search" id="empleado_perfil" class="btn btn-flat"><i class="fa fa-search"></i></button>
            </span>-->
          </div>
        </form>
        <ul class="sidebar-menu">
          <li>
            <a href="#" id="empleado_form_pessina">
               <i class="fa fa-plus"></i><span>Nuevo evento Pessina</span>
            </a>
          </li>
          <li>
            <a href="#"  id="empleado_form_alta">
              <i class="fa fa-user-times"></i> <span>ejemplo usr no encontrado</span>
            </a>
          </li>
          <li>
            <a href="#"  id="suscripcion_alertas">
              <i class="fa fa-bell"></i> <span>Suscripcion Alertas</span>
            </a>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-pie-chart"></i>
              <span>Reportes</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#" id="reporte_1"><i class="fa fa-circle-o"></i><span data-toggle="tooltip" title="" data-placement="right"> Reporte 1</span></a></li>
              <li><a href="#" id="#">  <i class="fa fa-circle-o"></i><span data-toggle="tooltip" title="" data-placement="right"> Reporte 2</span></a></li>
              <li><a href="#" id="#">  <i class="fa fa-circle-o"></i><span data-toggle="tooltip" title="" data-placement="right"> Reporte 3</span></a></li>
            </ul>
          </li>
        </ul>
      </section>
    </aside>
    <div class="content-wrapper" id="contentWrapper">
      <?php include("home.php"); ?>
    </div>
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <b>Version</b> beta
      </div>
      <strong>Sistemas app colaborativas - 2017 - </strong> todos los derechos reservados
    </footer>
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Create the tabs -->
      <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <!-- Home tab -->
        <li class="active"><a href="#Back_End" data-toggle="tab"><i class="fa fa-wrench"></i></a></li>
        <li>               <a href="#logs"     data-toggle="tab"><i class="fa fa-gears"> </i></a></li>
      </ul>
      <!-- Tab tab-content -->
      <div class="tab-content ">
        <!-- settings tab -->
        <div class="tab-pane active" id="Back_End">
          <h4 class="control-sidebar-heading">Back-End</h4>
          <!-- USUARIOS -->
          <ul class="control-sidebar-menu">
            <li data-toggle="tooltip" title="" data-placement="left">
              <a href="#" id="bk_usrs_list">
                <i class="menu-icon fa fa-group bg-blue"></i>
                <div class="menu-info">
                  <h4 class="control-sidebar-subheading">Usuarios</h4>
                  <p>Esta seccion permite el alta y edicion de los usuarios de la aplicacion</p>
                </div>
              </a>
            </li>
          </ul>
          <!-- ANTECEDENTES MEDICOS -->
          <ul class="control-sidebar-menu">
            <li data-toggle="tooltip" title="" data-placement="left">
              <a href="#" id="bk_antecedentes_medicos_list">
                <i class="menu-icon fa fa-dot-circle-o bg-yellow"></i>
                <div class="menu-info">
                  <h4 class="control-sidebar-subheading">Antecedentes medicos</h4>
                  <p>Esta seccion permite el alta y edicion de los antecedentes medios</p>
                </div>
              </a>
            </li>
          </ul>
          <!-- GRUPOS DE RIESGO -->
          <ul class="control-sidebar-menu">
            <li data-toggle="tooltip" title="" data-placement="left">
              <a href="#" id="bk_grupo_riesgo_list">
                <i class="menu-icon fa fa-dot-circle-o bg-aqua-active"></i>
                <div class="menu-info">
                  <h4 class="control-sidebar-subheading">Grupos de riesgo</h4>
                  <p>Esta seccion permite el alta y edicion de los gruposde riesgo</p>
                </div>
              </a>
            </li>
          </ul>

          <!-- MOTIVOS DE CONSULTA -->
          <ul class="control-sidebar-menu">
            <li data-toggle="tooltip" title="" data-placement="left">
              <a href="#" id="bk_motivo_consulta_list">
                <i class="menu-icon fa fa-dot-circle-o bg-red"></i>
                <div class="menu-info">
                  <h4 class="control-sidebar-subheading">Motivos consulta</h4>
                  <p>Esta seccion permite el alta y edicion de los motivos de consulta</p>
                </div>
              </a>
            </li>
          </ul>

          <!-- TIPO DE INTERVENCION -->
          <ul class="control-sidebar-menu">
            <li data-toggle="tooltip" title="" data-placement="left">
              <a href="#" id="bk_tipo_intervencion_list">
                <i class="menu-icon fa fa-dot-circle-o bg-green"></i>
                <div class="menu-info">
                  <h4 class="control-sidebar-subheading">Tipo Intervencion</h4>
                  <p>Esta seccion permite el alta y edicion de los tipos de intervencion</p>
                </div>
              </a>
            </li>
          </ul>

          <!-- ELEMENTOS UTILIZADOS -->
          <ul class="control-sidebar-menu">
            <li data-toggle="tooltip" title="" data-placement="left">
              <a href="#" id="bk_elementos_utilizados_list">
                <i class="menu-icon fa fa-dot-circle-o bg-purple"></i>
                <div class="menu-info">
                  <h4 class="control-sidebar-subheading">Elementos Utilizados</h4>
                  <p>Esta seccion permite el alta y edicion de los elementos utilizados</p>
                </div>
              </a>
            </li>
          </ul>

          <!-- PRESTADORES MEDICOS -->
          <ul class="control-sidebar-menu">
            <li data-toggle="tooltip" title="" data-placement="left">
              <a href="#" id="bk_prestador_list">
                <i class="menu-icon fa fa-dot-circle-o bg-orange"></i>
                <div class="menu-info">
                  <h4 class="control-sidebar-subheading">Prestadores Medicos</h4>
                  <p>Esta seccion permite el alta y edicion de los prestadores medicos</p>
                </div>
              </a>
            </li>
          </ul>



        </div> <!-- /.settings tab -->
        <div class="tab-pane" id="logs">
          <h3 class="control-sidebar-heading">REGISTRO DE EVENTOS (logs)</h3>
          <ul class="control-sidebar-menu">
            <li data-toggle="tooltip" title="" data-placement="left">
              <a href="#" id="appLogsApps">
                <i class="menu-icon fa fa-code bg-green"></i>
                <div class="menu-info">
                  <h4 class="control-sidebar-subheading">Registros de la app</h4>
                  <p>logs de aplicacion</p>
                </div>
              </a>
            </li>
          </ul>

          <!-- LISTAR -->
          <ul class="control-sidebar-menu">
            <li data-toggle="tooltip" title="" data-placement="left">
              <a href="#" id="appLogsSeg">
                <i class="menu-icon fa fa-user-secret bg-purple"></i>
                <div class="menu-info">
                  <h4 class="control-sidebar-subheading">Registros de seguridad</h4>
                  <p>logs de autennticacion</p>
                </div>
              </a>
            </li>
          </ul>
        </div>


      </div> <!-- /.tab-content -->
    </aside>
    <div class="control-sidebar-bg"></div>
  </div>



<div class="modal fade" role="dialog" id="modal-content">aca es el modal content</div>

  <!-- jQuery 2.2.3 -->
  <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <!-- Select2 -->
  <script src="plugins/select2/select2.full.min.js"></script>
  <!-- InputMask -->
  <script src="plugins/input-mask/jquery.inputmask.js"></script>
  <script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
  <script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>
  <!-- date-range-picker -->
  <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>-->
 <!--<script src="plugins/daterangepicker/daterangepicker.js"></script>-->
  <!-- bootstrap datepicker -->
  <!--<script src="plugins/datepicker/bootstrap-datepicker.js"></script>-->
  <!-- bootstrap color picker -->
  <!--<script src="plugins/colorpicker/bootstrap-colorpicker.min.js"></script>-->
  <!-- bootstrap time picker -->
  <!--<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>-->
  <!-- SlimScroll 1.3.0 -->
  <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- iCheck 1.0.1 -->
  <!--<script src="plugins/iCheck/icheck.min.js"></script>-->
  <!-- FastClick -->
  <script src="plugins/fastclick/fastclick.js"></script>
  <!-- Flatpickr -->
  <script src="dist/plugins/flatpickr/dist/flatpickr.min.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/app.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="dist/js/demo.js"></script>
  <!-- Page script -->

  <!-- DataTables -->
  <script src="plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
  <script type="text/javascript" src="dist/plugins/dropzone/dropzone.js"></script>
  <script type="text/javascript" src="dist/plugins/handlebars/handlebars-v4.0.10.js"></script>


  <script type="text/javascript" src="dist/plugins/bootstrapvalidator/js/bootstrapValidator.js"></script>

  <!-- datetimepicker -->
  <!--<script type="text/javascript" src="dist/js/datepicker.js"></script>-->
  <script type="text/javascript" src="dist/js/app/config/env.js"></script>
  <script type="text/javascript" src="dist/js/app/legajo/config.js"></script>
  <script type="text/javascript" src="dist/js/app/legajo/apiRequest.js"></script>
  <script type="text/javascript" src="dist/js/app/legajo/portada.js"></script>
  <script type="text/javascript" src="dist/js/app/legajo/vacunas.js"></script>
  <script type="text/javascript" src="dist/js/app/legajo/antecedentes.js"></script>

  <script type="text/javascript" src="dist/js/app/legajo/datosPersonales.js"></script>
  <script type="text/javascript" src="dist/js/app/legajo/datosMedicos.js"></script>
  <script type="text/javascript" src="dist/js/app/legajo/examenMedico.js"></script>
  <script type="text/javascript" src="dist/js/app/legajo/historiaMedica.js"></script>



  <script type="text/javascript" src="dist/js/app/legajo/legajo.js"></script>
  <script type="text/javascript" src="dist/js/app/legajo/search.js"></script>

  <script type="text/javascript" src="dist/js/app/legajo/search.js"></script>
  <script type="text/javascript" src="dist/js/app/index/index.js"></script>


  <script>
    $('#empleado_form_pessina').click(function(){
      $("#contentWrapper").load('employee/employee_profile.php');
    })
  </script>


 <!--


//Initialize Select2 Elements
//$(".select2").select2();
/*
//Datemask dd/mm/yyyy
$("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
//Datemask2 mm/dd/yyyy
$("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
//Money Euro
$("[data-mask]").inputmask();

//Date range picker
$('#reservation').daterangepicker();
//Date range picker with time picker
$('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
//Date range as a button
$('#daterange-btn').daterangepicker(
{
  ranges: {
    'Today': [moment(), moment()],
    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
    'This Month': [moment().startOf('month'), moment().endOf('month')],
    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
  },
  startDate: moment().subtract(29, 'days'),
  endDate: moment()
},
function (start, end) {
  $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
}
);
//Date picker
$('#datepicker').datepicker({
  autoclose: true
});
//iCheck for checkbox and radio inputs
$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
  checkboxClass: 'icheckbox_minimal-blue',
  radioClass: 'iradio_minimal-blue'
});
//Red color scheme for iCheck
$('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
  checkboxClass: 'icheckbox_minimal-red',
  radioClass: 'iradio_minimal-red'
});
//Flat red color scheme for iCheck
$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
  checkboxClass: 'icheckbox_flat-green',
  radioClass: 'iradio_flat-green'
});
//Colorpicker
$(".my-colorpicker1").colorpicker();
//color picker with addon
$(".my-colorpicker2").colorpicker();
//Timepicker
$(".timepicker").timepicker({
  showInputs: false
});*/

</script>-->
</body>
</html>
