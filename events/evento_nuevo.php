<?php
  // session_start();
  // echo "<pre>";
  // var_dump($_SESSION['user']);
  // echo "</pre>";
?>

<div class="content-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h1>Alta de evento<small></small>

  <small>
  <input type="radio" name="perfil" id="medico" value="medico"> Medico -
  <input type="radio" name="perfil" id="psicologo" value="psicologo"> Psicologo -
  <input type="radio" name="perfil" id="terapista" value="terapista"> Terapista -
  <input type="radio" name="perfil" id="administrativo" value="administrativo"> Administrativo
</small>
  </h1>
</div>

<script type="text/javascript">

//por defecto
function hideAllObject() {
	$('#fg_tipo_evento').hide();
	$('#fg_consultorios').hide();
	$('#fg_motivo_consulta').hide();
	$('#fg_tipo_intervencion').hide();
	$('#fg_fecha_consulta').hide();
	$('#fg_evolucion_consulta').hide();
	$('#fg_solicitud').hide();
	$('#fg_derivaciones').hide();
	//$('#fg_siniestros').hide();
	$('#fg_inasistencia').hide();
	$('#fg_adjuntar_documentos').hide();
	$('#fg_telefono').hide();
	$('#fg_direccion').hide();
	$('#fg_solicitante').hide();
	$('#fg_prestador').hide();
	$('#fg_familiar').hide();
	$('#fg_tipo_solicitud').hide();

	$('#bi_diagnostico').hide();
	$('#bi_elementos_us').hide();
	$('#bi_info_art').hide();
	$('#bi_interconsulta').hide();
	$('#fg_requiere_derivacion').hide();
	$('#fg_fecha_accion').hide();
	//$('#bi_info_solicitud').hide();

	$('#t_solicitud_telefono').hide();
	$('#t_solicitud_direccion').hide();
	$('#t_fecha_accion_f').hide();
	$('#t_fecha_accion_h').hide();

	//COMBO "tipo_evento"
	$("#s_tipo_evento").find("option[value='']").remove();
	$("#s_tipo_evento").find("option[value='consultorio']").remove();
	$("#s_tipo_evento").find("option[value='solicitud']").remove();
	$("#s_tipo_evento").find("option[value='domiciliaria']").remove();
	$("#s_tipo_evento").find("option[value='periferica']").remove();
	$("#s_tipo_evento").find("option[value='externa']").remove();
	$("#s_tipo_evento").find("option[value='telefonica']").remove();

	//COMBO "s_motivo_consulta"
	$("#s_motivo_consulta").find("option[value='']").remove();
	$("#s_motivo_consulta").find("option[value='accidente_trabajo']").remove();
	$("#s_motivo_consulta").find("option[value='enfermedad_profesional']").remove();
	$("#s_motivo_consulta").find("option[value='enfermedad_inculpable']").remove();
	$("#s_motivo_consulta").find("option[value='enfermedad_familiar']").remove();
	$("#s_motivo_consulta").find("option[value='donacion_sangre']").remove();
	$("#s_motivo_consulta").find("option[value='sin_licencia_medica']").remove();


	//COMBO "s_tipo_intervencion"
	$("#s_tipo_intervencion").find("option[value='']").remove();
	$("#s_tipo_intervencion").find("option[value='problematica_social']").remove();
	$("#s_tipo_intervencion").find("option[value='problematica_familiar']").remove();
	$("#s_tipo_intervencion").find("option[value='problematica_pareja']").remove();
	$("#s_tipo_intervencion").find("option[value='sintomas_somaticos']").remove();
	$("#s_tipo_intervencion").find("option[value='adecuacion_puesto']").remove();
	$("#s_tipo_intervencion").find("option[value='recalificacion_profecional']").remove();

	//COMBO "s_tipo_intervencion"
	$("#s_especialidad").find("option[value='']").remove();
	$("#s_especialidad").find("option[value='servicio_psicopatologia']").remove();
	$("#s_especialidad").find("option[value='servicio_oftalmologia']").remove();
	$("#s_especialidad").find("option[value='servicio_traumatologia']").remove();
	$("#s_especialidad").find("option[value='servicio_cardiologia']").remove();

	$('#s_inasistencia').hide();
	$('#s_requiere_derivacion').hide();
}

//Perfiles por defecto
function usrAdministrativo() {
	hideAllObject();

	//COMBO "tipo_evento"
	$('#s_tipo_evento').append('<option value="solicitud" selected>Solicitud</option>');

	//COMBO "motivo_consulta"
	$("#s_motivo_consulta").append('<option value="" selected>selecciona</option>');
	$("#s_motivo_consulta").append('<option value="enfermedad_inculpable">Enfermedad inculpable</option>');
	$("#s_motivo_consulta").append('<option value="enfermedad_familiar">Enfermedad familiar</option>');

	//OBJETOS "form-group"
	$('#fg_tipo_evento').show();
	$('#fg_telefono').show();
	$('#fg_direccion').show();
	$('#fg_solicitante').show();
	$('#fg_prestador').show();
	$('#fg_motivo_consulta').show();
	$('#fg_evolucion_consulta').show();
	$('#fg_tipo_solicitud').show();
}

function usrTerapista() {
	hideAllObject();

    //COMBO "s_motivo_consulta"
	$("#s_motivo_consulta").append('<option value="" selected>selecciona</option>');
	$("#s_motivo_consulta").append('<option value="enfermedad_inculpable">Enfermedad inculpable</option>');
	$("#s_motivo_consulta").append('<option value="accidente_trabajo">Accidente de trabajo</option>');
	$("#s_motivo_consulta").append('<option value="enfermedad_profesional">Enfermedad profesional</option>');


	//COMBO "s_tipo_evento"
	$("#s_tipo_evento").append('<option value="" selected>selecciona</option>');
	$("#s_tipo_evento").append('<option value="consultorio">Consultorio</option>');
	$("#s_tipo_evento").append('<option value="externa">Externa</option>');
	$("#s_tipo_evento").append('<option value="telefonica">Telefonica</option>');

	//COMBO "s_tipo_intervencion"
	//$("#s_tipo_intervencion").append('<option value="adecuacion_puesto">Adecuacion de puesto</option>');
	//$("#s_tipo_intervencion").append('<option value="recalificacion_profecional">Recalilficacion profecional</option>');

    //OBJETOS "form-group"
	$('#fg_tipo_evento').show();
    $('#fg_derivaciones').show();

}

function usrPsicologo() {
	hideAllObject();

	//COMBO "s_tipo_evento"
	$("#s_tipo_evento").append('<option value="" selected>selecciona</option>');
	$("#s_tipo_evento").append('<option value="consultorio">Consultorio</option>');
	$("#s_tipo_evento").append('<option value="externa">Externa</option>');
	$("#s_tipo_evento").append('<option value="telefonica">Telefonica</option>');

    //OBJETOS "form-group"
	$('#fg_tipo_evento').show();
    $('#fg_derivaciones').show();
	$('#fg_fecha_accion').show();
	$('#fg_fecha_consulta').show();
	$('#fg_evolucion_consulta').show();
	$('#fg_adjuntar_documentos').show();
	$('#fg_tipo_intervencion').show();
	$('#fg_requiere_derivacion').show();

	//OBJETOS "form-group"
	$('#bi_interconsulta').show();

	//COMBO "s_tipo_intervencion"
	$("#s_tipo_intervencion").append('<option value="" selected>selecciona</option>');
	$("#s_tipo_intervencion").append('<option value="problematica_social">Problemática social</option>');
	$("#s_tipo_intervencion").append('<option value="problematica_familiar">Problemática familiar</option>');
	$("#s_tipo_intervencion").append('<option value="problematica_pareja">Problemática de pareja</option>');
	$("#s_tipo_intervencion").append('<option value="sintomas_somaticos">Síntomas somáticos</option>');

	//COMBO "s_especialidad"
	$("#s_especialidad").append('<option value="" selected>selecciona</option>');
	$("#s_especialidad").append('<option value="servicio_psicopatologia">Servicio de psicopatologia</option>');

	//COMBO "s_motivo_consulta"
	$("#s_motivo_consulta").append('<option value="" selected>selecciona</option>');
	$("#s_motivo_consulta").append('<option value="accidente_trabajo">Accidente de trabajo</option>');
	$("#s_motivo_consulta").append('<option value="enfermedad_profesional">Enfermedad profesional</option>');
	$("#s_motivo_consulta").append('<option value="enfermedad_inculpable">Enfermedad inculpable</option>');
	$("#s_motivo_consulta").append('<option value="enfermedad_familiar">Enfermedad familiar</option>');
	//$("#s_motivo_consulta").append('<option value="donacion_sangre">Donacion de sangre</option>');
	$("#s_motivo_consulta").append('<option value="sin_licencia_medica">Sin licencia medica</option>');
}

function usrMedico() {
	hideAllObject();

	//OBJETOS "form-group"
    $('#fg_tipo_evento').show();
    $('#fg_motivo_consulta').show();
    $('#fg_fecha_consulta').show();
    $('#fg_evolucion_consulta').show();
    $('#fg_fecha_accion').show();
    $('#fg_inasistencia').show();
	$('#fg_adjuntar_documentos').show();
	$('#fg_requiere_derivacion').show();

	//OBJETOS "box-info"
    $('#bi_diagnostico').show();
    $('#bi_interconsulta').show();
    //$('#bi_elementos_us').show();
    //$('#fg_requiere_derivacion').show();

	//COMBO "s_motivo_consulta"
	$("#s_tipo_evento").append('<option value="" selected>selecciona</option>');
	$("#s_tipo_evento").append('<option value="consultorio">Consultorio</option>');
	$("#s_tipo_evento").append('<option value="solicitud">Solicitud</option>');
	$("#s_tipo_evento").append('<option value="domiciliaria">Domiciliaria</option>');
	$("#s_tipo_evento").append('<option value="periferica">Periferica</option>');
	$("#s_tipo_evento").append('<option value="externa">Externa</option>');
	$("#s_tipo_evento").append('<option value="telefonica">Telefonica</option>');

	//COMBO "s_motivo_consulta"
	$("#s_motivo_consulta").append('<option value="" selected>selecciona</option>');
	$("#s_motivo_consulta").append('<option value="accidente_trabajo">Accidente de trabajo</option>');
	$("#s_motivo_consulta").append('<option value="enfermedad_profesional">Enfermedad profesional</option>');
	$("#s_motivo_consulta").append('<option value="enfermedad_inculpable">Enfermedad inculpable</option>');
	$("#s_motivo_consulta").append('<option value="enfermedad_familiar">Enfermedad familiar</option>');
	$("#s_motivo_consulta").append('<option value="donacion_sangre">Donacion de sangre</option>');
	$("#s_motivo_consulta").append('<option value="sin_licencia_medica">Sin licencia medica</option>');

	//COMBO "s_especialidad"
	$("#s_especialidad").append('<option value="" selected>selecciona</option>');
	$("#s_especialidad").append('<option value="servicio_psicopatologia">Servicio de psicopatologia</option>');
	$("#s_especialidad").append('<option value="servicio_oftalmologia">Servicio de oftalmologia</option>');
	$("#s_especialidad").append('<option value="servicio_traumatologia">Servicio de traumatologia</option>');
	$("#s_especialidad").append('<option value="servicio_cardiologia">Servicio de cardiologia</option>');
}


$(document).ready(function(){
	hideAllObject();

    //Date picker
    $('#t_fecha_consulta_f').datepicker({
      autoclose: true
    });

    //Date picker
    $('#t_fecha_accion_f').datepicker({
      autoclose: true
    });

    //Timepicker
    $("#t_fecha_consulta_h").timepicker({
      showInputs: false
    });

    //Timepicker
    $("#t_fecha_accion_h").timepicker({
      showInputs: false
    });


//ACCIONES DE COMBOS

$('input:radio[name=perfil]').change(function() {
    if (this.value == 'medico') {
    	usrMedico();
    	//rol = "medico";
    }
    if (this.value == 'psicologo') {
    	usrPsicologo();
    }
    if (this.value == 'terapista') {
    	usrTerapista()
    }
    if (this.value == 'administrativo') {
    	usrAdministrativo();
    }
});


$('#s_tipo_evento').change(function(){

    var valor =$(this).val();
	var rol = $("[name=perfil]:checked").val();


    if(valor == 'consultorio'){
       $('#fg_consultorios').show();
     }else {
       $('#fg_consultorios').hide();
     }

	if(rol == 'medico'){
	usrMedico();
		if(valor == 'consultorio'){
			$("#s_tipo_evento").find("option[value='consultorio']").remove();
			$("#s_tipo_evento").append('<option value="consultorio" selected>Consultorio</option>');
			$('#fg_consultorios').show();
			$('#bi_elementos_us').show();
			//alert("1 - rol: "+rol+" | valor: "+valor);
		}else{
			$('#fg_consultorios').hide();
			$('#bi_elementos_us').hide();
		}

		if(valor == 'solicitud'){
		   usrAdministrativo();
			//COMBO "s_motivo_consulta"
			$("#s_tipo_evento").find("option[value='solicitud']").remove();
			$("#s_tipo_evento").append('<option value="">selecciona</option>');
			$("#s_tipo_evento").append('<option value="consultorio">Consultorio</option>');
			$("#s_tipo_evento").append('<option value="solicitud" selected>Solicitud</option>');
			$("#s_tipo_evento").append('<option value="domiciliaria">Domiciliaria</option>');
			$("#s_tipo_evento").append('<option value="periferica">Periferica</option>');
			$("#s_tipo_evento").append('<option value="externa">Externa</option>');
			$("#s_tipo_evento").append('<option value="telefonica">Telefonica</option>');
		}else{
			$('#fg_telefono').hide();
			$('#fg_direccion').hide();
			$('#fg_solicitante').hide();
			$('#fg_prestador').hide();
			//$('#fg_motivo_consulta').hide();
			//$('#fg_evolucion_consulta').hide();
			$('#fg_familiar').hide();
			$('#fg_solicitud').hide();
		}

		if(valor == 'domiciliaria'){
			$("#s_tipo_evento").find("option[value='domiciliaria']").remove();
			$("#s_tipo_evento").append('<option value="domiciliaria" selected>Domiciliaria</option>');
			$('#fg_solicitud').show();
		}else{
			$('#fg_solicitud').show();
		}


		if(valor == 'periferica'){
			$("#s_tipo_evento").find("option[value='periferica']").remove();
			$("#s_tipo_evento").append('<option value="periferica" selected>Periferica</option>');
			$('#fg_solicitud').show();
		}else{
			$('#fg_solicitud').hide();
		}

		if(valor == 'externa'){
			$("#s_tipo_evento").find("option[value='externa']").remove();
			$("#s_tipo_evento").append('<option value="externa" selected>Externa</option>');
			$('#bi_elementos_us').show();
		}else{
			$('#bi_elementos_us').hide();
		}

		if(valor == 'telefonica'){
			$("#s_tipo_evento").find("option[value='telefonica']").remove();
			$("#s_tipo_evento").append('<option value="telefonica" selected>Telefonica</option>');
			$('#bi_elementos_us').show();
		}else{
			$('#bi_elementos_us').hide();
		}
	} //FIN[if(rol == 'medico')]
}); //FIN[$('#s_tipo_evento').change(function()]

$('#s_derivaciones').change(function(){

	var rol = $("[name=perfil]:checked").val();
    var valor =$(this).val();

    if(valor != ''){
	    if 		 (rol == 'medico') {
	    	//posible logica para medico
	    }
	    if (rol == 'psicologo') {
	       $('#bi_diagnostico').show();
		   $('#fg_motivo_consulta').show();
	   }
	   if (rol == 'terapista') {
	       $('#bi_diagnostico').show();
		   $('#fg_motivo_consulta').show();
		   $('#fg_tipo_intervencion').show();
		   //$('#fg_siniestros').show();
		   $('#fg_adjuntar_documentos').show();
		   $('#fg_fecha_consulta').show();
		   $('#fg_fecha_accion').show();
		   $('#fg_evolucion_consulta').show();
	   }
     }else {
		$('input:radio[name=perfil]').change(function() {
		    if 		 (rol == 'medico') {
		    	//posible logica para medico
		    }
		    if (rol == 'psicologo') {
		       $('#bi_diagnostico').hide();
			   $('#fg_motivo_consulta').hide();
		   }
		   if (rol == 'terapista') {
		       $('#bi_diagnostico').hide();
			   $('#fg_motivo_consulta').hide();
			   $('#fg_tipo_intervencion').hide();
			   //$('#fg_siniestros').hide();
			   $('#fg_adjuntar_documentos').hide();
			   $('#fg_fecha_consulta').hide();
			   $('#fg_fecha_accion').hide();
			   $('#fg_evolucion_consulta').hide();
		   }
		});
     }
});

$('#s_motivo_consulta').change(function(){
    var valor =$(this).val();
    if(valor == 'accidente_trabajo' || valor == 'enfermedad_profesional'){
       $('#bi_info_art').show();
       $("#s_tipo_intervencion").append('<option value="recalificacion_profecional">Recalilficacion profecional</option>');
     }else {
       $('#bi_info_art').hide();
       $("#s_tipo_intervencion").find("option[value='recalificacion_profecional']").remove();
     }

    if(valor == 'enfermedad_inculpable' ){
       $("#s_tipo_intervencion").append('<option value="adecuacion_puesto">Adecuacion de puesto</option>');

     }else {
       $("#s_tipo_intervencion").find("option[value='adecuacion_puesto']").remove();
     }

/*
$("#s_tipo_intervencion").append('<option value="recalificacion_profecional">Recalilficacion profecional</option>');
$("#s_motivo_consulta").append('<option value="accidente_trabajo">Accidente de trabajo</option>');
$("#s_motivo_consulta").append('<option value="enfermedad_inculpable">Enfermedad inculpable</option>');
//COMBO "s_tipo_intervencion"
$("#s_tipo_intervencion").find("option[value='']").remove();
$("#s_tipo_intervencion").find("option[value='problematica_social']").remove();
$("#s_tipo_intervencion").find("option[value='problematica_familiar']").remove();
$("#s_tipo_intervencion").find("option[value='problematica_pareja']").remove();
$("#s_tipo_intervencion").find("option[value='sintomas_somaticos']").remove();
$("#s_tipo_intervencion").find("option[value='recalificacion_profecional']").remove();
Terapista
Derivacion - Inculpable  (tipointervencion adecuacion puestto)
accidente de trabajo
enfermedad profecional  -  realificacion profecional
*/

    if(valor == 'enfermedad_familiar' ){
       $('#fg_familiar').show();
     }else {
       $('#fg_familiar').hide();
     }
});

$('#s_fecha_accion').change(function(){
    var valor =$(this).val();
    if(valor != ''){
       $('#t_fecha_accion_f').show();
       $('#t_fecha_accion_h').show();
     }else {
       $('#t_fecha_accion_f').hide();
       $('#t_fecha_accion_h').hide();
     }
});

$('#s_solicitud_direccion').change(function(){
    var valor =$(this).val();
    if(valor == 'INDA'){
       //$('#fg_consultorios').css('display','block');
       $('#t_solicitud_direccion').show();
     }else {
       //$('#fg_consultorios').css('display','none');
       $('#t_solicitud_direccion').hide();
     }
});

$('#s_solicitud_telefono').change(function(){
    var valor =$(this).val();
    if(valor == 'INTA'){
       //$('#fg_consultorios').css('display','block');
       $('#t_solicitud_telefono').show();
     }else {
       //$('#fg_consultorios').css('display','none');
       $('#t_solicitud_telefono').hide();
     }
});

$('#s_solicitud_direccion').change(function(){
    var valor =$(this).val();
    if(valor == 'INDA'){
       //$('#fg_consultorios').css('display','block');
       $('#t_solicitud_direccion').show();
     }else {
       //$('#fg_consultorios').css('display','none');
       $('#t_solicitud_direccion').hide();
     }
});

$('#c_inasistencia').click(function(){
    if(this.checked) {
		$('#s_inasistencia').show();
   }else{
   		$('#s_inasistencia').hide();
   }
});

$('#c_requiere_derivacion').click(function(){
    if(this.checked) {
		$('#s_requiere_derivacion').show();
   }else{
   		$('#s_requiere_derivacion').hide();
   }
});



}); //FIN:[$(document).ready(function()]

</script>

<!-- Main content -->
<section class="content">
  <div class="row">

   <form role="form">

    <!-- left column -->
    <div class="col-md-6">
      <!-- general form elements -->
      <!-- <div class="box box-success"> -->
        <!-- <div class="box-header with-border">
          <h3 class="box-title">Info varia</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div> -->
        <!-- /.box-header -->
        <!-- form start -->

          <div class="box-body">


<div class="form-group" id="fg_tipo_evento">
<label>Tipo de evento</label>
<select class="form-control" name="s_tipo_evento" id="s_tipo_evento">
</select>
<span  id="fg_tipo_solicitud">
<input type="radio" name="tipo_solicitud" id="tipo_solicitud" value="domiciliaria"> Domiciliaria
<input type="radio" name="tipo_solicitud" id="tipo_solicitud" value="periferica"> Periferica
</span>
</div>


<!-- <div class="box box-info" id="bi_info_solicitud">
    <div class="box-header with-border">
      <h3 class="box-title">Informacion solicitud </h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
      </div>
    </div>
    <div class="box-body"> -->





 <!--   </div>
  </div> -->

            <div class="form-group" id="fg_consultorios">
              <label>Consultorio</label>
              <select class="form-control">
                <option selected>selecciona</option>
                <option>Hornos</option>
                <option>Saavedra</option>
                <option>Ultima Milla</option>
                <option>Lugano</option>
                <option>Almagro</option>
                <option>Ituzaingo</option>
                <option>Burzaco</option>
                <option>Quilmes</option>
                <option>La Plata</option>
                <option>Mar del Plata</option>
                <option>Cordoba</option>
                <option>Santa Fe</option>
                <option>Rosario</option>
                <option>Resistencia</option>
              </select>
            </div>


            <div class="form-group" id="fg_derivaciones">
              <label>Derivaciones</label>
              <select class="form-control" name="s_derivaciones" id="s_derivaciones">
                <option value="" selected>selecciona</option>
                <option value="1">Solicitud dd/mm/yyy - hh:mm:ss</option>
                <option value="2">Solicitud dd/mm/yyy - hh:mm:ss</option>
              </select>
            </div>

            <div class="form-group" id="fg_solicitud">
              <label>Solicitud</label>
              <select class="form-control">
                <option selected>selecciona</option>
                <option>Solicitud dd/mm/yyy - hh:mm:ss</option>
                <option>Solicitud dd/mm/yyy - hh:mm:ss</option>
              </select>
            </div>

            <div class="form-group" id="fg_motivo_consulta">
              <label>Motivo de consulta</label>
              <select class="form-control" name="s_motivo_consulta" id="s_motivo_consulta">
              </select>
            </div>

            <div class="form-group" id="fg_familiar">
              <label>Seleccionar familiar</label>
              <select class="form-control" name="s_familiar" id="s_familiar">
                <option value="" selected>selecciona</option>
                <option value="2">Hijo</option>
                <option value="1">Cónyuge</option>
                <option value="2">Madre</option>
                <option value="2">Padre</option>
              </select>
            </div>



            <div class="form-group" id="fg_tipo_intervencion">
              <label>Tipo de Intervencion</label>
              <select class="form-control" name="s_tipo_intervencion" id="s_tipo_intervencion">
              </select>
            </div>

            <div class="bootstrap-timepicker">
            	<div class="form-group" id="fg_fecha_consulta">
            		<label>Fecha consulta</label>
            		<div class="input-group date">
            			<div class="input-group-addon">
            				<i class="fa fa-calendar"></i>
            			</div>
            			<input type="text" class="form-control pull-right" name="t_fecha_consulta_f" id="t_fecha_consulta_f" value="<?php echo date('d/m/Y'); ?>">
            			<input type="text" class="form-control pull-right" name="t_fecha_consulta_h" id="t_fecha_consulta_h">
            		</div>
            	</div>
            </div>

            <div class="form-group" id="fg_evolucion_consulta">
              <label for="exampleInputEmail1">Evolucion de consulta</label>
              <textarea class="form-control" rows="3" placeholder=""></textarea>
            </div>



<!--
<div class="form-group">
<label for="exampleInputEmail1">Numero de carpeta</label>
<input type="text" name="" class="form-control"  placeholder="">
</div>

<div class="form-group">
<label for="exampleInputEmail1">Numero de siniestro</label>
<input type="text" name="" class="form-control"  placeholder="">
</div>
-->
<div class="form-group" id="fg_inasistencia">
  <label><input type="checkbox" class="flat-red" name="c_inasistencia" id="c_inasistencia" > Inasistencia</label>
  <select class="form-control" name="s_inasistencia" id="s_inasistencia">
    <option value="" selected>selecciona</option>
    <option value="justificada">Justificada</option>
    <option value="injustificada">Injustificada</option>
    <option value="pendiente_justificar">pendiente de justificar</option>
  </select>
</div>





</div>

<!-- </div> -->
<!-- /.box -->
</div>
<!--/.col (left) -->
<!-- right column -->
<div class="col-md-6">
  <div class="box box-info collapsed-box" id="bi_diagnostico">
    <div class="box-header with-border">
		<div class="alta-user">
		  <h3 class="box-title">Diagnostico </h3>
		  <select class="form-control">
		    <option selected="">selecciona</option>
		    <option>CIE 10</option>
		    <option>DSM 5</option>
		  </select>
		</div>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
        <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
      </div>
    </div>
    <div class="box-body">
      <div class="form-group">

        <label>Nivel 1</label>
        <select class="form-control">
          <option selected>selecciona</option>
          <option>Nivel 1-1</option>
          <option>Nivel 1-2</option>
          <option>Nivel 1-3</option>
          <option>Nivel 1-4</option>
          <option>Nivel 1-5</option>
          <option>Nivel 1-6</option>
        </select>

        <label>Nivel 2</label>
        <select class="form-control">
          <option selected>selecciona</option>
          <option>Nivel 2-1</option>
          <option>Nivel 2-2</option>
          <option>Nivel 2-3</option>
          <option>Nivel 2-4</option>
          <option>Nivel 2-5</option>
          <option>Nivel 2-6</option>
        </select>

        <label>Nivel 3</label>
        <select class="form-control">
          <option selected>selecciona</option>
          <option>Nivel 3-1</option>
          <option>Nivel 3-2</option>
          <option>Nivel 3-3</option>
          <option>Nivel 3-4</option>
          <option>Nivel 3-5</option>
          <option>Nivel 3-6</option>
        </select>

      </div>
    </div>
  </div>

  <div class="box box-info collapsed-box" id="bi_elementos_us">
    <div class="box-header with-border">
      <h3 class="box-title">Elementos utilizados/suministrados </h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
        <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
      </div>
    </div>
    <div class="box-body">
      <div class="form-group">

        <label>Elementos</label>
        <select class="form-control">
          <option selected>selecciona</option>
          <option>Gasas Pack 10</option>
          <option>Cinta Hipo</option>
          <option>Geringa</option>
        </select>

        <label>Medicamentos</label>
        <select class="form-control">
          <option selected>selecciona</option>
          <option>Ibuprofeno 500 Comprimido</option>
          <option>Ibuprofeno 400 Capsula</option>
        </select>

      </div>
    </div>
  </div>


  <div class="box box-info collapsed-box" id="bi_interconsulta">
    <div class="box-header with-border">
      <h3 class="box-title">Interconsulta</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
        <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
      </div>
    </div>
    <div class="box-body">
      <div class="form-group">
        <label for="exampleInputEmail1">Detalle</label>
        <textarea class="form-control" rows="3" placeholder=""></textarea>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Centro Medico</label>
        <select class="form-control">
          <option selected>selecciona</option>
          <option>Fitz Roy</option>
          <option>MLE</option>
          <option>Astrolaboral</option>
          <option>IRT</option>
        </select>
      </div>

      <div class="form-group">
        <label for="exampleInputEmail1">Especialidad</label>
        <select class="form-control" name="s_especialidad" id="s_especialidad">
        </select>
      </div>
    </div>
  </div>


  <div class="box box-info collapsed-box" id="bi_info_art">
    <div class="box-header with-border">
      <h3 class="box-title">Info ART</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
        <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
      </div>
    </div>
    <div class="box-body">

<div class="form-group" id="fg_siniestros">
  <label>Siniestros</label>
  <select class="form-control">
    <option selected>selecciona</option>
    <option>Siniestro 000000000  dd/mm/yyy - hh:mm:ss</option>
    <option>Siniestro 000000000  dd/mm/yyy - hh:mm:ss</option>
  </select>
</div>

  <div class="form-group">
    <label>Lugar del accidente</label>
    <select class="form-control">
      <option selected>selecciona</option>
      <option>en el trabajo</option>
      <option>en otro centro o lugar de trabajo</option>
      <option>ir o volver del trabajo</option>
      <option>desplazamiento en dia laboral</option>
      <option>otro</option>
    </select>
  </div>

     <div class="form-group">
        <label for="exampleInputEmail1">Horario Jornada</label>
        <input type="text" name="" class="form-control"  placeholder="">
      </div>

      <div class="form-group">
        <label for="exampleInputEmail1">Tarea haitual</label>
        <select class="form-control">
          <option selected>selecciona</option>
          <option>si</option>
          <option>no</option>
        </select>
      </div>

     <div class="form-group">
        <label for="exampleInputEmail1">Ocurrencia del accidente (calle)</label>
        <input type="text" name="" class="form-control"  placeholder="">
      </div>

  <div class="form-group">
    <label>Agente material asosiado</label>
    <select class="form-control">
      <option selected>selecciona</option>
<option>10001 - MOTORES TERMICOS</option>
<option>10200 - MOTORES DE EXPLOSION Y DE COMBISTIÓN INTERNA</option>
<option>10300 - MOTORES ELECTRICOS</option>
<option>10400 - COMPRESORES Y VENTILADORES</option>
<option>10500 - TRANSFORMADORES ELECTRICOS</option>
<option>10600 - OTROS MOTORES  NO ESPECIFICADOS BAJO ESTE EPIGRAFE</option>
<option>10700 - OTROS MOTORES  NO ESPECIFICADOS BAJO ESTE EPIGRAFE</option>
<option>10701 - ARBOLES DE TTRANSMISION</option>
<option>10702 - CORREAS, CABLES, POLEAS, CADENAS, ENGRANAJES</option>
<option>10703 - GENERADORES DE ENERGIA ELECTRICA</option>
<option>10704 - GENERADORES DE RADIACION</option>
<option>10705 - SISTEMAS CON CORREAS, CABLES, POLEAS, CADENAS, ENGRANAJES</option>
<option>10710 - OTROS SISTEMAS DE TRANSMISION NO LISTADOS BAJO ESTE EPIGRAFE</option>
<option>10800 - MAQUINARIAS DE AGRICULTURA, CAZA, SILVICULTURA Y PESCA</option>
<option>10801 - MAQUINARIAS PARA AGRICULTURA</option>
<option>10802 - MAQUINARIAS PARA GANADERIA</option>
<option>10803 - TRACTORES, TRACTORES CON REMOLQUE</option>
<option>10804 - CARRETILLAS MOTORIZADAS</option>
<option>10805 - MAQUINARIAS PARA LA ACTIVIDAD FORESTAL</option>
<option>10806 - MAQUINARIAS UTILIZADAS PARA LA ACTIVIDAD E INDUSTRIA PESQUERA</option>
<option>10900 - OTRAS MAQUINARIAS</option>
<option>10901 - MAQUINARIAS UTILIZADAS EN MINAS SUBTERRANEAS</option>
<option>10902 - MAQUINARIAS UTILIZADAS EN MINAS A CIELO ABIERTO Y CANTERAS</option>
<option>10903 - MAQUINARIAS UTILIZADAS EN  MATADEROS, PREPARACION Y CONSERVACION DE LA CARNE (INCLUYENDO LA ELABORACION DE FACTURAS)</option>
<option>10904 - MAQUINARIAS PARA ENVASADO, PROCESAMIENTO Y CONSERVACION DE PESCADOS, CRUSTACEOS Y OTROS PRODUCTOS DE LAGOS Y RIOS</option>
<option>10905 - MAQUINARIAS PARA LLA ELABORACION DE PRODUCTOS ALIMENTICIOS</option>
<option>10906 - MAQUINARIAS PARA LA ELABORACION DE BEBIDAS</option>
<option>10907 - MAQUINARIAS PARA LA ELABORACION DE PRODUCTOS DEL TABACO</option>
<option>10908 - MAQUINARIAS PARA HILAR, TEJER Y OTRAS MAQUINAS DE LA INDUSTRIA TEXTIL</option>
<option>10909 - MAQUINARIAS PARA EL CURTIIDO, LA PREPARACION DEL CUERO Y LA ELABORACION DE PRODUCTOS DE CUERO</option>
<option>10910 - MAQUINARIAS PARA EL PROCESAMIENTO DE LA MADERA (ASERRADEROS)</option>
<option>10911 - MAQUINARIAS PARA LA FABRICACION DE PRODUCTOS DE LA MADERA</option>
<option>10912 - MAQUINARIAS PARA LA ELABORACION DE PASTA DE MADERA, PAPEL Y CARTON</option>
<option>10913 - MAQUINARIAS UTILIZADAS EN LA IMPRESIÓN O ENCUADERNACION</option>
<option>10914 - MAQUINARIAS PARA LA EDICION Y GRABACION DE PPRODUCTOS DE PAPELERIA</option>
<option>10915 - MAQUINARIAS PARA LA ACTIVIDAD PETROLERA</option>
<option>10916 - MAQUINARIAS PARA EL TRABAJO DE METALES</option>
<option>10917 - MAQUINARIAS PARA LA INDUSTRIA QUIMICA</option>
<option>10918 - MAQUINARIAS PARA LA INDUSTRIA METALURGICA</option>
<option>10919 - MAQUINARIAS PARA LA CONSTRUCCION Y ACTIVIDADES VIALES</option>
<option>10920 - MAQUINARIAS PARA LA PRODUCCION DE ELECTRICIDAD, GAS Y AGUA</option>
<option>10921 - MAQUINARIAS PARA LA ELABORACION DE PRODUCTOS PLASTICOS</option>
<option>10922 - MAQUINARIAS PARA TRABAJOS DE LIMPIEZA Y MANTENIMIENTO</option>
<option>10923 - MAQUINARIAS PARA LA INSDUSTRIA AUTOMOTRIZ</option>
<option>10930 - OTRAS MAQUINARIAS NO LISTADAS BAJO ESTE EPIGRAFE</option>
<option>20000 - MEDIOS DE TRANSPORTE TERRESTRE</option>
<option>20001 - CAMIONES</option>
<option>20002 - CAMIONETAS</option>
<option>20003 - FURGONES</option>
<option>20004 - MICROOMNIBUS O COLECTIVOS URBANOS</option>
<option>20005 - OMNIBUS</option>
<option>20006 - AUTOMOVILES</option>
<option>20007 - MOTOCICLETAS</option>
<option>20008 - BICICLETAS</option>
<option>20009 - VEHICULOS DE TRACCION ANIMAL</option>
<option>20010 - VEHICULOS ACCIONADOS POR LA FUERZA DEL HOMBRE PARA TRANSPORTE</option>
<option>20011 - VEHICULOS MOTORIZADOS NO CLASIFICADOS BAJO  OTROS EPIGRAFES PARA TRANSPORTE</option>
<option>20020 - OTROS MEDIOS DE TRANSPORTE TERRESTRE NO INCLUIDOS BAJO ESTE EPIGRAFE</option>
<option>20100 - APARATOS DE IZAR</option>
<option>20101 - GRUAS</option>
<option>20102 - ASCENSORES, MONTACARGAS</option>
<option>20103 - CABRESTANTES</option>
<option>20104 - POLEAS</option>
<option>20105 - APAREJOS</option>
<option>20106 - AUTOELEVADORES</option>
<option>20107 - PLATAFORMA DE ELEVACION</option>
<option>20110 - OTROS APARATOS DE IZAR NO INCLUIDOS PREVIAMENTE</option>
<option>20200 - MEDIOS DE TRANSPORTE POR VIA FERREA</option>
<option>20201 - FERROCARRILES INTERURBANOS</option>
<option>20202 - SUBTERRANEOS</option>
<option>20203 - EQUIPOS DE TRANSPORTE POR VIA FERREA UTILIZADOS EN LAS MINAS, LAS GALERIAS O LAS CANTERAS</option>
<option>20204 - EQUIPOS DE TRANSPORTE POR VIA FERREA UTILIZADOS EN ESTABLECIMIENTOS INDUSTRIALES O MUELLES</option>
<option>20210 - OTROS MEDIOS DE TRANSPORTE POR VIA FERREA NO LISTADOS BAJO ESTE EPIGRAFE</option>
<option>20400 - MEDIOS DE TRANSPORTE POR AIRE</option>
<option>20401 - AVIONES</option>
<option>20402 - AVIONETAS, PLANEADORES</option>
<option>20410 - OTROS MEDIOS DE TRANSPORTE AEREOS NO LISTADOS BAJO ESTE EPIGRAFE</option>
<option>20500 - MEDIOS DE TRANSPORTE ACUATICO</option>
<option>20501 - MEDIOS DE TRASNPORTE POR AGUA CON MOTOR</option>
<option>20502 - MEDIOS DE TRASNPORTE POR AGUA SIN MOTOR</option>
<option>20600 - OTROS MEDIOS DE TRANSPORTE </option>
<option>20601 - TRANSPORTADORES AEREOS POR CABLE</option>
<option>20602 - TRANSPORTADORES MECANICOS A EXCEPCION DE LOS TRANSPORTADORES AEREOS</option>
<option>20603 - TRANSPORTADORES POR CABLE</option>
<option>20610 - OTROS MEDIOS DE TRANSPORTE NO INCLUIDOS BAJO ESTE EPIGRAFE</option>
<option>30100 - RECIPIENTES DE PRESION SIN FOGON</option>
<option>30200 - CAÑERIAS Y ACCESORIOS DE PRESION</option>
<option>30300 - CILINDROS DE GAS</option>
<option>30400 - EQUIPOS PARA BUCEO Y SUS ACCESORIOS</option>
<option>30500 - CUBAS ELECTROLITICAS</option>
<option>30600 - CABINAS</option>
<option>30700 - ELEMENTOS DE CAZA</option>
<option>30800 - CAMARAS (INCLUYE CAMARAS FRIGORIFICAS)</option>
<option>30810 - OTROS APARATOS O ACCESORIOS NO INCLUIDOS BAJO ESTE EPIGRAFE</option>
<option>30900 - MEDIOS MATERIALES PARA EL ALMACENAMIENTO</option>
<option>30901 - SILOS</option>
<option>30902 - TOLVAS</option>
<option>30903 - CONTENEDORES</option>
<option>30904 - DEPOSITOS</option>
<option>30905 - BODEGAS (INCLUYE BODEGAS DE BARCOS)</option>
<option>30906 - ESTANTERIAS</option>
<option>30907 - ESTIBAS Y PALLETS</option>
<option>30908 - TANQUES PARA LIQUIDOS Y GASES</option>
<option>30909 - TAMBORES</option>
<option>30910 - BIDONES</option>
<option>30911 - BOLSAS, CAJAS, FRASCOS</option>
<option>30912 - BALDES, RECIPIENTES</option>
<option>30920 - OTROS MEDIOS MATERIALES PARA EL ALMACENAMIENTO NO INCLUIDOS BAJO ESTE EPIGRAFE</option>
<option>31000 - HORNOS, FOGONES, ESTUFAS</option>
<option>31001 - ALTOS HORNOS</option>
<option>31002 - HORNOS DE REFINERIA</option>
<option>31003 - ESTUFAS</option>
<option>31004 - FOGONES</option>
<option>31005 - CRISOLES</option>
<option>31006 - CALDERAS</option>
<option>31010 - OTROS HORNOS, FOGONES, ESTUFAS NO INCLUIDOS BAJO ESTE EPIGRAFE</option>
<option>31100 - PLANTAS REFRIGERADORAS (INCLUYE MEDIOS DE REFRIGERACION)</option>
<option>31010 - PLANTAS DE REFRIGERACION</option>
<option>31102 - EQUIPOS DE REFRIGERACION</option>
<option>31200 - INSTALACIONES ELECTRICAS, INCLUIDOS LOS MOTORES ELECTRICOS</option>
<option>31201 - CONDUCTORES Y CABLES ELECTRICOS</option>
<option>31202 - TRANSFORMADORES</option>
<option>31203 - APARATOS DE MANDO Y DE CONTROL</option>
<option>31204 - INSTALACIONES ELECTRICAS (POSTES, TORRES)</option>
<option>31205 - HERRAMIENTAS ELECTRICAS MANUALES</option>
<option>31210 - OTRAS INSTALACIONES ELECTRICAS (CON EXCEPCION DE LAS HERRAMIENTAS ELECTRICAS MANUALES) NO INCLUIDAS BAJO ESTE EPIGRAFE</option>
<option>31300 - HERRAMIENTAS, IMPLEMENTOS Y UTENSILLOS A EXCEPCION DE LAS HERRAMIENTAS ELECTRICAS MANUALES</option>
<option>31301 - HERRAMIENTAS MANUALES ACCIONADAS MECANICAMENTE A EXCEPCION DE LAS HERRAMIENTAS ELECTRICAS MANUALES</option>
<option>31302 - HIDRAULICAS</option>
<option>31303 - NEUMATICAS</option>
<option>31304 - HERRAMIENTAS MANUALES NO ACCIONADAS MECANICAMENTE</option>
<option>31305 - INSTRUMENTOS Y ACCESORIOS DE USO MEDICO, VETERINARIO U OTROS</option>
<option>31306 - ELEMENTOS Y ACCESORIOS (NO MAQUINAS) UTILIZADOS PARA LA PESCA</option>
<option>31310 - OTRAS HERRAMIENTAS, IMPLEMENTOS Y UTENSILLOS A EXCEPCION DE LAS HERRAMIENTAS ELECTRICAS MANUALES NO INCLUIDAS BAJO ESTE EPIGRAFE</option>
<option>31400 - MEDIOS DE ASCENSO</option>
<option>31401 - ESCALERAS PORTATILES</option>
<option>31402 - ANDAMIOS</option>
<option>31403 - SILLETAS</option>
<option>31404 - RAMPAS MOVILES</option>
<option>31405 - PLATAFORMAS</option>
<option>31410 - OTROS MEDIOS DE ASCENSO NO INCLUIDOS BAJO ESTE EPIGRAFE</option>
<option>31500 - DISPOSITIVOS DE DISTRIBUCION DE MATERIA</option>
<option>31501 - CAÑERIAS DE GAS, AIRE, AGUA, MATERIAS PRIMAS Y FLUIDOS</option>
<option>31502 - CANALIZACIONES, TUBERIAS FLEXIBLES, VALVULAS, JUNTAS</option>
<option>31503 - EQUIPOS DE VENTILACION</option>
<option>31504 - TRANSPORTADORES MECANICOS</option>
<option>31505 - CINTAS TRANSPORTADORAS</option>
<option>31506 - CHIMANGOS</option>
<option>31507 - DESAGÜES Y REJILLAS</option>
<option>31508 - OTROS DISPOSITIVOS DE DISTRIBUCION DE MATERIA NO INCLUIDOS BAJO ESTE EPIGRAFE</option>
<option>31600 - OTRAS HERRAMIENTAS, IMPLEMENTOS Y UTENSILLOS</option>
<option>40100 - EXPLOSIVOS O INFLAMABLES</option>
<option>40200 - POLVOS, GASES, LIQUIDOS Y PRODUCTOS QUIMICOS A EXCEPCION DE LOS EXPLOSIVOS</option>
<option>40201 - POLVOS</option>
<option>40202 - GASES, VAPORES, HUMOS, NIEBLAS</option>
<option>40203 - LIQUIDOS</option>
<option>40204 - PRODUCTOS QUIMICOS</option>
<option>40205 - FRAGMENTOS VOLANTES</option>
<option>40210 - OTROS MATERIALES Y SUSTANCIAS NO INCLUIDOS BAJO ESTE EPIGRAFE</option>
<option>40300 - RADIACIONES</option>
<option>40301 - RADIACIONES IONIZANTES</option>
<option>40302 - RADIACIONES DE OTRO TIPO</option>
<option>50100 - EN EL EXTERIOR</option>
<option>50101 - CONDICIONES CLIMATICAS</option>
<option>50102 - SUPERFICIES DE TRANSITO Y DE TRABAJO</option>
<option>50103 - AGUA</option>
<option>50104 - EXCAVACIONES, ZANJAS Y POZOS</option>
<option>50105 - CONDICIONES TERMOHIGROMETRICAS EXTREMAS</option>
<option>50106 - CONDICION HIPER O HIPOBARICA</option>
<option>50107 - RUIDO</option>
<option>50108 - FUEGO</option>
<option>50109 - HUMO</option>
<option>50110 - OTROS ELEMENTOS EN EL EXTERIOR DEL MEDIOAMBIENTE DE TRABAJO NO INCLUIDOS BAJO ESTE EPIGRAFE</option>
<option>50200 - EN EL INTERIOR</option>
<option>50201 - PISOS</option>
<option>50202 - ESPACIOS EXIGUOS</option>
<option>50203 - ESCALERAS</option>
<option>50204 - OTRAS SUPERFICIES DE TRANSITO Y DE TRABAJO (BANCOS, ELEMENTOS DE TRABAJO Y MOBILIARIOS EN GENERAL)</option>
<option>50205 - ABERTURAS EN EL SUELO Y EN LAS PAREDES</option>
<option>50206 - CONDICIONES TERMOHIGROMETRICAS EXTREMAS</option>
<option>50207 - CONDICION HIPER O HIPOBARICA</option>
<option>50208 - RUIDO</option>
<option>50209 - AGUA</option>
<option>50210 - FUEGO</option>
<option>50220 - OTROS ELEMENTOS EN EL INTERIOR DEL MEDIOAMBIENTE DE TRABAJO NO INCLUIDOS BAJO ESTE EPIGRAFE</option>
<option>50300 - AMBIENTE SUBTERRANEOS</option>
<option>50301 - TEJADOS Y REVESTIMIENTOS DE GALERIAS DE TUNELES, ETC.</option>
<option>50302 - PISOS DE GALERIAS DE TUNELES, ETC.</option>
<option>50303 - FRENTES DE MINAS, TUNELES, ETC.</option>
<option>50304 - POZOS DE MINAS</option>
<option>50305 - EXCAVACIONES, ZANJAS Y POZOS</option>
<option>50306 - FUEGO</option>
<option>50307 - AGUA</option>
<option>50308 - CONDICIONES TERMOHIGROMETRICAS EXTREMAS</option>
<option>50309 - CONDICION HIPER O HIPOBARICA</option>
<option>50310 - RUIDO</option>
<option>50320 - OTROS ELEMENTOS DE AMBIENTES SUBTERRANEOS DEL MEDIOAMBIENTE DE TRABAJO NO INCLUIDOS BAJO ESTE EPIGRAFE</option>
<option>60100 - ARMA DE FUEGO</option>
<option>60200 - ARMA BLANCA</option>
<option>60300 - ARBOLES, PLANTAS, CULTIVOS (INCLUIDOS RAMAS, TRONCOS)</option>
<option>60400 - HONGOS</option>
<option>60500 - ANIMALES DOMESTICOS</option>
<option>60600 - ANIMALES DE CRIA</option>
<option>60700 - ANIMALES SALVAJES</option>
<option>60800 - INSECTOS, ARACNIDOS, SERPIENTES</option>
<option>60900 - MICOORGANISMOS</option>
<option>61000 - RESIDUOS DOMICILIARIOS</option>
<option>61100 - RESIDUOS INDUSTRIALES</option>
<option>61200 - RESIDUOS PATOGENOS</option>
<option>61300 - RESIDUOS QUIMICOS</option>
<option>61400 - RESIDUOS DE ORIGEN ANIMAL</option>
<option>61500 - RESIDUOS DE ORIGEN VEGETAL</option>
<option>61600 - OTROS RESIDUOS NO ESPECIFICADOS ANTERIORMENTE</option>
<option>61700 - PERSONAS</option>
<option>61800 - MATERIAS PRIMAS, PRODUCTOS ELABORADOS Y/O INTERMEDIOS</option>
    </select>
  </div>

     <div class="form-group">
        <label for="exampleInputEmail1">Timepo de exposicion al agente</label>
        <input type="text" name="" class="form-control"  placeholder="">
      </div>


      <div class="form-group">
        <label for="exampleInputEmail1">Forma de diagnostico</label>
        <select class="form-control">
          <option selected>selecciona</option>
          <option>Examen Periodico (R)</option>
          <option>Obra social (O)</option>
          <option>Prestaciones ART (B)</option>
          <option>Ausensia prolongada (A)</option>
        </select>
      </div>

      <div class="form-group">
        <label for="exampleInputEmail1">Forma del accidente</label>
        <select class="form-control">
          <option selected>selecciona</option>
<option>101 - CAIDAS DE PERSONAS CON DESNIVEL POR CAIDAS DESDE ALTURAS (ARBOLES, EDIFICIOS, ANDAMIOS, ESCALERAS, MAQUINAS DE TRABAJO, VEHICULOS)</option>
<option>102 - CAIDAS DE PERSONAS CON DESNIVEL POR CAIDAS EN PROFUNDIDADES (POZOS, FOSOS, EXCAVACIONES, ABERTURAS EN EL SUELO)</option>
<option>103 - CAIDAS DE PERSONAS QUE OCURREN AL MISMO NIVEL</option>
<option>104 - CAIDA DE PERSONAS AL AGUA</option>
<option>201 - DERRUMBE (CAIDAS DE MASAS DE TIERRA, DE ROCAS, DE PIEDRAS, DE NIEVE)</option>
<option>202 - DESPLOME (DE EDIFICIOS, DE MUROS, DE ANDAMIOS, DE ESCALERAS, DE PILAS DE MERCANCIAS)</option>
<option>203 - CAIDAS DE OBJETOS EN CURSO DE MANUTENCION MANUAL</option>
<option>204 - CAIDAS DE OBJETOS MOBILIARIOS (ARTEFACTOS DE LUZ, VENTANAS, MARCOS, BIBLIOTECAS, ETC.)</option>
<option>205 - OTRAS CAIDAS DE OBJETOS NO INCLUIDOS EN EPIGRAFES ANTERIORES DE ESTE APARTADO</option>
<option>301 - PISADAS SOBRE OBJETOS</option>
<option>302 - CHOQUE CONTRA OBJETOS INMOVILES (A EXCEPCION DE CHOQUES DEBIDOS A UNA CAIDA ANTERIOR)</option>
<option>303 - CHOQUE CONTRA OBJETOS MOVILES</option>
<option>304 - GOLPES POR OBJETOS MOVILES (COMPRENDIDOS LOS FRAGMENTOS VOLANTES Y LAS PARTICULAS) A EXCEPCION DE LOS GOLPES POR OBJETOS QUE CAEN</option>
<option>401 - ATRAPAMIENTO POR UN OBJETO</option>
<option>402 - ATRAPAMIENTO ENTRE UN OBJETO INMOVIL Y UN OBJETO MOVIL</option>
<option>403 - ATRAPAMIENTO ENTRE DOS OBJETOS MOVILES (A EXCEPCION DE LOS OBJETOS VOLANTES O QUE CAEN)</option>
<option>501 - ESFUERZOS FISICOS EXCESIVOS AL LEVANTAR OBJETOS</option>
<option>502 - ESFUERZOS FISICOS EXCESIVOS AL EMPUJAR OBJETOS</option>
<option>503 - ESFUERZOS FISICOS EXCESIVOS AL TIRAR DE OBJETOS</option>
<option>504 - ESFUERZOS FISICOS EXCESIVOS AL MANEJAR OBJETOS</option>
<option>505 - ESFUERZOS FISICOS EXCESIVOS AL LANZAR OBJETOS</option>
<option>601 - EXPOSICION AL CALOR (DE LA ATMOSFERA O DEL AMBIENTE DE TRABAJO)</option>
<option>602 - EXPOSICION AL FRIO (DE LA ATMOSFERA O DEL AMBIENTE DE TRABAJO)</option>
<option>603 - CONTACTO CON SUSTANCIAS U OBJETOS CALIENTES</option>
<option>604 - CONTACTO CON SUSTANCIAS U OBJETOS MUY FRIOS</option>
<option>605 - CONTACTO CON FUEGO</option>
<option>701 - EXPOSICION A LA CORRIENTE ELECTRICA (TIERRA HUMEDA, AGUA O AMBIENTE CON VAPOR QUE TRANSMITA ELECTRICIDAD)</option>
<option>702 - CONTACTO DIRECTO CON FUENTE DE GENERACION O TRANSMISION DE CORRIENTE ELECTRICA</option>
<option>801 - CONTACTO POR INHALACION DE SUSTANCIAS QUIMICAS</option>
<option>802 - CONTACTO POR INGESTION DE SUSTANCIAS QUIMICAS</option>
<option>803 - CONTACTO POR ABSORCION CUTANEA DE SUSTANCIAS QUIMICAS</option>
<option>804 - CONTACTO CON AGENTES BIOLOGICOS (ABSORCION, INHALACION)</option>
<option>805 - EXPOSICION A RADIACIONES IONIZANTES</option>
<option>806 - EXPOSICION A OTRAS RADIACIONES</option>
<option>807 - INOCULACION DE AGENTES BIOLOGICOS (POR PINCHAZO, HERIDAS CORTANTES)</option>
<option>901 - EXPLOSION O IMPLOSION</option>
<option>902 - INCENDIO</option>
<option>903 - ATROPELLAMIENTO DE ANIMALES</option>
<option>904 - MORDEDURA DE ANIMALES</option>
<option>905 - PICADURAS</option>
<option>906 - ATROPELLAMIENTO POR VEHICULO</option>
<option>907 - CHOQUE DE VEHICULOS</option>
<option>908 - FALLAS EN LOS MECANISMOS PARA TRABAJOS HIPERBARICOS</option>
<option>909 - AGRESION CON ARMAS</option>
<option>910 - AGRESION SIN ARMAS</option>
<option>911 - INJURIA PUNZO-CORTANTE O CONTUSA INVOLUNTARIA</option>
<option>999 - OTRAS FORMAS DE ACCIDENTE NO INCLUIDAS EN LA PRESENTE CODIFICACION</option>
        </select>
      </div>
    </div>
  </div>


      <div class="form-group" id="fg_telefono">
        <label>Telefono</label>
        <select class="form-control" name="s_solicitud_telefono" id="s_solicitud_telefono">
          <option value="INTA" >INGRESAR NUEVO TELEFONO ALTERNATIVO</option>
          <option value="1" selected>Telefono (SAP)</option>
          <option value="2" >Telefono (alternativo 1)</option>
          <option value="3" >Telefono (alternativo 2)</option>
        </select>
        <input type="text" name="t_solicitud_telefono" id="t_solicitud_telefono" class="form-control"  placeholder="nuevo telefono">
      </div>

      <div class="form-group" id="fg_direccion">
        <label>Direccion</label>
        <select class="form-control" name="s_solicitud_direccion" id="s_solicitud_direccion">
          <option value="INDA">INGRESAR NUEVA DIRECCION ALTERNATIVA</option>
          <option value="1" selected>Direccion (SAP) </option>
          <option value="2">Direccion (alternativa 1)</option>
        </select>
        <input type="text" name="t_solicitud_direccion" id="t_solicitud_direccion" class="form-control"  placeholder="nueva direccion">
      </div>


      <div class="form-group" id="fg_solicitante">
        <label for="exampleInputEmail1">Solicitante</label>
        <input type="text" name="" class="form-control"  placeholder="quien solicito">
      </div>

      <div class="form-group" id="fg_prestador">
        <label>Prestador</label>
        <select class="form-control">
          <option selected>selecciona</option>
          <option>Prestador 1</option>
          <option>Prestador 2</option>
        </select>
      </div>

<!--
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">Proximos Pasos </h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
      </div>
    </div>
    <div class="box-body">
-->

      <!-- <div class="form-group"> -->

        <div class="form-group" id="fg_requiere_derivacion">
          <label><input type="checkbox" class="flat-red" name="c_requiere_derivacion" id="c_requiere_derivacion"> Requiere derivacion</label>
          <select class="form-control" name="s_requiere_derivacion" id="s_requiere_derivacion">
            <option value="" selected>selecciona</option>
            <option value="psicologica">[tipo perfil] - [nombre de  usuario]</option>
            <option value="terapista">Medico - Aubia, Vanina Lorena</option>
            <option value="terapista">Medico - Chambouleyron, Lucia</option>
            <option value="terapista">Psico - Mosto, Andrea Giovanna</option>
            <option value="terapista">Terapista - Perdiguero, Myriam</option>
          </select>
        </div>

        <div class="bootstrap-timepicker">
        	<div class="form-group" id="fg_fecha_accion">
        		<label>Fecha Accion</label>
        		<div class="input-group date">
        			<div class="input-group-addon">
        				<i class="fa fa-calendar"></i>
        			</div>
        			<select class="form-control" name="s_fecha_accion" id="s_fecha_accion">
        				<option selected value="">selecciona</option>
        				<option value="alta_t_a_temporarias">Alta tareas adecuadas temporarias</option>
        				<option value="alta_t_a_definitivas">Alta tareas adecuadas definitivas</option>
        				<option value="alta_t_abituales">Alta tareas abituales</option>
        				<option value="proxima_consulta">Proxima consulta</option>
        				<option value="fin_intervencion">Fin intervencion</option>
        			</select>
        			<input type="text" class="form-control pull-right" id="t_fecha_accion_f" name="t_fecha_accion_f" placeholder="dd/mm/yyyy"><input type="text" class="form-control pull-right" id="t_fecha_accion_h" name="t_fecha_accion_h" placeholder="hh:mm">
        		</div>
        	</div>
        </div>

		<div class="form-group" id="fg_adjuntar_documentos">
		<label for="exampleInputFile">Adjuntar documentos</label>
		<input type="file" id="exampleInputFile">
		</div>

      <!-- </div> -->


 <!--   </div>
  </div> -->




</div>

<!--
<div class="box box-info">
<div class="box-header with-border">
<h3 class="box-title">CIE 10</h3>
</div>
<div class="box-body">
Lorem
</div>
<div class="box-footer">

<div class="form-group">
<label>Estado</label>
<div class="radio">
<label>
<input type="radio" name="optEstado" id="optEstado1" value="activo">
activo
</label>
</div>
<div class="radio">
<label>
<input type="radio" name="optEstado" id="optEstado2" value="inactivo">
inactivo
</label>
</div>
</div>

<div class="form-group">
<label>Perfil</label>
<div class="radio">
<label>
<input type="radio" name="optPerfil" id="optPerfil1" value="activo">
activo
</label>
</div>
<div class="radio">
<label>
<input type="radio" name="optPerfil" id="optPerfil2" value="inactivo">
inactivo
</label>
</div>
</div>

<div class="box-footer">
<button type="submit" class="btn btn-primary">generar</button>
</div>

</div>
</div>
-->

</div>
<!--/.col (right) -->


<div class="box-footer">
  <button type="submit" class="btn btn-primary pull-right">Generar evento</button>
</div>

</form>

</div>
<!-- /.row -->
</section>
<!-- /.content -->

<script type="text/javascript">
/*

//iCheck for checkbox and radio inputs
$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
  checkboxClass: 'icheckbox_minimal-blue',
  radioClass: 'iradio_minimal-blue'
});
//Red color scheme for iCheck
$('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
  checkboxClass: 'icheckbox_minimal-red',
  radioClass: 'iradio_minimal-red'
});
//Flat red color scheme for iCheck
$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
  checkboxClass: 'icheckbox_flat-green',
  radioClass: 'iradio_flat-green'
});

*/
</script>
