<!-- Modal html-->

  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <section class="content-header">
        <h1>Alta de Evento</h1>
      </section>

      <section class="content">
        <div class="row">
          <form id="eventForm" role="form">
            <input type="hidden" name="user_id" value="1">
            <input type="hidden" name="empleado_id" value="2">

            <!-- left column -->
            <div class="col-md-6">
              <div class="box-body">
                <div class="form-group" id="fg_tipo_evento">
                  <label>Tipo de evento</label>
                  <select class="form-control" name="tipo_evento_id" id="s_tipo_evento" style="width: 100%" data-placeholder="Seleccione tipo de evento">
                    <option></option>
                  </select>
                </div>
                <div class="form-group" id="fg_tipo_solicitud">
                  <label for="tipo_solicitud_domiciliaria"><input type="radio" id="tipo_solicitud_domiciliaria" name="tipo_solicitud" class="flat-red" value="domiciliaria"> Domiciliaria</label>
                  <label for="tipo_solicitud_periferica"><input type="radio" id="tipo_solicitud_periferica" name="tipo_solicitud" class="flat-red" value="periferica"> Periferica</label>
                </div>
                <div class="form-group" id="fg_consultorio">
                  <label>Consultorio</label>
                  <select class="form-control" id="s_consultorio" name="consultorio" style="width: 100%" data-placeholder="Seleccione consultorio">
                    <option></option>
                  </select>
                </div>
                <div class="form-group" id="fg_derivaciones">
                  <label>Derivaciones</label>
                  <select class="form-control" name="s_derivaciones" id="s_derivaciones" style="width: 100%" data-placeholder="Seleccione derivación">
                    <option></option>
                  </select>
                </div>

                <div class="form-group" id="fg_solicitud">
                  <label>Solicitud</label>
                  <select class="form-control" name="solicitud_id" id="s_solicitud" data-placeholder="Seleccione una solicitud" style="width: 100%">
                    <option></option>
                  </select>
                </div>
                <div class="form-group" id="fg_motivo_consulta">
                  <label>Motivo de consulta</label>
                  <select class="form-control" name="motivo_id" id="s_motivo_consulta" style="width: 100%" data-placeholder="Seleccione motivo">
                    <option></option>
                  </select>
                </div>
                <div class="form-group" id="fg_familiar">
                  <label>Familiar</label>
                  <select class="form-control" name="familiar" id="s_familiar" style="width: 100%" data-placeholder="Seleccione familiar">
                      <option></option>
                  </select>
                </div>
                <div class="form-group" id="fg_tipo_intervencion">
                  <label>Tipo de Intervencion</label>
                  <select class="form-control" name="tipo_intervencion_id" id="s_tipo_intervencion">
                    <option></option>
                  </select>
                </div>

                <!-- Fecha consulta -->
                <div class="form-group" id="fg_fecha_consulta">
                    <label>Fecha consulta</label>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input id="fe_fecha_consulta" name="fecha_consulta" class="flatpickr flatpickr-input active" type="text" placeholder="Seleccione fecha y hora">
                    </div>
                  </div>

                <div class="form-group" id="fg_evolucion_consulta">
                  <label for="ta_evolucion_consulta">Evolución</label>
                  <textarea id="ta_evolucion_consulta" name="evolucion" class="form-control" rows="3" placeholder=""></textarea>
                </div>
                <div class="form-group" id="fg_inasistencia">
                  <label><input type="checkbox" class="flat-red" name="c_inasistencia" id="c_inasistencia" > Inasistencia</label>
                </div>

                <div class="form-group" id="fg_ausentismo">
                  <select class="form-control" name="ausentismo" id="s_inasistencia" style="width: 100%;"></select>
                </div>
              </div>
            </div>
            <!-- Box diagnostico -->
            <div class="col-md-6">
              <div class="box box-info" id="bi_diagnostico">
                <div class="box-header with-border">
                  <div class="alta-user">
                    <!-- <h3 class="box-title"> </h3> -->
                     <label for="s_tipo_diagnostico">Diagnóstico</label>
                    <select class="form-control" id="s_tipo_diagnostico" name="tipo_diagnostico" data-placeholder="Seleccione">
                      <option></option>
              		  </select>
                    <span class="help-block invalidMessage"></span>
                  </div>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div>
                </div>
                <div class="box-body">

                  <div class="form-group" id="fg_diagnostico_abuelo">
                    <label>Nivel 1</label>
                    <select id="diagnostico_abuelo_id" class="form-control" name="diagnostico_abuelo_id" data-placeholder="Selecccione" data-width="100%">
                      <option></option>
                    </select>
                  </div>

                  <div class="form-group" id="fg_diagnostico_padre">
                    <label>Nivel 2</label>
                    <select id="diagnostico_padre_id" class="form-control" name="diagnostico_padre_id" data-placeholder="Selecccione" data-width="100%">
                      <option></option>
                    </select>
                  </div>

                  <div class="form-group" id="fg_diagnostico_hijo">
                    <label>Nivel 3</label>
                    <select id="diagnostico_hijo_id" class="form-control" name="diagnostico_hijo_id" data-placeholder="Selecccione" data-width="100%">
                      <option></option>
                    </select>
                  </div>

                </div>
              </div>

              <div class="box box-info collapsed-box" id="bi_elementos_us">
                <div class="box-header with-border">
                  <h3 class="box-title">Elementos utilizados/suministrados </h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div>
                </div>
                <div class="box-body">
                  <div class="form-group">
                    <label>Elementos</label>
                    <select class="form-control">
                      <option selected>selecciona</option>
                      <option>Gasas Pack 10</option>
                      <option>Cinta Hipo</option>
                      <option>Geringa</option>
                    </select>
                    <label>Medicamentos</label>
                    <select class="form-control">
                      <option selected>selecciona</option>
                      <option>Ibuprofeno 500 Comprimido</option>
                      <option>Ibuprofeno 400 Capsula</option>
                    </select>

                  </div>
                </div>
              </div>
              <div class="box box-info collapsed-box" id="bi_interconsulta">
                <div class="box-header with-border">
                  <h3 class="box-title">Interconsulta</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div>
                </div>
                <div class="box-body">
                  <div class="form-group" id="fg_interconsulta_detalle">
                    <label for="ta_interconsulta">Detalle</label>
                    <textarea class="form-control" rows="3" name="interconsulta" id="ta_interconsulta"></textarea>
                  </div>
                  <div class="form-group" id="fg_interconsulta">
                    <label for="s_centros">Centro Medico</label>
                    <select class="form-control" name="centromedico_id" data-placeholder="Seleccione centro médico" id="s_centros" style="width: 100%">
                      <option></option>
                    </select>
                  </div>
                  <div class="form-group" id="fg_especialidad">
                    <label for="s_especialidad">Especialidad</label>
                    <select class="form-control" name="especialidadservicio_id" id="s_especialidad" data-placeholder="Seleccione especialidad" style="width: 100%">
                      <option></option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="box box-info collapsed-box" id="bi_info_art">
                <div class="box-header with-border">
                  <h3 class="box-title">Info ART</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div>
                </div>
                <div class="box-body">
                  <div class="form-group" id="fg_siniestros">
                    <label>Siniestro</label>
                    <select class="form-control" id="s_siniestro" data-placeholder="Seleccione evento de siniestro" style="width: 100%">
                      <option></option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="s_lugar_accidente">Lugar del accidente</label>
                    <select class="form-control" id="s_lugar_accidente" name="lugar_accidente" placeholder="Seleccione una opción" placeholder="Seleccione lugar del siniestro" data-placeholder="Seleccione lugar" style="width: 100%">
                      <option></option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="s_horario_jornada">Horario Jornada</label>
                    <input type="text" class="form-control flatpickr flatpickr-input active" id="s_horario_jornada" name="hora_accidente"/>
                  </div>
                  <div class="form-group">
                    <label for="s_tarea_habitual">Tarea Habitual</label>
                    <select class="form-control" id="s_tarea_habitual" name="tarea_habitual">
                      <option value="si">si</option>
                      <option value="no">no</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="s_calle_accidente">Ocurrencia del accidente (Calle)</label>
                    <input type="text" class="form-control" id="s_calle_accidente" name="calle_accidente"/>
                  </div>
                  <div class="form-group">
                    <label for="s_agente_material">Agente material asociado</label>
                      <select class="form-control" id="s_agente_material" name="agentematerial_id" data-placeholder="Seleccione una opción" style="width: 100%">
                        <option></option>
                      </select>
                  </div>
                  <div class="form-group">
                    <label for="s_tiempoexposicionagente">Tiempo de exposición al agente</label>
                    <input type="text" class="form-control" id="s_tiempoexposicionagente" name="tiempoexposicionagente"/>
                  </div>
                  <div class="form-group">
                    <label for="s_formadiagnostico">Forma de diagnóstico</label>
                    <select class="form-control" id="s_formadiagnostico" name="formadiagnostico_id" placeholder="Seleccione una opción" style="width: 100%">
                      <option></option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="s_forma_accidente">Forma del accidente</label>
                    <select class="form-control" id="s_forma_accidente" name="forma_accidente_id" placeholder="Seleccione una opción" style="width: 100%">
                      <option></option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group" id="fg_telefono">
                <label>Telefono</label>
                <select class="form-control" name="telefono_id" id="s_solicitud_telefono" style="width: 100%" data-placeholder="Seleccione teléfono">
                  <option></option>
                </select>
              </div>
              <div class="form-group" id="fg_direccion">
                <label>Direccion</label>
                <select class="form-control" name="direccion_id" id="s_solicitud_direccion" style="width: 100%" data-placeholder="Seleccione dirección">
                  <option></option>
                </select>
              </div>
              <div class="form-group" id="fg_solicitante">
                <label for="s_solicitante">Solicitante</label>
                <input id="s_solicitante" name="solicitante" type="text" class="form-control" placeholder="Nombre del solicitante">
              </div>
              <div class="form-group" id="fg_prestador">
                <label>Prestador</label>
                <select name="prestador_id" id="s_prestador" class="form-control" style="width: 100%" data-placeholder="Seleccione prestador">
                  <option></option>
                </select>
              </div>
              <div class="form-group" id="fg_requiere_derivacion">
                <label><input type="checkbox" class="flat-red" id="c_requiere_derivacion"> Requiere derivación</label>
                <div class="form-group" id="fg_grupo_derivacion" style="display:none;">
                  <select class="form-control" name="derivacion_id" id="s_requiere_derivacion" data-placeholder="Seleccione grupo de derivación" style="width:100%">
                    <option></option>
                  </select>
                </div>
              </div>

              <!-- Fecha acción -->
              <div class="form-group" id="fg_fecha_accion">
                <label>Fecha acción</label>
                <select class="form-control" name="detalle_accion" id="s_fecha_accion">
                  <option value="alta_t_a_temporarias">Alta tareas adecuadas temporarias</option>
                  <option value="alta_t_a_definitivas">Alta tareas adecuadas definitivas</option>
                  <option value="alta_t_abituales">Alta tareas habituales</option>
                  <option value="proxima_consulta">Próxima consulta</option>
                  <option value="fin_intervencion">Fin intervencion</option>
                </select>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input id="fe_fecha_accion" name="fecha_accion" class="flatpickr flatpickr-input active" type="text" placeholder="Seleccione fecha y hora">
                </div>
              </div>

              <div class="form-group" id="fg_adjuntar_documentos">
                <label for="adjunto">Adjuntar documentos</label>
                <input type="file" id="adjunto" name="adjunto">
              </div>
            </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-primary pull-right">Generar evento</button>
            </div>
        </div>
        </form>
    </div>
    </section>
  </div>

<!-- end  modal -->
<!--<script type="text/javascript">
$(document).ready(function(){

  $("#fe_fecha_consulta, #fe_fecha_accion").flatpickr({
    enableTime: true,
    altInput: true,
    locale: 'es'
  })
  $("#s_horario_jornada").flatpickr(
    {
      enableTime: true,
      noCalendar: true,
      enableSeconds: false,
      time_24hr: false,
      dateFormat: "H:i",
      defaultHour: 8,
      defaultMinute: 0,
      locale: 'es'
  }
  )

  createEvent($('#eventForm'), rules)

  $('#newEvent').modal({ 'show': true })
})-->
</script>
