<!-- RANGO DE EDAD POR GENERO -----------------------------------
    SELECT
 CASE WHEN (YEAR(CURDATE())-YEAR(DATE_FORMAT(STR_TO_DATE(`fech_nacimiento`,'%Y%m%d'),'%Y%m%d')) BETWEEN  0 AND 18) THEN 'De  0 a 18' ELSE
  CASE WHEN (YEAR(CURDATE())-YEAR(DATE_FORMAT(STR_TO_DATE(`fech_nacimiento`,'%Y%m%d'),'%Y%m%d')) BETWEEN 19 AND 29) THEN 'De 19 a 29' ELSE
   CASE WHEN (YEAR(CURDATE())-YEAR(DATE_FORMAT(STR_TO_DATE(`fech_nacimiento`,'%Y%m%d'),'%Y%m%d')) BETWEEN 30 AND 39) THEN 'De 30 a 39' ELSE
    CASE WHEN (YEAR(CURDATE())-YEAR(DATE_FORMAT(STR_TO_DATE(`fech_nacimiento`,'%Y%m%d'),'%Y%m%d')) BETWEEN 40 AND 49) THEN 'De 40 a 49' ELSE
     CASE WHEN (YEAR(CURDATE())-YEAR(DATE_FORMAT(STR_TO_DATE(`fech_nacimiento`,'%Y%m%d'),'%Y%m%d')) BETWEEN 50 AND 59) THEN 'De 50 a 59' ELSE
      CASE WHEN (YEAR(CURDATE())-YEAR(DATE_FORMAT(STR_TO_DATE(`fech_nacimiento`,'%Y%m%d'),'%Y%m%d')) BETWEEN 60 AND 69) THEN 'De 60 a 69' ELSE
       CASE WHEN (YEAR(CURDATE())-YEAR(DATE_FORMAT(STR_TO_DATE(`fech_nacimiento`,'%Y%m%d'),'%Y%m%d')) >= 69) THEN 'De 69 o más'
       END 
      END
     END
    END
   END
  END
 END rango, COUNT(*) total, des_sexo AS genero
FROM `sap_users_cv`
GROUP BY rango,des_sexo
ORDER BY rango ASC;
----------------------------------- -->

<?php
include ("sap_conn.php");  
$link=Conectarse();//abre la conexion


//Cantidad de empleados por genero ----------------------------------
$query="
SELECT 'hombres' as genero, COUNT(des_sexo) as cantidad FROM `sap_users_cv` 
WHERE des_sexo = 'M' 
GROUP BY des_sexo 
UNION ALL 
SELECT 'mujeres' as genero, COUNT(des_sexo) as cantidad FROM `sap_users_cv` 
WHERE des_sexo = 'F' 
GROUP BY des_sexo; 
";

//echo "<pre>".$query."</pre>";

$rs = mysql_query($query, $link)or die("<br /><b>Error al ejecutar: </b><pre>".$query."</pre><br />");

if (@mysql_num_rows($rs)!=0){
  $i=0;
  while ($row = mysql_fetch_array ($rs)){
    $i++;
    $CxG_Genero[$i] = $row['genero'];
    $CxG_Cantidad[$i] = $row['cantidad'];
  } //FIN: while ($Row = mysql_fetch_array ($Rs...
}//FIN: if (@mysql_num_rows($rs)!=0...

$CxG_Cantidad_F=$CxG_Cantidad[2]; //MUJERES
$CxG_Cantidad_M=$CxG_Cantidad[1]; //HOMBRES

$CxG_Total=$CxG_Cantidad_F+$CxG_Cantidad_M;

$CxG_Cantidad_F_p = round((($CxG_Cantidad_F*100)/$CxG_Total));
$CxG_Cantidad_M_p = round((($CxG_Cantidad_M*100)/$CxG_Total));

/*echo "CxG_Cantidad F: ".$CxG_Cantidad_F." - ".$CxG_Cantidad_F_p."%<br />";
echo "CxG_Cantidad M: ".$CxG_Cantidad_M." - ".$CxG_Cantidad_M_p."%<br />";
echo "CxG_Total M+F: ".$CxG_Total."<br />";*/

//Cantidad de empleados por genero ----------------------------------
$query="
    SELECT
 CASE WHEN (YEAR(CURDATE())-YEAR(DATE_FORMAT(STR_TO_DATE(`fech_nacimiento`,'%Y%m%d'),'%Y%m%d')) BETWEEN  0 AND 18) THEN '<=18' ELSE
  CASE WHEN (YEAR(CURDATE())-YEAR(DATE_FORMAT(STR_TO_DATE(`fech_nacimiento`,'%Y%m%d'),'%Y%m%d')) BETWEEN 19 AND 29) THEN '19 a 29' ELSE
   CASE WHEN (YEAR(CURDATE())-YEAR(DATE_FORMAT(STR_TO_DATE(`fech_nacimiento`,'%Y%m%d'),'%Y%m%d')) BETWEEN 30 AND 39) THEN '30 a 39' ELSE
    CASE WHEN (YEAR(CURDATE())-YEAR(DATE_FORMAT(STR_TO_DATE(`fech_nacimiento`,'%Y%m%d'),'%Y%m%d')) BETWEEN 40 AND 49) THEN '40 a 49' ELSE
     CASE WHEN (YEAR(CURDATE())-YEAR(DATE_FORMAT(STR_TO_DATE(`fech_nacimiento`,'%Y%m%d'),'%Y%m%d')) BETWEEN 50 AND 59) THEN '50 a 59' ELSE
      CASE WHEN (YEAR(CURDATE())-YEAR(DATE_FORMAT(STR_TO_DATE(`fech_nacimiento`,'%Y%m%d'),'%Y%m%d')) BETWEEN 60 AND 69) THEN '60 a 69' ELSE
       CASE WHEN (YEAR(CURDATE())-YEAR(DATE_FORMAT(STR_TO_DATE(`fech_nacimiento`,'%Y%m%d'),'%Y%m%d')) >= 69) THEN '>69'
       END 
      END
     END
    END
   END
  END
 END rango, COUNT(*) total, des_sexo AS genero
FROM `sap_users_cv`
GROUP BY rango,des_sexo;
";

//echo "<pre>".$query."</pre>";

$rs = mysql_query($query, $link)or die("<br /><b>Error al ejecutar: </b><pre>".$query."</pre><br />");

  $arr = array();
  
  while ($obj = mysql_fetch_object($rs)) {

     $arr[] = array('rango' => $obj->rango,
                   'total' => utf8_encode($obj->total),
                   'genero' => $obj->genero,
    );
  
  } //FIN: while ($Row = mysql_fetch_array ($Rs...

//echo '' . json_encode($arr) . ''; 
//echo "<pre>";
//print_r($arr);
//echo "</pre>";

function obtener_totales($e){
  return $e["total"];
}


function filtrar_masculinos($e){
  return ($e["genero"]==="M");
}


function filtrar_femeninos($e){
  return ($e["genero"]==="F");
}


$totales_masculinos=array_map("obtener_totales",array_filter($arr,"filtrar_masculinos"));

$totales_femeninos=array_map("obtener_totales",array_filter($arr,"filtrar_femeninos"));


/*

foreach($arr as $item) {

  if ($item['genero'] === 'M')


}

*/
/*
echo "<pre>";
echo "Totales";
print_r(array_fill ( count($totales_femeninos) , 7 - count($totales_femeninos) , 0 ));
echo "</pre>";
*/
mysql_free_result($rs);
mysql_close($link);//cierra la conexion


?>



    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        SAP Reportes
        <small>Personal activo</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"  id="_home"><i class="fa fa-home"></i> home</a></li>
        <li><a href="#">sap</a></li>
        <li class="active">reportes</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-6">
         


       <!-- DONUT CHART -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Cantidad por genero</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-8">
                  <div class="chart-responsive">
                    <canvas id="cantidad_x_genero" style="height:250px"></canvas>
                  </div>
                  <!-- ./chart-responsive -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <ul class="chart-legend clearfix">
                    <li><i class="fa fa-circle-o text-red" style="color: #e637a4 !important;"></i> Mujeres</li>
                    <li><i class="fa fa-circle-o text-blue" style="color: #4b94c0 !important;"></i> Hombres</li>
                  </ul>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="#"> <span class="label label-label label-danger"><?php echo $CxG_Cantidad_F; ?></span> Mujeres
                  <span class="pull-right text-red">
                    <i class="fa fa-angle-down"></i> <?php  echo $CxG_Cantidad_F_p; ?>%
                  </span></a>
                </li>
                <li><a href="#"> <span class="label label-success"><?php echo $CxG_Cantidad_M; ?></span> Hombres
                  <span class="pull-right text-green">
                    <i class="fa fa-angle-up"></i> <?php  echo $CxG_Cantidad_M_p; ?>%
                  </span></a>
                </li>
              </ul>
            </div>
            <!-- /.footer -->
          </div>
          <!-- /.box -->


        </div>
        <!-- /.col (LEFT) -->
        <div class="col-md-6">
          


          <!-- BAR CHART -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Edades por genero</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="barChart" style="height:230px"></canvas>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col (RIGHT) -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->


<!-- ChartJS 1.0.1 -->
<script src="plugins/chartjs/Chart.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */




    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $("#cantidad_x_genero").get(0).getContext("2d");
    var cantidad_x_genero = new Chart(pieChartCanvas);
    var PieData = [
      {
        value: <?php echo $CxG_Cantidad_M; ?>,
        color: "#4b94c0",
        highlight: "#4b94c0",
        label: "Hombres"
      },
      {
        value: <?php echo $CxG_Cantidad_F; ?>,
        color: "#e637a4",
        highlight: "#e637a4",
        label: "Mujeres"
      }
    ];

    var pieOptions = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke: true,
      //String - The colour of each segment stroke
      segmentStrokeColor: "#fff",
      //Number - The width of each segment stroke
      segmentStrokeWidth: 2,
      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 50, // This is 0 for Pie charts
      //Number - Amount of animation steps
      animationSteps: 100,
      //String - Animation easing effect
      animationEasing: "easeOutBounce",
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate: true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale: false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    };
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    cantidad_x_genero.Doughnut(PieData, pieOptions);

    //-------------
    //- BAR CHART -
    //-------------

  var areaChartData = {
      labels: ["<=18", "19 a 29", "30 a 39", "40 a 49", "50 a 59", "60 a 69", ">69"],
      datasets: [
        {
          label: "HOMBRES",
          fillColor: "rgba(75, 148, 192, 1)",
          strokeColor: "rgba(75, 148, 192, 1)",
          pointColor: "rgba(75, 148, 192, 1)",
          pointStrokeColor: "#4b94c0",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(75, 148, 192,1)",
          data: [1, 1621, 3369, 2714, 1161, 165, 1]
        },
        {
          label: "MUJERES",
          fillColor: "rgba(230,55,164,0.9)",
          strokeColor: "rgba(230,55,164,0.8)",
          pointColor: "#e637a4",
          pointStrokeColor: "rgba(230,55,164,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(230,55,164,1)",
          data: [, 396, 1033, 733, 262, 47, ]
        }
      ]
    };

    var barChartCanvas = $("#barChart").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas);
    var barChartData = areaChartData;
    barChartData.datasets[1].fillColor = "#e637a4";
    barChartData.datasets[1].strokeColor = "#e637a4";
    barChartData.datasets[1].pointColor = "#e637a4";
    var barChartOptions = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: true,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - If there is a stroke on each bar
      barShowStroke: true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth: 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing: 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing: 1,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to make the chart responsive
      responsive: true,
      maintainAspectRatio: true
    };

    barChartOptions.datasetFill = false;
    barChart.Bar(barChartData, barChartOptions);
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $.ajaxSetup({
    beforeSend:function(){
        $("#loader").show();
      },
      success:function(res){
        $("#loader").hide();
      }
    }); 

    $("#_home").click(function(event) {
      $("#contentWrapper").load('home.php'); 
    });
  });
</script>