  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Edición</h3>
          <div class="box-tools pull-right">
            <button id="close_modal" type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
          </div>
        </div>
        <div class="box-body">
          <form id="formABM" class="form-horizontal">
            <div class="col-lg-12" id="fields">
              <div class="col-md-12" id="alert_error" style="display: none;"></div>

              <div class="col-md-4">
                <div class="box box-success">
                  <div class="box-body box-profile">
                    <!-- FORMULARIO DE DATOS -->
                    <div class="form-group">
                      <label class="control-label">Nombre</label>
                      <input readonly type="text" class="form-control input-sm pull-right" name="nombre" placeholder="nombre">
                    </div>
                    <div class="form-group">
                      <label class="control-label">Apellido</label>
                      <input readonly type="text" class="form-control input-sm pull-right" name="apellido" placeholder="apellido">
                    </div>
                    <div class="form-group">
                      <label class="control-label">Usuario de AD</label>
                      <input readonly type="text" class="form-control input-sm pull-right" name="userad" placeholder="usuario de ad">
                    </div>
                    <div class="form-group">
                      <label class="control-label">Email</label>
                      <input readonly type="text" class="form-control input-sm pull-right" name="email" placeholder="mail">
                    </div>
                    <div class="form-group">
                      <label class="control-label">Documento</label>
                      <input readonly type="text" class="form-control input-sm pull-right" name="documento" placeholder="documento">
                    </div>
                    <div class="form-group">
                      <label class="control-label">Cuil</label>
                      <input readonly type="text" class="form-control input-sm pull-right" name="cuil" placeholder="cuil">
                    </div>
                    <div class="form-group">
                      <label class="control-label">Legajo</label>
                      <input readonly type="text" class="form-control input-sm pull-right" name="legajo" placeholder="legajo">
                    </div>
                    <div class="form-group">
                      <label class="control-label">Sector</label>
                      <input type="text" class="form-control input-sm pull-right" name="sector" placeholder="escriba aqui el sector">
                    </div>
                    <div class="form-group">
                      <label class="control-label">Puesto</label>
                      <input type="text" class="form-control input-sm pull-right" name="puesto" placeholder="escriba aqui el puesto">
                    </div>
                    <div class="form-group">
                      <label class="control-label">Gerencia</label>
                      <input type="text" class="form-control input-sm pull-right" name="gerencia" placeholder="escriba aqui el gerencia">
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-8">
                <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_seguridad" data-toggle="tab">Seguridad</a></li>
                    <li><a href="#tab_info_profesional" data-toggle="tab">Informacion profesional</a></li>
                  </ul>
                  <div class="tab-content">
                    <div class="active tab-pane" id="tab_seguridad">
                      <div class="post">
                        <h2 class="page-header" style="font-size: 1.3em;">
                          <i class="fa fa-mortar-board"></i> Mis Roles
                          <small class="pull-right">esto define a que puede acceder</small>
                        </h2>
                        <div class="row invoice-info">
                          <div class="col-sm-6">
                            <select id="role" name="role_id" class="form-control select2 select2-dropdown" data-placeholder="Seleccione Rol.." style="width: 100%"></select>
                          </div>
                        </div>
                        <hr />
                        <h2 class="page-header" style="font-size: 1.3em;">
                <i class="fa fa-edit"></i> Mis Permisos se escritura
                <small class="pull-right">esto define que puedo modificar</small>
              </h2>
                        <select id="write_permissions" name="write_permissions[]" data-entity="permissions" class="form-control select2" multiple="multiple" data-placeholder="seleccione permisos de escritura" style="width: 100%;"></select>
                        <hr />
                        <h2 class="page-header" style="font-size: 1.3em;">
                <i class="fa fa-eye"></i> Mis Permisos de lectura
                <small class="pull-right">esto define que puedo ver</small>
              </h2>
                        <select id="read_permissions" name="read_permissions[]" data-entity="permissions" class="form-control select2" multiple="multiple" data-placeholder="seleccione permisos de lectura" style="width: 100%;"></select>
                      </div>
                    </div>
                    <div class="tab-pane" id="tab_info_profesional">
                      <div class="box-body">
                        <div class="form-group">
                          <label class="control-label"><i class="fa fa-mortar-board margin-r-5"></i>Matrícula</label>
                          <input type="text" class="form-control input-sm pull-right" name="matricula" placeholder="escriba aqui la matrícula">
                        </div>
                        <hr>
                        <div class="form-group">
                          <label class="control-label"><i class="fa fa-key margin-r-5"></i>Firma digital vigente</label>
                          <input type="text" class="form-control input-sm pull-right" name="firma" placeholder="firma">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="pull-left">
                    <div class="form-group">
                      <label class="control-label">Estado</label>
                      <input type="radio" name="estado" class="flat-red" value="1" checked> activo
                      <input type="radio" name="estado" class="flat-red" value="0"> inactivo
                    </div>
                  </div>
                  <div class="pull-right">
                    <button type="submit" class="btn btn-block btn-sm btn-danger"><i class="fa fa-plus"></i>  Guardar</button>
                  </div>
                </div>
              </div>
            </div>
        </div>
        </form>
      </div>
    </div>
  </div>


<script type="text/javascript" src="dist/js/app/ui/search.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    var id = "<?=$_GET['id']?>"
    abmAction = 'edicion'
    var deferreds = [] // array of ajax requests
    loadAbm($('#formABM'), rulesUsers, id)
    deferreds.push(
      getOptions(baseUrl, 'permissions'),
      getOptions(baseUrl, 'roles'),
      getEntity(baseUrl, meta.entity, id)
    )
    $.when.apply($, deferreds)
      .done(function(permissions, roles, entity) {
        setUserForm(permissions[0], roles[0], entity[0])
       
      })
  })
</script>
