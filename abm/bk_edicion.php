  <div class="modal-dialog">
    <div class="modal-content">
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Edición <span id="title-header"></span></h3>
          <div class="box-tools pull-right">
            <button id="close_modal" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          </div>
        </div>
        <div class="box-body">

          <div class="col-md-12" id="alert_error" style="display: none;"></div>

          <form id="formABM" class="form-horizontal">
            <div class="col-lg-12" id="fields">
              <div class="form-group">
                <label class="control-label">Nombre</label>
                <input type="text" class="form-control input-sm pull-right" name="nombre" placeholder="Escriba aqui el nombre">
              </div>
              <br />
              <div class="form-group">
                <label class="control-label">Estado</label>
                <input type="radio" name="estado" class="flat-red" value="1"> activo
                <input type="radio" name="estado" class="flat-red" value="0"> inactivo
              </div>
            </div>
            <hr />
            <div class="pull-right">
              <button type="submit" class="btn btn-block btn-sm btn-danger"><i class="fa fa-floppy-o"></i>  Guardar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

<script type="text/javascript">
  $(document).ready(function() {
    var id = "<?=$_GET['id']?>";
    abmAction = 'edicion'

    loadAbm($('#formABM'), rules, id)

    var promise = $.ajax({
      'url': baseUrl + meta.entity + '/' + id
    })
    promise.then(function(res) {
      setValue('nombre', res.nombre)
      setEstado(res.deleted_at)
    
    });
  })
</script>
