  <div class="modal-dialog">
    <div class="modal-content">
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Alta</h3>
          <div class="box-tools pull-right">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          </div>
        </div>
        <div class="box-body">

          <div class="col-md-12" id="alert_error" style="display: none;"></div>

          <form id="formABM" class="form-horizontal">
            <div class="col-lg-12" id="fields">

              <!-- Lista de campos -->
              <div class="form-group">
                <label for="antecedentes">Antecedentes</label>
                <select id="antecedentes" name="antecedentes[]" data-entity="antecedentes" class="form-control select2" multiple="multiple" data-placeholder="seleccione el Antecedente" style="width: 100%;"></select>
              </div>

              <div class="form-group">
                <label class="control-label">Nombre</label>
                <input type="text" class="form-control input-sm pull-right" name="nombre" placeholder="escriba aqui la descripcion">
              </div>
              <br />
              <div class="form-group">
                <label class="control-label">Estado</label>
                <input type="radio" name="estado" class="flat-red" value="1" checked> activo
                <input type="radio" name="estado" class="flat-red" value="0"> inactivo
              </div>
            </div>
            <hr />
            <div class="pull-right">
              <button type="submit" class="btn btn-block btn-sm btn-danger"><i class="fa fa-plus"></i>  Guardar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>


<script type="text/javascript">

  $(document).ready(function() {
    abmAction = 'alta'
    var deferreds = [];
    loadAbm($('#formABM'), rules, null)
    deferreds.push(
      getOptions(baseUrl, 'antecedentes')
    );

    $.when.apply($, deferreds)
      .done(function(antecedentes) {
        // antecedentes
        s2 = $(".select2")
        if( s2.length > 0 ) {
          $.each(s2, function(idx, el){
            var dataset = $(el).data()
            selected = entity ? entity[dataset.entity] : []
            var sel2_1 = createSelect2(
              $('#' + el.id),
              antecedentes,
              dataset.placeholder)
            initSelect2(sel2_1, selected)
          })
        }

        s2.on('change', setSubmitEnabled)

       
  })
  })

  // function setEnabledSubmit(ev){
  //
  // }
</script>
