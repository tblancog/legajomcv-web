<!-- <div class="modal fade" id="alta" role="dialog">
  <div class="modal-dialog <?php //echo ($_REQUEST['id'] == 'users' ? 'modal-lg' : '')  ?>">
    <div class="modal-content"></div>
  </div>
</div>

<div class="modal fade" id="edicion" role="dialog">
  <div class="modal-dialog  <?php //echo ($_REQUEST['id'] == 'users' ? 'modal-lg' : '')  ?>">
    <div class="modal-content"></div>
  </div>
</div>

<div class="modal fade" id="edicion-grupos-riesgo" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content"></div>
  </div>
</div> -->

<!-- <div class="modal fade" id="edicion-usuarios" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content"></div>
  </div>
</div> -->

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1 id="list-header">#title-header#</h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="#">Back-End</a></li>
    <li class="active">#active#</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">

  <div class="row">
    <div class="col-xs-12">

      <div class="box">
        <div class="box-header">
          <h3 class="box-title pull-right">Informacion obtenida a las <?php echo date("H:i:s"); ?></h3><br /><br />
          <div class="col-md-12" id="alert_ok" style="display: none;"></div>
          <a href="javascript:void(0)" onClick= 'modalAltaEntity()' class="btn btn-sm btn-success btn-flat pull-left"><i class="fa fa-user-plus"></i> ALTA DE <span id="newBtn"></span></a>
        </div>
        <input type="hidden" id="entity" value="<?php echo $_REQUEST['id'] ?>">
        <div class="box-body" id="boxBody">
          <!-- aca se muestra la pagina -->

          <!-- Datatable  -->
          <table id="dt" class="table table-bordered table-hover"></table>

        </div>
      </div>
    </div>
</section>

<script type="text/javascript" src="dist/js/app/ui/ui.js"></script>
<script type="text/javascript" src="dist/js/app/abm/element-events.js"></script>
<script type="text/javascript" src="dist/js/app/rules/rules.js"></script>
<script type="text/javascript" src="dist/js/app/rules/users.js"></script>
<script type="text/javascript" src="dist/js/app/rules/grupos_riesgo.js"></script>
<script type="text/javascript" src="dist/js/app/ui/alert.js"></script>
<script type="text/javascript" src="dist/js/app/ui/datatables.init.js"></script>
<script type="text/javascript" src="dist/js/app/abm/abm.js"></script>
