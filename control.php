<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();

include_once("config.php");
include_once("loader.php");

//LDAP ------------------------------------------------------------->
if($tValidacion=="LDAP" or $tValidacion=="LDAP_DB"){
	$ad = ldap_connect($host)
		or die("Imposible Conectar a ".$host); //Conecto al host

	ldap_set_option($ad, LDAP_OPT_REFERRALS, $LdapSetOptionReferrals);
	ldap_set_option($ad, LDAP_OPT_PROTOCOL_VERSION, $LdapSetOptionVersion);

	if($debugger==true){
		echo "ad [".$ad."](control.php)<br />";
		echo "user [".$user."](control.php)<br />";
		echo "pass [".$pass."](control.php)<br />";
	}

	$bd = ldap_bind($ad, $user, $pass); // Valido las credenciales para accesar al servidor LDAP

	//Levanto datos del usuario logueado ------------------------------------------------------->
	//(esta seccion se  puede desactivar si solo se requiere la autenticacion)
	$dn = "DC=corp, DC=cablevision, DC=com, DC=ar";


		if($debugger==false){
	        //$postUsername="cablevision\mpessina";
			echo "<strong>user:       </strong> ".$user       ."<br />";
			echo "<strong>postUsername:       </serror_reporting(E_ALL);
ini_set('display_errors', 1);<pre>";
			print_r($attrs);
			echo "</pre>";
		}

		$filter = "(&(objectCategory=Person)(objectClass=user) (samaccountname=".$postUsername."))";
		//$filter = "(&(objectCategory=Person)(objectClass=user) (samaccountname=*pessina*))";
		if($debugger==false){
			echo "<strong>filter:       </strong> ".$filter       ."<br />";
		}

		// if($attr==true){
			$result  = ldap_search($ad, $dn, $filter, $attrs); // or die ("Error en ldap_search: ".ldap_error($ad));
		// }else{
			// $result  = ldap_search($ad, $dn, $filter); // or die ("Error en ldap_search: ".ldap_error($ad));
		// }
		$entries = ldap_get_entries($ad, $result); //or die ("err  ldap_get_entries");

		//--------------------------------------------------------------------------------------------<
	if($debugger==true){
		echo "bd [".$bd."](control.php)<br />";
	}


	if($bd==1 || getenv('LDAP_VALIDATION') == 0){ //devuelve 1 si la autenticacion fue correcta caso contrario devuelve vacio

		$_SESSION['company']		=$entries[0]["company"][0];-
		$_SESSION['cn']				=$entries[0]["cn"][0];
		$_SESSION['thumbnailphoto']	=$entries[0]["thumbnailphoto"][0];
		$_SESSION['samaccountname']	=$entries[0]["samaccountname"][0];

		$ldapValidate=true;

		if($debugger==true){
			echo "<strong>filter:       </strong> ".$_SESSION['filter']       	."<br />";
			echo "<strong>company:       </strong> ".$_SESSION['company']       ."<br />";
			echo "<strong>cn:            </strong> ".$_SESSION['cn']            ."<br />";
			echo "<strong>thumbnailphoto:</strong> ".$_SESSION['thumbnailphoto']."<br />";
			echo "<strong>samaccountname:</strong> ".$_SESSION['samaccountname']."<br />";
		}
		ldap_close($ad);

	}else{
		$ldapValidate=false;
		$_SESSION = array();
		session_destroy();
		header("Location: http://$page_login?err=$msg_err_auth");
		exit;
	}

	if($debugger==true){
		echo "LDAP or LDAP_DB [".$tValidacion."](control.php)<br />";
		echo "bd [".$bd."] '1=autenticado LDAP' (control.php)<br />";
		echo "ldapValidate [".$ldapValidate."](control.php)<br />";
	}
}//FIN: if($tValidacion=="LDAP" or $tValidacion=="LDAP_DB"...
//------------------------------------------------------------------<

/**
* Revisar si el usuario autenticado en LDAP está
* en la base de legajo médico, traer los roles y permisos que tenga
*
*/
$resource = $curl->get( getenv('API_BASE_URL').
									 			"users/$postUsername");
$user = json_decode($resource->response);
if( $resource->http_status_code == 200 &&			 // request exitoso
 		count($resource->response) > 0  &&				// que al menos traiga datos
		(count($user->role) > 0 || count($user->permissions) > 0)		// que al menos tenga roles o permisos
) {
		$_SESSION['user'] = $user;
		$_SESSION['postUsername'] = $postUsername;
		header("Location: http://$page_app");
}
else {

	$_SESSION = array();
	session_destroy();
	header("Location: http://$page_login?err=$msg_err_auth");
}
