<!DOCTYPE html>
<html>
	<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>Acceso al sistema</title>

		<meta charset="utf-8" />
		<!-- <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />
		<link rel="stylesheet" href="login/css/demo.css" />
		<link rel="stylesheet" href="login/css/css.css?family=Open+Sans:300,400,700" />
		<link rel="stylesheet" href="login/css/sky-forms.css" />
		<link rel="stylesheet" href="login/css/sky-forms-red.css" />
		<!-- Font Awesome -->
  		<link rel="stylesheet" href="font/font-awesome/4.7.0/css/font-awesome.min.css">
		<!--[if lt IE 9]>
			<link rel="stylesheet" href="login/css/sky-forms-ie8.css">
		<![endif]-->

		<script src="login/js/jquery-1.9.1.min.js"></script>
		<script src="login/js/jquery.validate.min.js"></script>
		<!--[if lt IE 10]>
			<script src="login/js/jquery.placeholder.min.js"></script>
		<![endif]-->
		<!--[if lt IE 9]>
			<script src="login/js//html5.js"></script>
			<script src="login/js/sky-forms-ie8.js"></script>
		<![endif]-->

		<?php
			//include("login/sysFiles/config.php");
		?>

	</head>

	<body class="bg-red">
		<div class="body body-s">
			<form action="control.php" method="post" id="formLogin" class="sky-form" name="screenError" />
				<header><i class="fa fa-heartbeat"></i><b> LEGAJO MEDICO </b>DIGITAL</a> </header>
				<fieldset>

					<?php if($_GET["err"]!=""){ ?>
                        <section>
                        <div class="OrDie"><?php echo $_GET["err"]; ?></div>
                        </section>
                    <?php } //FIN: if($_GET["err"]!="... ?>

					<?php if($multiEmpresa==true){ ?>
					<section>
						<label class="select">
							<select name="gender">
								<option value="0" selected="" disabled="" />Dominio
                                <option value="1" />CABLEVISION
                                <option value="2" />COIRSA
                                <option value="3" />COACSA
								<option value="4" />USUARIO LOCAL
							</select>
                            <b class="tooltip tooltip-bottom-right">Seleccione el tipo de validacion</b>
                         </label>
                    </section>
                    <?php }else{ ?>
						<input type="hidden" name="gender" value="<?php echo $empresaDefault; ?>" />
                    <?php } ?>

					<?php
						if($debugger==true){
							echo "phpversion: "			 .phpversion()		   ."<br />";
							echo "php_sapi_name: "		 .php_sapi_name()	   ."<br />";
							echo "session_status: "		 .session_status()     ."<br />";
							echo "PHP_SESSION_ACTIVE: "	 .PHP_SESSION_ACTIVE   ."<br />";
							echo "PHP_SESSION_NONE: "	 .PHP_SESSION_NONE	   ."<br />";
							echo "PHP_SESSION_DISABLED: ".PHP_SESSION_DISABLED ."<br />";
							echo "session_id: "			 .session_id()		   ."<br />";
							echo "debugger: "			 .$debugger			   ."<br />";
							echo "captcha:  "			 .$captcha 			   ."<br />";
							echo "id_apps:  "			 .$id_apps			   ."<br />";
							echo "urlApp:  "			 .$urlApp			   ."<br />";
							echo "multiEmpresa:  "		 .$multiEmpresa		   ."<br />";
							echo "empresaDefault:  "	 .$empresaDefault	   ."<br />";
							echo "postUsername:  "		 .$postUsername		   ."<br />";
							echo "postPassword:  "		 .$postPassword		   ."<br />";
							echo "tValidacion:  "		 .$tValidacion		   ."<br />";
						}
					?>

                    <section>
						<label class="input">
							<i class="icon-append icon-user"></i>
							<input type="text" name="username" placeholder="Nombre de usuario" />
							<b class="tooltip tooltip-bottom-right">escriba aqui su nombre de usuario</b>
						</label>
					</section>
					<section>
						<label class="input">
							<i class="icon-append icon-lock"></i>
							<input type="password" name="password" placeholder="Contraseña" id="password" />
							<b class="tooltip tooltip-bottom-right">escriba aqui su clave de acceso</b>
						</label>
					</section>

					<?php if($captcha==true){ ?>
                    <section>
						<label class="label">Ingrese los caracteres:</label>
						<label class="input input-captcha">
							<img src="login/captcha/image.php" width="100" height="35" alt="escriba los caracteres que ve en la imagen en el cuadro de texto" />
							<input type="text" maxlength="6" name="captcha" id="captcha">
							<b class="tooltip tooltip-bottom-right">escriba los caracteres que ve en la imagen en el cuadro de texto</b>
						</label>
					</section>
					<?php } ?>

			</fieldset>
				<footer>
					<button type="submit" class="button">ENTRAR</button>
				</footer>
			</form>
		</div>

		<script type="text/javascript">
			$(function()
			{
				// Validation
				$("#formLogin").validate(
				{
					// Rules for form validation
					rules:
					{
						username:
						{
							required: true
						},
						email:
						{
							required: false,
							email: true
						},
						password:
						{
							required: true,
							minlength: 3,
							maxlength: 20
						},
						passwordConfirm:
						{
							required: false,
							minlength: 3,
							maxlength: 20,
							equalTo: '#password'
						},
						firstname:
						{
							required: false
						},
						lastname:
						{
							required: false
						},
						gender:
						{
							required: true
						},
						screenError:
						{
							required: true
						},
						terms:
						{
							required: false
						},
						captcha:
						{
							required: true,
							remote: 'captcha/process.php'
						}

					},

					// Messages for form validation
					messages:
					{
						login:
						{
							required: 'por favor escribi tus credenciales de ingreso'
						},
						email:
						{
							required: 'escribi tu email',
							email: 'escribi un email valido'
						},
						password:
						{
							required: 'Debe ingresar una contraseña'
						},
						passwordConfirm:
						{
							required: 'escribi tu contraseña una ves mas',
							equalTo: 'reescribi la contraseña'
						},
						firstname:
						{
							required: 'escribi tu nombre'
						},
						lastname:
						{
							required: 'escribi tu apellido'
						},
						gender:
						{
							required: 'Debe seleccionar el tipo de autenticacion (si selecciona Cablevision debe ingresar su usuario de windows)'
						},
						username:
						{
							required: 'Debe ingresar un nombre de usuario)'
						},
						screenError:
						{
							required: '???????!'
						},
						terms:
						{
							required: 'acepto condiciones de uso'
						},
						captcha:
						{
							required: 'ingrese los caracteres',
							remote: 'se requiere que el validador sea correcto'
						}

					},

					// Do not change code below
					errorPlacement: function(error, element)
					{
						error.insertAfter(element.parent());
						//alert("AAA");
					}

				});
				//alert("ccc");
			});
		</script>
	</body>
</html>
