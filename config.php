<?php

//GENERAL ------------------------------------------------>
$debugger		= false; //true=activa el debuger false=incactiva el debuger
$captcha		= false; //true=activa captcha  /  false=inactiva captcha
$id_apps		= 0; //Id de la aplicacion en la base "usr_x_apps"
$httpHost 		= $_SERVER['HTTP_HOST'];
$serverSelf		= rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
$path = '/legajomcv/legajomcv-web/';
// $path = '/';
$page_login		= $httpHost.$path."login.php"; //URL de la pagina del login
$page_logout    = $httpHost.$path."logout.php"; //URL de la pagina del logout
$page_app		= $httpHost.$path."index.php"; //URL de la aplicacion a la que se va a acceder
$multiempresa	= false;  //true=activa captcha  /  false=inactiva captcha
$empresaDefault	= "1"; //1=cablevision
$postUsername	= trim(preg_replace("/[^A-Za-z0-9]/", "", $_POST["username"])); //saco caracteres especiales
$postPassword	= trim($_POST["password"]); //Tomo el post de la clave del usuario y lo paso a una variable
// $tValidacion	= "LDAP_DB"; //Defino el tipo de validacion "LDAP", "DB", "LDAP_DB"
$tValidacion	= "LDAP"; //Defino el tipo de validacion "LDAP", "DB", "LDAP_DB"
//GENERAL FIN --------------------------------------------<

//LDAP --------------------------------------------------->
$user = "cablevision\\".$postUsername;
$pass=$postPassword;
$host="zeus-cv4.corp.cablevision.com.ar";
$LdapSetOptionVersion=3;
$LdapSetOptionReferrals=0;
$dn = "DC=corp, DC=cablevision, DC=com, DC=ar";
// $attr=false; //true=funciona la customizacion de atributos de $attrs
$attrs = array( //atributos que se quieren traer del usuario logueado "attr" debe estar en true
  "samaccountname",
  "company",
  "cn",
  "thumbnailphoto"
);
//LDAP FIN------------------------------------------------<

//MENSAJES ----------------------------------------------->
$msg_err_auth="<strong>usuario y/o clave invalida</strong><br /><i>es posible que sus credenciales no tengan suficientes privilegios para acceder a la aplicacion</i>";
$msg_err_mysql_conn="<strong>error interno de la aplicacion</strong><br /><i>es posible que no se encuentre el servidor de base de datos, consulte con el departamento de IT";
$msg_err_mysql_select_db="<strong>error interno de la aplicacion</strong><br /><i>es posible que no se encuentre la base de datos, consulte con el departamento de IT";

$msg_err_mysql_uery="<strong>error interno de la aplicacion</strong><br /><i>es posible un problema de estructura en la consulta a la base de datos, consulte con el departamento de IT";
//MENSAJES FIN--------------------------------------------<
