<?php

require "vendor/autoload.php";

// Env variables
$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

// Curl library
$curl = new Curl\Curl();
