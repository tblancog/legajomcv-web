function EventoFormView() {


	this.get = function () {

		let url = webUrl + "view/EventoFormView.php"
		return Web(url)
	}



	this.hideAll = function () {

		$('.form-group, .box.box-info').hide()
	}



	this.showAll = function () {

		$('.form-group, .box.box-info').show()
	}



	this.show = function (element) {

		$(element).show()
	}



	this.showTipoEvento = function (element) {

		$('#fg_tipo_evento').show()
	}


	this.showElements = function (elements) {


		$.each(elements, function (index, value) {
			$(value).show()
		})
	}



	this.hideElements = function (elements) {


		$.each(elements, function (index, value) {
			$(value).hide()
		})
	}




	this.select2TipoEvento = function (data) {

		let element = $("#s_tipo_evento")

		let s2 = select2_FromData(element, data, 'Seleccione un tipo de evento')

		return s2
	}

	this.select2Consultorio = function (data) {

		let element = $("#s_consultorio")

		let s2 = select2_FromData(element, data, 'Seleccione consultorio')

		return s2
	}

	this.flatpickrFecha = function (elements, options) {

		let element = $("#s_consultorio")

		let s2 = flatpickr_create(
			elements, options
		)
// ['#fe_fecha_consulta', '#fe_fecha_accion'], { enableTime: true }
		return s2
	}


	this.select2MotivoConsulta = function (data, flag_empty) {
		var elForm = this;
		var element = $("#s_motivo_consulta")

		if(typeof flag_empty != 'undefined' && flag_empty){
			// si flag_empty es  true entonces vacía el select 2
			element.empty()
			element.prepend('<option></option>')
		}

		var s2 = select2_FromData(element, data, 'Seleccione un Motivo de Consulta')
		.on('change', function() {

			var selected = $(this).select2('data')[0]

			if(selected.slug == 'enfermedad-familiar') {

				// Mostrar select de parentesco
				elForm.show('#fg_familiar')

			} else {

				elForm.hideElements(['#fg_familiar'])

			}
		})

		return s2
	}



	this.select2Familiar = function (data) {

		let element = $("#s_familiar")

		let s2 = select2_FromData(element, data, 'Seleccione famliar')

		return s2
	}

	this.select2Prestador = function (data) {

		let element = $("#s_prestador")

		let s2 = select2_FromData(element, data, 'Seleccione un Motivo de Consulta')

		return s2
	}



	this.select2Direccion = function (data) {


		let mapData = $.map(data,function (e) {

			return {'id':e.id, 'text':e.direccion}
		})




		let element = $("#s_solicitud_direccion")

		let s2 = select2_FromData(element, mapData, 'Seleccione una Dirección')

		return s2
	}



	this.select2Telefono = function (data) {


		let mapData = $.map(data,function (e) {

			return {'id':e.id, 'text':e.numero}
		})


		let element = $("#s_solicitud_telefono")

		let s2 = select2_FromData(element, mapData, 'Seleccione un  Telefono')

		return s2
	}







	this.bootstrapValidator = function (rules, fields,user_id,model) {

		let form = $('#eventForm')
		// bind bootstrap validator
		form.bootstrapValidator(rules, { live: 'disabled' })

			.on('success.form.bv', function(e) {

	  			e.preventDefault()

	  			let dataParser = getJsonDom(fields, [{'name':'user_id', 'value':user_id}])

					// console.log(fields)


	  			model.create(dataParser).done(function(result){
	    			$('#modal-success').modal('show');
	  			}).fail(function(error){
   					$('#modal-danger').modal('show');
	  			}).always(function(){
		    		$('#modal-content').modal('hide');
	  			})
    	})
	}





}
