var currentRole;
var currentEventType;

function showEventModal(role){
  $('#newEvent').remove()
  currentRole = role
  var deffered = [
    getAjaxHtml('events/new_event.php'),
    apiGet('empleados/2')
  ]
    switch (role) {
      case 'medico':
        deffered.push ( apiGet('tipos_evento/list'),
                        apiGet('motivos/list'),
                        apiGet('prestadores/list'),
                        apiGet('centros_medicos/list'),
                        apiGet('especialidades/list')
                      )

        $.when.apply($, deffered)
         .then( function(modalContent, empleado, tipos_evento,
                        motivos, prestadores, centros, especialidades){

            $('body').append(modalContent[0])
            usrMedico(tipos_evento[0],
                              motivos[0],
                              prestadores[0],
                              empleado[0].telefonos,
                              empleado[0].direcciones,
                              centros[0],
                              especialidades[0]
                            )
        })
      break

      case 'administrativo':
        deffered.push ( apiGet('tipos_evento/list'),
                        apiGet('motivos/list'),
                        apiGet('prestadores/list')
                      )

        $.when.apply($, deffered)
         .then( function( modalContent, empleado, tipos_evento,
                          motivos, prestadores
              ){

            $('body').append(modalContent[0])
            usrAdministrativo(tipos_evento[0],
                              motivos[0],
                              prestadores[0],
                              empleado[0].telefonos,
                              empleado[0].direcciones)
          })
      break

      case 'terapista':
        deffered.push ( apiGet('tipos_evento/list'),
                        apiGet('motivos/list') )

        $.when.apply($, deffered)
         .then( function( modalContent, empleado, tipos_evento, motivos ){
            $('body').append(modalContent[0])
            usrTerapista(tipos_evento[0],
                              motivos[0])
          })
      break

      case 'psicologia':
        deffered.push ( apiGet('tipos_evento/list'),
                        apiGet('motivos/list') )

        $.when.apply($, deffered)
         .then( function( modalContent, empleado, tipos_evento, motivos ){
            $('body').append(modalContent[0])
            usrPsicologo(tipos_evento[0],
                              motivos[0])
          })
      break
    }
}

function usrAdministrativo(tipos_evento, motivos, prestadores, telefonos, direcciones) {
	hideAllObject();

  $('#fg_tipo_evento').show()
  showHideControls('solicitud')

  // Tipo de evento
  $("#s_tipo_evento").select2({
    placeholder: 'Seleccione tipo de evento',
    data: tipos_evento.filter(function(el){
      return el.value == 'solicitud'
    })
  }).val(6).trigger('change')   // id=6 solicitud

	// Sólo válidas enfermedades inculpables y familiares
	$("#s_motivo_consulta").select2({
    placeholder: 'Seleccione motivo',
    data: motivos.filter(function(el){
      return (el.value == 'enfermedad-inculpable' ||
              el.value == 'enfermedad-familiar')
    })
  }).on("change", function (ev) {
    // get selected
    var selected = parseInt($(ev.currentTarget).find("option:selected").val())
    // 3: enfermedad-familiar
    showHideElementIf($("#fg_familiar"), [3], selected)
  })

  // Select2 familiar (si se selecciona enfermedad inculpable)
  showFamiliar()

  // Select2 prestadores
  createS2Data( $("#s_prestador"), prestadores,
                { prestadores: 'Seleccione prestador' })

  // Select2 telefono
  createS2Data( $("#s_solicitud_telefono"),
                mapS2(telefonos, 'id', 'numero', ['descripcion']),
                { templateResult: formatTelAndAddresses, placeholder: 'Seleccione teléfono', tags: true } )

  // Select2 dirección
  createS2Data( $("#s_solicitud_direccion"),
                mapS2(telefonos, 'id', 'numero', ['descripcion']),
                { templateResult: formatTelAndAddresses, placeholder: 'Seleccione dirección', tags: true } )

  // Enable
	$('.solicitud').show();

  // Ocultar los que no apliquen
	$('#fg_familiar').hide();

}

function usrMedico(tipos_evento, motivos, prestadores, telefonos, direcciones, centros, especialidades) {
	hideAllObject();

  $('#fg_tipo_evento').show()

  // Tipo de evento
  createS2Data( $("#s_tipo_evento"), tipos_evento)
              .on("change", function (ev) {
    var selected = parseInt($(ev.currentTarget).find("option:selected").val())

    // Tipo de evento: consultorio (1)
    if( selected == 1 ){
      showHideControls('consulta')

      showConsultorios()

    // Tipo de evento: domiciliaria (2) o periférica (3)
    } else if( [2,3].indexOf(selected) >= 0 ) {
      showHideControls('periferica-domiciliaria')
      createS2Ajax( $("#s_solicitud"), 'eventos/list?tipoevento=solicitud')
                  .on("change", function (ev) {
                    var selected = parseInt($(ev.currentTarget).find("option:selected").val())
                    setMotivoFromSolicitudId(selected) })

    // Tipo de evento: telefónica(4), externa(5)
    } else if ( [4,5].indexOf(selected) >= 0 ) {

      showHideControls('telefonica-externa')
      motivoS2.select2({ data: motivos })

    // Tipo de evento: solicitud (6)
    } else if( selected  == 6) {
      showHideControls('solicitud')
      motivoS2.select2({ data: motivos })
    }
  })

	// Motivos
	var motivoS2 = createS2Data( $("#s_motivo_consulta"),  motivos)
    .on("change", function (ev) {

    var selected = parseInt($(ev.currentTarget).find("option:selected").val())

    // 1: accidente-de-trabajo
    if(selected  == 1) {
      showInfoART()
    } else {
      hideART()
    }
    // 3: enfermedad-familiar
    showHideElementIf($("#fg_familiar"), [3], selected)
  })

  // Select2 familiar (si se selecciona enfermedad inculpable)
  showFamiliar()

  // Checkbox inasistencia con opciones
  showInasistencia()

  // Diagnósticos
  var diagnostics_type = [ { id: 'CIE10', text: 'CIE10' }, { id: 'DSM5', text: 'DSM5' } ]
  var s2Grandpa
  var grandpaSelected = ''
  var selected_type = 'CIE10'

  createS2Data( $('#s_tipo_diagnostico'), diagnostics_type )
    .on('change', function(ev) {
    var selected_type = $(ev.currentTarget).find('option:selected').val()
    setS2Diagnosticos(selected_type, '', '');
  })

  setS2Diagnosticos(selected_type, '', '');

  // Select2 prestadores
  createS2Data($("#s_prestador"), prestadores)

  // Select2 telefono
  createS2Data( $("#s_solicitud_telefono"),
                mapS2(telefonos, 'id', 'numero', ['descripcion']),
                { templateResult: formatTelAndAddresses, tags: true } )
  // Select2 dirección
  createS2Data($("#s_solicitud_direccion"),
              mapS2(direcciones, 'id', 'direccion', ['descripcion']),
              { templateResult: formatTelAndAddresses, tags: true } )

  // Select2 centros_medicos
  createS2Data($("#s_centros"), centros)

  // Select2 especialidades
  createS2Data($("#s_especialidad"), especialidades)

  // Check requiere derivación
  $("#c_requiere_derivacion").click(function(ev){
    $('#fg_grupo_derivacion').toggle()
      createS2Ajax($('#s_requiere_derivacion'), 'derivables/list')
  })
}

function usrTerapista(tipos_evento, motivos) {
  var motivoS2, extraData
  var selected_type = 'CIE10'

  hideAllObject();
  $('#fg_tipo_evento').show()

  // Tipo de evento
  createS2Data( $("#s_tipo_evento"),
              tipos_evento.filter(function(el){
                  return ( ['consultorio', 'externa', 'telefonica'].indexOf( el.value ) >= 0)
                }))
    .on("change", function (ev) {
      var selected = parseInt($(ev.currentTarget).find("option:selected").val())
      if(selected == 1) {
        showHideControls('consulta')
      } else if ( [4,5].indexOf(selected) >= 0 ) {
        showHideControls('telefonica-externa')
      }

      $('#fg_derivaciones').show()

      // Derivaciones
      var der = createS2Ajax($("#s_derivaciones"), 'eventos/list?grupo_derivacion=terapistas')
      .on('change', function(ev) {

        var selected = parseInt($(ev.currentTarget).find("option:selected").val())

        // Set fields from derivacion
        var extraData = der.select2('data')[0]
        var abuelo = $('#diagnostico_abuelo_id')
          .html('<option value='+ extraData.diagnostico_abuelo_id +'>'+ extraData.abuelo +'</option>')
        var padre = $('#diagnostico_padre_id')
          .html('<option value='+ extraData.diagnostico_padre_id +'>'+ extraData.padre +'</option>')
        var hijo = $('#diagnostico_hijo_id')
          .html('<option value='+ extraData.diagnostico_hijo_id +'>'+ extraData.hijo +'</option>')

        motivoS2.val(extraData.motivo_id).trigger('change')
      })

      // Motivos
      motivoS2 = createS2Data( $("#s_motivo_consulta"),  motivos)

      // Consultorio
      showConsultorios()
      // Select2 familiar (si se selecciona enfermedad inculpable)
      showFamiliar()
      // Checkbox inasistencia con opciones
      showInasistencia()

      // Deshabilitar interconsulta y requiere derivación para el terapista
      $('#bi_interconsulta, #fg_requiere_derivacion').hide()
  })


  // Select2 motivos
  // createS2Data($("#s_motivo_consulta"), motivos)
  // setS2Diagnosticos(selected_type, '', '')



}

function usrPsicologo(tipos_evento, motivos) {
  var motivoS2, extraData
  var selected_type = 'CIE10'

  hideAllObject();
  $('#fg_tipo_evento').show()

  // Tipo de evento
  createS2Data( $("#s_tipo_evento"),
              tipos_evento.filter(function(el){
                  return ( ['consultorio', 'externa', 'telefonica'].indexOf( el.value ) >= 0)
                }))
    .on("change", function (ev) {
      var selected = parseInt($(ev.currentTarget).find("option:selected").val())
      if(selected == 1) {
        showHideControls('consulta')
      } else if ( [4,5].indexOf(selected) >= 0 ) {
        showHideControls('telefonica-externa')
      }

      $('#fg_derivaciones').show()

      // Derivaciones
      var der = createS2Ajax($("#s_derivaciones"), 'eventos/list?grupo_derivacion=psicologos')
      .on('change', function(ev) {

        var selected = parseInt($(ev.currentTarget).find("option:selected").val())

        // Set fields from derivacion
        var extraData = der.select2('data')[0]
        var abuelo = $('#diagnostico_abuelo_id')
          .html('<option value='+ extraData.diagnostico_abuelo_id +'>'+ extraData.abuelo +'</option>')
        var padre = $('#diagnostico_padre_id')
          .html('<option value='+ extraData.diagnostico_padre_id +'>'+ extraData.padre +'</option>')
        var hijo = $('#diagnostico_hijo_id')
          .html('<option value='+ extraData.diagnostico_hijo_id +'>'+ extraData.hijo +'</option>')

        motivoS2.val(extraData.motivo_id).trigger('change')
      })

      // Motivos
      motivoS2 = createS2Data( $("#s_motivo_consulta"),  motivos)

      // Consultorio
      showConsultorios()
      // Select2 familiar (si se selecciona enfermedad inculpable)
      showFamiliar()
      // Checkbox inasistencia con opciones
      showInasistencia()

      // Deshabilitar interconsulta y requiere derivación para el terapista
      $('#bi_interconsulta, #fg_requiere_derivacion').hide()
  })


  // Select2 motivos
  // createS2Data($("#s_motivo_consulta"), motivos)
  // setS2Diagnosticos(selected_type, '', '')



}

function showFamiliar(){
  createS2Data( $("#s_familiar"),
         [ { id: 'hijo', text: 'Hijo' },
            { id: 'conyugue', text: 'Cónyuge' },
            { id: 'madre', text: 'Madre' },
            { id: 'padre', text: 'Padre' } ],
        { placeholder: 'Seleccione familiar' })
}

function showConsultorios(){
  // Consultorio
  createS2Data($("#s_consultorio"), [
    { id: "Hornos", text: "Hornos" },
    { id: "Saavedra", text: "Saavedra" },
    { id: "Ultima Milla", text: "Ultima Milla" },
    { id: "Lugano", text: "Lugano" },
    { id: "Almagro", text: "Almagro" },
    { id: "Ituzaingo", text: "Ituzaingo" },
    { id: "Burzaco", text: "Burzaco" },
    { id: "Quilmes", text: "Quilmes" },
    { id: "La Plata", text: "La Plata" },
    { id: "Mar del Plata", text: "Mar del Plata" },
    { id: "Cordoba", text: "Cordoba" },
    { id: "Santa Fe", text: "Santa Fe" },
    { id: "Rosario", text: "Rosario" },
    { id: "Resistencia", text: "Resistencia" }
  ])
}

function showInasistencia(){
  // Cuando chequeo inasistencia debe mostrarse select 2 ausentismo
  $("#c_inasistencia").click( function() {
    $("#fg_ausentismo").toggle()
  } )

  // Select2 ausentismo, al principio queda oculto
  createS2Data( $("#s_inasistencia"),
    [ { id: 'justificada', text: 'Justificada' },
      { id: 'injustificada', text: 'Injustificada' },
      { id: 'pendiente', text: 'Pendiente de justificar' } ])
}

function showHideElementIf(element, haystack, needle){
  if( haystack.indexOf(needle) >= 0 ){
    element.show()
  }else{
    element.hide()
  }
}

function showHideControls(tipoEvento) {
  hideAllObject();
  switch (tipoEvento) {

    case 'consulta':
    visible = '#fg_consultorio,  #fg_tipo_evento, #fg_motivo_consulta, #fg_fecha_consulta, \
              #fg_evolucion_consulta, #fg_inasistencia, #bi_diagnostico, \
              #fg_diagnostico_abuelo, #fg_diagnostico_padre, #fg_diagnostico_hijo, \
              #bi_interconsulta, #fg_interconsulta_detalle, \
              #fg_interconsulta, #fg_especialidad, #fg_especialidad, \
              #fg_requiere_derivacion, #fg_fecha_accion, #fg_adjuntar_documentos'
    break;

    case 'solicitud':
      visible = '#fg_tipo_evento, #fg_motivo_consulta, #fg_tipo_solicitud, \
                #fg_telefono, #fg_evolucion_consulta, #fg_direccion, \
                #fg_solicitante, #fg_prestador'

      break;
    case 'telefonica-externa':
      visible = '#fg_tipo_evento, #fg_motivo_consulta, #fg_fecha_consulta, \
                  #fg_evolucion_consulta, #fg_inasistencia, #bi_diagnostico, \
                  #fg_diagnostico_abuelo, #fg_diagnostico_padre, #fg_diagnostico_hijo, \
                  #bi_interconsulta, #fg_interconsulta_detalle, \
                  #fg_interconsulta, #fg_especialidad, #fg_especialidad, \
                  #fg_requiere_derivacion, #fg_fecha_accion, #fg_adjuntar_documentos'
      break;

    case 'periferica-domiciliaria':
      visible = '#fg_tipo_evento, #fg_motivo_consulta, #fg_fecha_consulta, \
                #fg_solicitud, \
                #fg_evolucion_consulta, #fg_inasistencia, #bi_diagnostico, \
                #fg_diagnostico_abuelo, #fg_diagnostico_padre, #fg_diagnostico_hijo, \
                #bi_interconsulta, #fg_interconsulta_detalle, \
                #fg_interconsulta, #fg_especialidad, #fg_especialidad, \
                #fg_requiere_derivacion, #fg_fecha_accion, #fg_adjuntar_documentos'
      break;

  }
  $(visible).show() // Mostrar todos los campos de solicitud
}

function setS2Diagnosticos(selected_type, abuelo_id, padre_id) {

  var abuelos, padres, hijos
  var selected_abuelo, selected_padre, selected_hijo
  var abuelo_id = ''
  var padre_id = ''
  var hijo_id = ''

  // Select2 diagnóstico abuelo
  abuelos = createS2Ajax( $('#diagnostico_abuelo_id'),
                          'diagnosticos/abuelos?tipo=' + selected_type )

  abuelos.on('change', function(){
    selected_abuelo = abuelos.select2('data')[0]
    abuelo_id = selected_abuelo.id
    padres = createS2Ajax( $('#diagnostico_padre_id'),
                          'diagnosticos/padres?id=' + abuelo_id )
  })

  // Select2 diagnóstico padre
  padres = createS2Ajax( $('#diagnostico_padre_id'),
                          'diagnosticos/padres?id=' + abuelo_id )
  padres.on('change', function(){
    selected_padre = padres.select2('data')[0]
    padre_id = selected_padre.id
    hijos = createS2Ajax( $('#diagnostico_hijo_id'), 'diagnosticos/hijos?id=' + padre_id )
  })

  hijos = createS2Ajax( $('#diagnostico_hijo_id'), 'diagnosticos/hijos?id=' + padre_id )
  hijos.trigger('change', function(){
    selected_hijo = hijos.select2('data')[0]
    padre_id = selected_hijo.padre_id
    hijos.select2().val(padre_id)
  //   padres = createS2Ajax( $('#diagnostico_padre_id'),
  //                           'diagnosticos/padres=id=' + padre_id )
  //                           .val(padre_id)
  //                           .trigger('change')
  })
}

function setMotivoFromSolicitudId(id) {
    var deffered =[
      apiGet('eventos/'+ id)
    ]
    $.when.apply($, deffered)
     .then( function(evento){
       $("#s_motivo_consulta")
          .select2()
          .val(evento.motivo_id)
          .trigger('change')
    })
}

function hideAllObject() {
  $('.form-group, .box.box-info').hide()
}

function showInfoART(){
  $("#bi_info_art")
    .show()
    .find(".form-group")
    .show()

  createS2Ajax($('#s_siniestro'), 'eventos/list?motivo=accidente-de-trabajo')

  createS2Data($("#s_lugar_accidente"),
                [ { id: 'En el trabajo', text: 'En el trabajo' },
                  { id: 'En otro centro o lugar de trabajo', text: 'En otro centro o lugar de trabajo' },
                  { id: 'ir o volver del trabajo', text: 'Ir o volver del trabajo' },
                  { id: 'desplazamiento en dia laboral', text: 'desplazamiento en dia laboral' },
                  { id: 'Otro', text: 'Otro' } ])

  createS2Ajax($('#s_agente_material'), 'agentes_materiales/list')
  createS2Ajax($('#s_formadiagnostico'), 'formas_diagnostico/list')
  createS2Ajax($('#s_forma_accidente'), 'formas_accidente/list')

}

function hideART(){
  $("#bi_info_art")
    .hide()
}

function createS2Data(selector, dataArray, extraOptions){
  var options = {
    data: dataArray
  }
  var allOptions = Object.assign(options, extraOptions)
  return selector.select2(allOptions)
}

function createS2Ajax(selector, endpoint){
  return selector.select2(
    { ajax: {
      url:  baseUrl + endpoint,
      processResults: function (data, page) {
        return { results: data };
      }}}).trigger('change')
}

function mapS2(arr, id, text, extra){
  if(arr.length){
    return arr.map(function(el){
      result = {
        id: el[id],
        text: el[text]
      }
      // Extra attributes
      $.each(extra, function(idx, e){
        result[e]= el[e]
      })
      return result
    })
  }
}

function formatTelAndAddresses(data){
  if(!data.descripcion){
    return data.text
  }
  return  $('<span>'+ data.text + ' <b>('+ data.descripcion +')</b></span>')
}

function getAjaxHtml(path){
  return jQuery.get( webUrl + path)
}

function apiGet(path){
  return $.ajax({
    url: baseUrl + path,
    method: 'GET',
    dataType: 'json'
  })
}

function apiPost(path, data){
  return $.ajax({
    url: baseUrl + path,
    method: 'POST',
    dataType: 'json',
    data: data
  })
}

function createEvent(form, rules){

  form
    .bootstrapValidator(rules, { live: 'disabled' })
    .on('success.form.bv', function(e) {
      e.preventDefault()
      var data = form.serializeArray().filter(function(el){
        element = $("[name="+ el.name +"]")
        isVisible = element.is(":visible")
        isTypeHidden = element.attr('type') == 'hidden'
          return ((isVisible || isTypeHidden) ? el : false)
      })

      apiPost('eventos', data )
      .done(function(result){
        $('#modal-success').modal('show');
      })
      .fail(function(error){
        $('#modal-danger').modal('show');
      })
      .always(function(){
        $('#newEvent').modal('hide');
      })
    })
}

function getSuggestOptions( url, params ){
  return {
    minimumInputLength: 2,
    placeholder: 'Buscar diagnóstico..',
    ajax: {
      dataType: 'json',
      url: baseUrl + url,
      delay: 250,
      data: function (params) {
        return {
          q: params.term
        };
      },
      processResults: function (data, params) {
        return {
          results: data
        };
      },
      cache: true
    }
  }
}
