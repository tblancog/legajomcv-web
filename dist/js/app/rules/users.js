var rulesUsers = {
  message: 'This value is not valid',
  feedbackIcons: {
    valid: 'glyphicon glyphicon-ok',
    invalid: 'glyphicon glyphicon-remove',
    validating: 'glyphicon glyphicon-refresh'
  },
  fields: {
    userad:{
      validators: {
        notEmpty: {
          message: 'No es posible guardar sin usuario de ad'
        }
      }
    },
    email: {
      validators: {
          notEmpty: {
              message: 'Se requiere el email'
          },
          emailAddress: {
              message: 'Se requiere un email válido'
          }
      }
    },
    puesto: {
      validators: {
        notEmpty: {
          message: 'Puesto requerido'
        }
      }
    },
    sector: {
      validators: {
        notEmpty: {
          message: 'Se requiere el sector'
        }
      }
    },
    gerencia: {
      validators: {
        notEmpty: {
          message: 'Se requiere la gerencia'
        }
      }
    },
    role_id: {
      validators: {
        notEmpty: {
          message: 'Al menos debe tener un rol'
        }
      }
    }
  }
};
