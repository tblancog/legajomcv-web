var rules = {
  live: 'disabled',
  message: 'This value is not valid',
  feedbackIcons: {
    valid: 'glyphicon glyphicon-ok',
    invalid: 'glyphicon glyphicon-remove',
    validating: 'glyphicon glyphicon-refresh'
  },
  fields: {
    tipo_evento_id: {
      validators: {
        notEmpty: {
          message: 'Tipo de evento requerido'
        }
      }
    },
    tipo_solicitud: {
      validators: {
        notEmpty: {
          message: 'Tipo de solicitud requerido'
        }
      }
    },
    consultorio: {
      validators: {
        notEmpty: {
          message: 'Consultorio requerido'
        }
      }
    },
    fecha_consulta: {
      validators: {
        notEmpty: {
          message: 'Fecha requerida'
        }
      }
    },
    s_tipo_diagnostico: {
      validators: {
        notEmpty: {
          message: 'Tipo de diagnóstico requerido'
        }
      }
    },
    diagnostico_abuelo_id: {
      validators: {
        notEmpty: {
          message: 'Diagnostico de nivel 1 requerido'
        }
      }
    },
    diagnostico_padre_id: {
      validators: {
        notEmpty: {
          message: 'Diagnostico de nivel 2 requerido'
        }
      }
    },
    diagnostico_hijo_id: {
      validators: {
        notEmpty: {
          message: 'Diagnostico de nivel 3 requerido'
        }
      }
    },
    ausentismo: {
      validators: {
        notEmpty: {
          message: 'Consultorio requerido'
        }
      }
    },
    motivo_id: {
      validators: {
        notEmpty: {
          message: 'Motivo de consulta requerido'
        }
      }
    },
    familiar: {
      validators: {
        notEmpty: {
          message: 'Familiar requerido'
        }
      }
    },
    telefono_id: {
      validators: {
        notEmpty: {
          message: 'Teléfono requerido'
        }
      }
    },
    direccion_id: {
      validators: {
        notEmpty: {
          message: 'Dirección requerido'
        }
      }
    },
    solicitante: {
      validators: {
        notEmpty: {
          message: 'Solicitante requerido'
        }
      }
    },
    prestador_id: {
      validators: {
        notEmpty: {
          message: 'Prestador requerido'
        }
      }
    },
    derivacion_id: {
      validators: {
        notEmpty: {
          message: 'Derivación requerida'
        }
      }
    },
    fecha_accion: {
      validators: {
        notEmpty: {
          message: 'Fecha acción'
        }
      }
    },
    detalle_accion: {
      validators: {
        notEmpty: {
          message: 'Detalle acción requerido'
        }
      }
    },
    solicitud_id: {
      validators: {
        notEmpty: {
          message: 'Fecha acción'
        }
      }
    },
    solicitud_id: {
      validators: {
        notEmpty: {
          message: 'Solicitud requerida'
        }
      }
    },
    s_derivaciones: {
      validators: {
        notEmpty: {
          message: 'Derivaciones requeridas'
        }
      }
    }
  }
}
