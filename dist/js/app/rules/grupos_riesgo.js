var rulesGruposRiesgo = {
  message: 'This value is not valid',
  feedbackIcons: {
    valid: 'glyphicon glyphicon-ok',
    invalid: 'glyphicon glyphicon-remove',
    validating: 'glyphicon glyphicon-refresh'
  },
  fields: {
    nombre: {
      validators: {
        notEmpty: {
          message: 'Nombre requerido'
        }
      }
    }
  }
};
