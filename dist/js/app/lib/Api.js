function Api() {

	this.get = function (url,data) {

		params = { 'url':url, 'type':'get'}

		if (data!=null) {
			 params.data = data
		}

		return $.ajax(params);
	}


	this.post=function  (url, data) {

		return $.ajax({'url':url,
					   'type':'post',
					   'dataType':'json',
					   'data':data
		});
	}

}