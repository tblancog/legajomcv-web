function MotivoConsultaModel() {


	let _baseUrl = baseUrl + "roles/"
	let api = new Api();

	this.get = function (perfil_id, tipo_evento_id) {
		return api.get(_baseUrl + perfil_id + '/tipos/'
														+ tipo_evento_id + "/motivos")
	}
}
