function FamiliarModel() {

	this.get = function () {

    return $([
      {'id': 'hijo', 'text':'Hijo'},
      {'id': 'conyugue', 'text':'Cónyugue'},
      {'id': 'madre', 'text':'Madre'},
      {'id': 'padre', 'text':'Padre'}
	  ]).promise()
  }
}
