function ConsultorioModel() {

	this.get = function () {

    return $([
			{ id: "Hornos", text: "Hornos" },
			{ id: "Saavedra", text: "Saavedra" },
			{ id: "Ultima Milla", text: "Ultima Milla" },
			{ id: "Lugano", text: "Lugano" },
			{ id: "Almagro", text: "Almagro" },
			{ id: "Ituzaingo", text: "Ituzaingo" },
			{ id: "Burzaco", text: "Burzaco" },
			{ id: "Quilmes", text: "Quilmes" },
			{ id: "La Plata", text: "La Plata" },
			{ id: "Mar del Plata", text: "Mar del Plata" },
			{ id: "Cordoba", text: "Cordoba" },
			{ id: "Santa Fe", text: "Santa Fe" },
			{ id: "Rosario", text: "Rosario" },
			{ id: "Resistencia", text: "Resistencia" }
	  ]).promise()
  }
}
