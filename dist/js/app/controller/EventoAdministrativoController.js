function EventoAdministrativoController(params) {


	// Modelos a utilizar
	let eventoFormView = new EventoFormView();
	let model = new EventoAdministrativoModel();

	let empleadoModel = new EmpleadoModel();
	let tipoEventoModel = new TipoEventoModel()
	let motivoConsultaModel = new MotivoConsultaModel()
	let prestadorModel = new PrestadorModel()
	let familiarModel = new FamiliarModel()


	// reglas bootstrapValidator
	let rules = {
	  live: 'disabled',
	  message: 'This value is not valid',
	  feedbackIcons: {
	    valid: 'glyphicon glyphicon-ok',
	    invalid: 'glyphicon glyphicon-remove',
	    validating: 'glyphicon glyphicon-refresh'
	  },
	  fields: {
	    tipo_evento_id: {
	      validators: {
	        notEmpty: {
	          message: 'Tipo de evento requerido'
	        }
	      }
	    },
	    tipo_solicitud: {
	      validators: {
	        notEmpty: {
	          message: 'Tipo de solicitud requerido'
	        }
	      }
	    },
	    consultorio: {
	      validators: {
	        notEmpty: {
	          message: 'Consultorio requerido'
	        }
	      }
	    },
	    fecha_consulta: {
	      validators: {
	        notEmpty: {
	          message: 'Fecha requerida'
	        }
	      }
	    },
	    s_tipo_diagnostico: {
	      validators: {
	        notEmpty: {
	          message: 'Tipo de diagnóstico requerido'
	        }
	      }
	    },
	    diagnostico_abuelo_id: {
	      validators: {
	        notEmpty: {
	          message: 'Diagnostico de nivel 1 requerido'
	        }
	      }
	    },
	    diagnostico_padre_id: {
	      validators: {
	        notEmpty: {
	          message: 'Diagnostico de nivel 2 requerido'
	        }
	      }
	    },
	    diagnostico_hijo_id: {
	      validators: {
	        notEmpty: {
	          message: 'Diagnostico de nivel 3 requerido'
	        }
	      }
	    },
	    ausentismo: {
	      validators: {
	        notEmpty: {
	          message: 'Consultorio requerido'
	        }
	      }
	    },
	    motivo_id: {
	      validators: {
	        notEmpty: {
	          message: 'Motivo de consulta requerido'
	        }
	      }
	    },
	    familiar: {
	      validators: {
	        notEmpty: {
	          message: 'Familiar requerido'
	        }
	      }
	    },
	    telefono_id: {
	      validators: {
	        notEmpty: {
	          message: 'Teléfono requerido'
	        }
	      }
	    },
	    direccion_id: {
	      validators: {
	        notEmpty: {
	          message: 'Dirección requerido'
	        }
	      }
	    },
	    solicitante: {
	      validators: {
	        notEmpty: {
	          message: 'Solicitante requerido'
	        }
	      }
	    },
	    prestador_id: {
	      validators: {
	        notEmpty: {
	          message: 'Prestador requerido'
	        }
	      }
	    },
	    derivacion_id: {
	      validators: {
	        notEmpty: {
	          message: 'Derivación requerida'
	        }
	      }
	    },
	    fecha_accion: {
	      validators: {
	        notEmpty: {
	          message: 'Fecha acción'
	        }
	      }
	    },
	    detalle_accion: {
	      validators: {
	        notEmpty: {
	          message: 'Detalle acción requerido'
	        }
	      }
	    },
	    solicitud_id: {
	      validators: {
	        notEmpty: {
	          message: 'Fecha acción'
	        }
	      }
	    },
	    solicitud_id: {
	      validators: {
	        notEmpty: {
	          message: 'Solicitud requerida'
	        }
	      }
	    },
	    s_derivaciones: {
	      validators: {
	        notEmpty: {
	          message: 'Derivaciones requeridas'
	        }
	      }
	    }
	  }
	}

	//names de controles a guardar en la base de datos

	let fields = ['tipo_evento_id',
		          'tipo_solicitud',
		  	      'motivo_id',
			      'telefono_id',
			      'direccion_id',
			      'solicitante',
			      'prestador_id',
			      'evolucion',
			      'empleado_id',
						'familiar'
	]


	// agrupadores de cada control

	let groups = [

		'#fg_tipo_evento',
		'#fg_motivo_consulta',
		'#fg_telefono',
		'#fg_direccion',
		'#fg_solicitante',
		'#fg_prestador',
		'#fg_evolucion_consulta',
		'#fg_tipo_solicitud',

	]

	this.init = function () {

  	  	let promises = new Array()

  	  	promises.push(eventoFormView.get())
  	  	promises.push(tipoEventoModel.get(params.roleId))
  	  	promises.push(prestadorModel.get('administrativo'))
  	  	promises.push(empleadoModel.get(1,'administrativo'))
  	  	promises.push(familiarModel.get())

  		$.when.apply($, promises).then( function( resHtml,
				 																		resTipoEvento,
			  																		resPrestador,
				 																		resEmpleado,
																						resFamiliar
																					 ){

            $('#modal-content').html(resHtml[0])

            $('#modal-content').modal('show')

						console.log(params.userId)
            eventoFormView.bootstrapValidator(rules, fields, params.userId, model)

            eventoFormView.hideAll()

			let tipoEventoS2 = eventoFormView.select2TipoEvento(resTipoEvento[0])
			let prestadorS2 = eventoFormView.select2Prestador(resPrestador[0])
			let direccionS2 = eventoFormView.select2Direccion(resEmpleado[0].direcciones)
			let telefonoS2 = eventoFormView.select2Telefono(resEmpleado[0].telefonos)
			let familiarS2 = eventoFormView.select2Familiar(resFamiliar)

			eventoFormView.showTipoEvento()

			tipoEventoS2.on('change', function () {

				selected = tipoEventoS2.select2('data')[0]

				if (selected.id !='' || typeof selected.id != 'undefined' ) {

					motivoConsultaModel.get(params.roleId, selected.id)
						.done(function(resMotivoConsulta) {

								eventoFormView.select2MotivoConsulta(resMotivoConsulta, true)
								eventoFormView.showElements(groups)

						}).fail(function (err) {

							alert("hubo un error al cargar el motivo de consulta")

						})

				} else {

					eventoFormView.hideElements(groups)
					eventoFormView.showTipoEvento()
				}
			})

			// Change de motivo

		}).fail(function(err) {

			alert('hubo un error al obtener los datos para el tipo de evento')
		})
	}
}
