function EventoMedicoController(params) {


	// Modelos a utilizar
	let eventoFormView = new EventoFormView();
	let model = new EventoMedicoModel();

	let empleadoModel = new EmpleadoModel();
	let tipoEventoModel = new TipoEventoModel()
	let consultorioModel = new ConsultorioModel()
	let motivoConsultaModel = new MotivoConsultaModel()
	let familiarModel = new FamiliarModel()

	// reglas bootstrapValidator
	let rules = {
		live: 'disabled',
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			tipo_evento_id: {
				validators: {
					notEmpty: {
						message: 'Tipo de evento requerido'
					}
				}
			},
			tipo_solicitud: {
				validators: {
					notEmpty: {
						message: 'Tipo de solicitud requerido'
					}
				}
			},
			consultorio: {
				validators: {
					notEmpty: {
						message: 'Consultorio requerido'
					}
				}
			},
			fecha_consulta: {
				validators: {
					notEmpty: {
						message: 'Fecha requerida'
					}
				}
			},
			s_tipo_diagnostico: {
				validators: {
					notEmpty: {
						message: 'Tipo de diagnóstico requerido'
					}
				}
			},
			diagnostico_abuelo_id: {
				validators: {
					notEmpty: {
						message: 'Diagnostico de nivel 1 requerido'
					}
				}
			},
			diagnostico_padre_id: {
				validators: {
					notEmpty: {
						message: 'Diagnostico de nivel 2 requerido'
					}
				}
			},
			diagnostico_hijo_id: {
				validators: {
					notEmpty: {
						message: 'Diagnostico de nivel 3 requerido'
					}
				}
			},
			ausentismo: {
				validators: {
					notEmpty: {
						message: 'Consultorio requerido'
					}
				}
			},
			motivo_id: {
				validators: {
					notEmpty: {
						message: 'Motivo de consulta requerido'
					}
				}
			},
			familiar: {
				validators: {
					notEmpty: {
						message: 'Familiar requerido'
					}
				}
			},
			telefono_id: {
				validators: {
					notEmpty: {
						message: 'Teléfono requerido'
					}
				}
			},
			direccion_id: {
				validators: {
					notEmpty: {
						message: 'Dirección requerido'
					}
				}
			},
			solicitante: {
				validators: {
					notEmpty: {
						message: 'Solicitante requerido'
					}
				}
			},
			prestador_id: {
				validators: {
					notEmpty: {
						message: 'Prestador requerido'
					}
				}
			},
			derivacion_id: {
				validators: {
					notEmpty: {
						message: 'Derivación requerida'
					}
				}
			},
			fecha_accion: {
				validators: {
					notEmpty: {
						message: 'Fecha acción'
					}
				}
			},
			detalle_accion: {
				validators: {
					notEmpty: {
						message: 'Detalle acción requerido'
					}
				}
			},
			solicitud_id: {
				validators: {
					notEmpty: {
						message: 'Fecha acción'
					}
				}
			},
			solicitud_id: {
				validators: {
					notEmpty: {
						message: 'Solicitud requerida'
					}
				}
			},
			s_derivaciones: {
				validators: {
					notEmpty: {
						message: 'Derivaciones requeridas'
					}
				}
			}
		}
	}

	//names de controles a guardar en la base de datos

	let fields = [
				'tipo_evento_id',
				'motivo_id',
				'familiar',
				'fe_fecha_consulta',
				'evolucion',
				'c_inasistencia',
				'ausentismo'
	]


	// agrupadores de cada control

	let groups = [

		'#fg_tipo_evento',
		'#fg_motivo_consulta',
		'#fg_consultorio',
		'#fg_fecha_consulta',
		'#fg_fecha_accion',
		'#fg_evolucion_consulta',
		'#fg_inasistencia',
		// '#fg_ausentismo',

	]

	this.init = function () {

  	  	let promises = new Array()

  	  	promises.push(eventoFormView.get())
  	  	promises.push(tipoEventoModel.get(params.roleId))
  	  	promises.push(consultorioModel.get())
				promises.push(motivoConsultaModel.get(params.roleId))
  	  	promises.push(empleadoModel.get(1,'medico'))

  		$.when.apply($, promises).then( function( resHtml,
				 																		resTipoEvento,
				 																		resConsultorio,
				 																		resMotivoConsulta,
				 																		resEmpleado
																					 ){

            $('#modal-content').html(resHtml[0])

            $('#modal-content').modal('show')

            eventoFormView.bootstrapValidator(rules, fields, params.userId, model)

            eventoFormView.hideAll()
			let tipoEventoS2 = eventoFormView.select2TipoEvento(resTipoEvento[0])
			let consultorioS2 = eventoFormView.select2Consultorio(resConsultorio)
			let motivoConsultaS2 = eventoFormView.select2MotivoConsulta(resMotivoConsulta[0])

			// eventoFormView.showTipoEvento()
			eventoFormView.showElements(groups)
			eventoFormView.flatpickrFecha(['#fe_fecha_consulta', '#fe_fecha_accion'],
																		{ enableTime: true })

			tipoEventoS2.on('change', function () {

				selected = $(this).select2('data')[0]

				if (selected.id !='' || typeof selected.id != 'undefined' ) {

					motivoConsultaModel.get(params.roleId, selected.id)
						.done(function(resMotivoConsulta) {

								eventoFormView.select2MotivoConsulta(resMotivoConsulta, true)

						}).fail(function (err) {

							alert("hubo un error al cargar el motivo de consulta")

						})
			//
				} else {

					// eventoFormView.hideElements(groups)
					// eventoFormView.showTipoEvento()
				}
			})

		}).fail(function(err) {

			alert('hubo un error al obtener los datos para el tipo de evento')
		})
	}

}
