function ApiMain() {

	this.getTiposEventos = function (roleId) {

		return $([{"id": 1, "text":"tipoEvento1"}]).promise()
	}
}


function Controller(config) {



	let apiMain = new ApiMain()

	//let config = config
	
	let rules = {

	  live: 'disabled',
	  message: 'This value is not valid',
	  feedbackIcons: {
	    valid: 'glyphicon glyphicon-ok',
	    invalid: 'glyphicon glyphicon-remove',
	    validating: 'glyphicon glyphicon-refresh'
	  },
	  fields: {
	    tipo_evento_id: {
	      validators: {
	        notEmpty: {
	          message: 'Tipo de evento requerido'
	        }
	      }
	    },
	    tipo_solicitud: {
	      validators: {
	        notEmpty: {
	          message: 'Tipo de solicitud requerido'
	        }
	      }
	    },
	    motivo_id: {
	      validators: {
	        notEmpty: {
	          message: 'Motivo de consulta requerido'
	        }
	      }
	    },
	    telefono_id: {
	      validators: {
	        notEmpty: {
	          message: 'Teléfono requerido'
	        }
	      }
	    },
	    direccion_id: {
	      validators: {
	        notEmpty: {
	          message: 'Dirección requerido'
	        }
	      }
	    },
	    solicitante: {
	      validators: {
	        notEmpty: {
	          message: 'Solicitante requerido'
	        }
	      }
	    },
	    prestador_id: {
	      validators: {
	        notEmpty: {
	          message: 'Prestador requerido'
	        }
	      }
	    }
	  
	  }
	}


	let fields = ['tipo_evento_id',
		          'tipo_solicitud',
		  	      'motivo_id',
			      'telefono_id',
			      'direccion_id',
			      'solicitante',
			      'prestador_id',
			      'evolucion'
	]



	function postEventoRequest() {

		return $.ajax({'url':baseUrl + 'eventos',
					   'type':'post',
					   'data': getJsonDom(fields, [{'name':'user_id','value':config.userId}])

		})
	}



	function getJsonData() {

		let result = []

		fields.forEach(function(field) {

			result.push("'" + field + "':'" + $("[name='"+field+"']").val() + "'")
		})

		result.push("'user_id:'" + config.userId + "'")

		return '{' + result.join(',') + '}'
	}
	



	populateHtml = function() {

		// ocultamos todos los controles del formulario

		$('.form-group, .box.box-info').hide()

		// mostramos el tipo de evento

		$('#fg_tipo_evento').show()


		let form = $('#eventForm')

		let selectTiposEventos = select2WithPromise($('#s_tipo_evento'),
											   		apiMain.getTiposEventos(config.roleId));
	
		// bind tiposEventos

		console.log(selectTiposEventos)

		selectTiposEventos.on('change', function (e) {

			let tipoEventoId = parseInt($(e.currentTarget).find('option:selected').val())

			if (!$.isNumeric(tipoEventoId)) {

				//ocultar

				return;
			}


			let requestMotivosConsulta = apiMain.getMotivosConsulta(config.roleId, tipoEventoId)
															
			let selectMotivosConsulta = select2WithPromise($('#s_motivo_consulta'), requestMotivosConsulta)

			// mostrar controles

		})


		// bind bootstrap validator
		form.bootstrapValidator(rules, { live: 'disabled' })

			.on('success.form.bv', function(e) {
	  		
	  			e.preventDefault()
	  		
	  			postEventoRequest().done(function(result){
	    			$('#modal-success').modal('show');
	  			}).fail(function(error){
   					$('#modal-danger').modal('show');
	  			}).always(function(){
		    		$('#newEvent').modal('hide');
	  			})
    	})
	}


	this.init = function () {

			
  	  	var deffered = [


  	  		$.ajax({url: webUrl + 'events/new_event.php',
  	  				type: 'get',
  	  				dataType: 'html'
  	  		})
    		
    	
  		]

  		$.when.apply($, deffered).then( function( modalContent ){

  		
 		
            $('#modal-content').html(modalContent)

            $('#modal-content').modal('show')

            populateHtml()



        })
	}

}