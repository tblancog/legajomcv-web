var s2, ajax;

function loadAbm(form, rules,id)
{
  form
    .bootstrapValidator(rules) // "rules" object in rules.js
    .on('success.form.bv', function(e) {
      e.preventDefault() // Prevent form submission
      settings = getFormMeta(meta.entity, abmAction, id)  // presente en ui.js
      var bv = form.data('bootstrapValidator') // Get the BootstrapValidator instance
      var datos = form.serialize()
      var url = settings.formAction
      var method = settings.method
      ajax = $.ajax({
        dataType: "json",
        url: url,
        method: method,
        data: datos
      })

      ajax.then(
        // sucesss response
        function(ajax) {
          $(".close").click() // modal close

          // Update datatable with "dt" var present in datatables.init.js
          dt.ajax.reload()
          // Sets up a new alert box
          var alert = new Alert(abmAction, meta.title, null)
          $("#alert_ok").html(alert.render)
            .show()
        },
        // error response
        function(err) {
          var alert = new Alert("error", meta.title, err.responseJSON.errors)
          $("#alert_error").html(alert.render)
            .show()
        }
      ).always(function() {
        $("#loader").hide()
      })
    })
}
