/** Events **/
$(document).ready(function() {
  meta = getEntityMeta( $('#entity').val() )
  $('#list-header').text(meta.header)
  $('#newBtn, .breadcrumb .active').text(meta.title)
})

// // Destroy modal
$("#alta, #edicion").on("hide.bs.modal", function(){
  $(this).find('.modal-content').html("");
})

// Alta button clicked
$("[data-target='#alta']").click(function(e){
  abmAction = 'alta'
})
