$(function () {

   
    $.ajaxSetup({
      beforeSend:function(){
        $("#loader").show();
      },
      success:function(res){
        $("#loader").hide();
      } });


    //Muestra la pagina principal cuando se loguea el usuario
    $("#home").click(function(event) {
      $("#contentWrapper").load('home.php');
    });

    //Formulario que se muestra cuando no se encuentra un usuario ni en SAP ni en la base de la aplicacion
    $("#empleado_form_alta").click(function(event) {
      $("#contentWrapper").load('empleado_form_alta.php');
    });

    //Pagina que muestra el perfil del empleado
    $("#empleado_perfil").click(function(event) {
      $("#contentWrapper").load('empleado_perfil.php');
    });

    //Formulario nuevo evento
    $("#evento_nuevo").click(function(event) {
      $("#contentWrapper").load('evento_nuevo.php');
    });

    //Reporte de ejemplo
    $("#reporte_1").click(function(event) {
      $("#contentWrapper").load('reporte_1.php');
    });

    //Muestra la pagina para suscribirse a las alertas
    $("#suscripcion_alertas").click(function(event) {
      $("#contentWrapper").load('suscripcion_alertas.php');
    });

    //Muestra la pagina para suscribirse a las alertas haciendo clic en el header de noticias
    $("#suscripcion_alertas_notice").click(function(event) {
      $("#contentWrapper").load('suscripcion_alertas.php');
    });

    //Muestra el perfil del usuario logueado
    $("#usr_logueado_perfil").click(function(event) {
      $("#contentWrapper").load('usr_logueado_perfil.php');
    });

    //-- FORMULARIOS DEL BACK-END ----------------------------------------------------------------------------\\
    //-- bk_lista.php, bk_alta, bk_edicion, (GENERICO A TODOS LOS FORMULARIOS)  ------------------------------\\
    //-- bk_usrs_alta, bk_usrs_edicion, (FORMULARIO DE ALTA Y EDICION DE USUARIOS)  --------------------------\\
    //-- bk_grupo_riesgo_alta, bk_grupo_riesgo_edicion (FORMULARIO DE ALTA Y EDICION DE GRUPOS DE RIESGO)  ---\\

    //Usuarios
    $("#bk_usrs_list").click(function(event) {
      $("#contentWrapper").load('abm/bk_lista.php', { id: 'users' });
    });

    //Antecedentes
    $("#bk_antecedentes_medicos_list").click(function(event) {
      $("#contentWrapper").load('abm/bk_lista.php', { id: 'antecedentes' });
    });

    //Grupo de riesgo
    $("#bk_grupo_riesgo_list").click(function(event) {
      $("#contentWrapper").load('abm/bk_lista.php', { id: 'grupos_riesgo' });
    });

    //Motivos de consulta
    $("#bk_motivo_consulta_list").click(function(event) {
      $("#contentWrapper").load('abm/bk_lista.php', { id: 'motivos' });
    });

    //Tipos de intervencion
    $("#bk_tipo_intervencion_list").click(function(event) {
      $("#contentWrapper").load('abm/bk_lista.php', { id: 'tipos_intervencion' });
    });

    //Elementos utilizados
    $("#bk_elementos_utilizados_list").click(function(event) {
      $("#contentWrapper").load('abm/bk_lista.php', { id: 'elementos' });
    });

    //Prestadores medicos
    $("#bk_prestador_list").click(function(event) {
      $("#contentWrapper").load('abm/bk_lista.php', { id: 'prestadores' });
    });
    
});