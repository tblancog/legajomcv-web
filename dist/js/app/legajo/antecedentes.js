function Antecedentes(config, empleadoId) {

    var parent = this

    this.config = config

    this.empleadoId = empleadoId

	
    this.saveAntecedentes = function (antecedentes) {

		return $.ajax({	'url': parent.config.baseUrlApi + '/' + parent.empleadoId + '/antecedentes',
						'type': 'post',
            	        'dataType': 'json',
            	        'data': {'antecedentes':antecedentes}
    	});
    }

	this.populate = function (data) {

		$.each(data,function(key,obj) {
			$("#select_antecedentes").append("<option value='" +obj.id + "' selected>"+obj.nombre +"</option>")
		});



		$("#select_antecedentes").select2({

		    minimumInputLength: 2,

		    escapeMarkup: function(m) {return m},

		    language: {
		   	  inputTooShort:  function(){ return "Ingrese Antecedente" },
		   	  noResults: function () { return "El Antecedente no existe";}
		    },
		    placeholder: 'Buscando Antecedente',
		    ajax: {

		        dataType: 'json',

		       
		        url: function(term){
		            return _configLegajoWeb.baseUrlApi + '/' + term.term + '/suggestAntecedentes';
		        },

		        delay: 250,

		        data: function(params) {
		            return {
		                term: params.term
		            }
		        },

		        processResults: function (data, page) {
		            var myResults = [];


		            jQuery.each(data, function (index, item) {
		                myResults.push({
		                    'id': item.id,
		                    'text': item.nombre
		                });
		            });

		            return {
		                results: myResults
		            };
		        }

		    }
		});

		$("#btn_actualizar_antecedentes").click(function() {

			let antecedentes = $("#select_antecedentes").val();


			parent.saveAntecedentes(antecedentes)

		});
	
	}
}