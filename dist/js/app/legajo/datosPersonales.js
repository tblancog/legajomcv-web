function DatosPersonales(config, empleadoId) {

    var parent = this


    var populateDireccionesAlternativas = function (direcciones) {

		$("#lista_direcciones_alternativas").html("")

		$.each(direcciones, function(key,obj) {

			$("#lista_direcciones_alternativas").append('<dd>'+obj.direccion +'<small><span class="text-red"> alternativa</span></small></dd>');

		});
	}



	var populateTelefonosAlternativos = function (telefonos) {

		$("#lista_telefonos_alternativos").html("")

		$.each(telefonos, function(key,obj) {

			$("#lista_telefonos_alternativos").append('<dd>'+obj.numero +'<small><span class="text-red"> alternativo</span></small></dd>');

		});
	}


    this.config = config

    this.empleadoId = empleadoId

	
    this.dataDireccionesAlternativas = function () {

		return $.ajax({	'url': parent.config.baseUrlApi + '/' + parent.empleadoId + '/direccionesAlternativas',
						'type': 'get',
            	        'dataType': 'json'  
    	});
    }



    this.dataTelefonosAlternativos = function () {

		return $.ajax({	'url': parent.config.baseUrlApi + '/' + parent.empleadoId + '/telefonosAlternativos',
						'type': 'get',
            	        'dataType': 'json'  
    	});
    }



    this.saveDireccionAlternativa = function (direccion) {

		return $.ajax({	'url': parent.config.baseUrlApi + '/' + parent.empleadoId + '/direccionesAlternativas',
						'type': 'post',
            	        'dataType': 'json',
            	        'data': {'direccion':direccion}
    	});
    }



    this.saveTelefonoAlternativo = function (telefono) {

		return $.ajax({	'url': parent.config.baseUrlApi + '/' + parent.empleadoId + '/telefonosAlternativos',
						'type': 'post',
            	        'dataType': 'json',
            	        'data': {'telefono':telefono}
    	});
    }


	this.populate = function (data) {

		$("#datopersonal_fecha_nacimiento").html(data.datosPersonales.fecha_nacimiento)
		$("#datopersonal_fecha_ingreso").html(data.datosPersonales.fecha_ingreso)
		$("#datopersonal_genero").html(data.datosPersonales.genero)
		$("#datopersonal_nacionalidad").html(data.datosPersonales.nacionalidad)
		$("#datopersonal_estadocivil").html(data.datosPersonales.estadocivil)
		$("#datopersonal_estadocivil").html(data.datosPersonales.estadocivil)
		$("#datopersonal_perfil").html(data.datosPersonales.perfil)
		$("#datopersonal_jefe_directo").html(data.datosPersonales.jefe_directo)
		$("#datopersonal_referente_rrhh").html(data.datosPersonales.referente_rrhh)
		$("#datopersonal_art").html(data.datosPersonales.art)
		$("#datopersonal_obra_social_prepaga").html(data.datosPersonales.obra_social_prepaga)
		$("#datopersonal_domicilio_laboral").html(data.datosPersonales.domicilio_laboral)
		$("#datopersonal_telefono_laboral").html(data.datosPersonales.telefono_laboral)
		$("#datopersonal_empresa").html(data.datosPersonales.empresa)
		$("#datopersonal_encuadre").html(data.datosPersonales.encuadre)
		$("#datopersonal_art").html(data.datosPersonales.art)
		$("#datopersonal_domicilio_principal").html(data.direccionPrincipal?data.direccionPrincipal.direccion:'')
		$("#datopersonal_telefono_principal").html(data.telefonoPrincipal?data.telefonoPrincipal.numero:'')


		populateDireccionesAlternativas(data.direccionesAlternativas);

		populateTelefonosAlternativos(data.telefonosAlternativos);


		// direccion
		$("#btn_agregar_direccion_alternativa").click(function() {

    		let direccion = $("#txt_direccion_alternativa").val();

			if ($.trim(direccion) =='') {

				alert("Complete el campo de direccion alternativa")

				return
			}

			let promise = parent.saveDireccionAlternativa(direccion)

			promise.done(function(res) {

				$("#txt_direccion_alternativa").val('');

				parent.dataDireccionesAlternativas().done(function(resDireccionesAlternativas) {

					populateDireccionesAlternativas(resDireccionesAlternativas);

				});

			}).fail(function(err) {

				alert("hubo un error al intentar guardar la dirección")

			})

		});


		// telefono
		$("#btn_agregar_telefono_alternativo").click(function() {

    		let telefono = $("#txt_telefono_alternativo").val();

			if ($.trim(telefono) =='') {

				alert("Complete el campo de telefono alternativa")

				return
			}

			let promise = parent.saveTelefonoAlternativo(telefono)

			promise.done(function(res) {


				$("#txt_telefono_alternativo").val('');

				parent.dataTelefonosAlternativos().done(function(resTelefonosAlternativos) {

					populateTelefonosAlternativos(resTelefonosAlternativos);

				});

			}).fail(function(err) {

				alert("hubo un error al intentar guardar el teléfono")

			})

		});
	}
}