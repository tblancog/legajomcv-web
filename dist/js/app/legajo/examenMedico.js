function ExamenMedico(config, empleadoId) {

    var parent = this

 	this.config = config

    this.empleadoId = empleadoId

    this.save = function (data) {


    }

	this.populate = function (data) {

		Dropzone.prototype.submitRequest = function (xhr,formData, files) {
			xhr.setRequestHeader("X-Requested-With","")
			xhr.setRequestHeader("Cache-Control","")
			xhr.setRequestHeader("Access-Control-Allow-Methods", "POST, PUT, PATCH, DELETE, OPTIONS")
            xhr.setRequestHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
			xhr.send(formData);
		}

		
		var dropzone = new Dropzone(".dropzone", 
								    { 
								      'url': parent.config.baseUrlApi + '/1/examenMedico/upload',
								       autoProcessQueue:false
								    
	    });


	    $("#btn_subir_examen").click(function() {

	    	dropzone.options.autoProccessQueue = true
	    	dropzone.processQueue()
	    });
	}
}