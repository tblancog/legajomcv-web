function DatosMedicos(config, empleadoId) {

    var parent = this

 	this.config = config

    this.empleadoId = empleadoId


    


    this.update = function (data) {

		return $.ajax({	'url': parent.config.baseUrlApi + '/' + parent.empleadoId + '/datosMedicos',
						'type': 'post',
            	        'dataType': 'json',
            	        'data': data
    	})
    }
    

	this.populate = function (data) {



		if (data.datosMedicos) {
			let peso = data.datosMedicos.peso.split(".")
			let altura = data.datosMedicos.altura.split(".")

			let peso_kg = 0
			let peso_gr = 0

			let altura_mt = 0
			let altura_cm = 0

			if (altura.length >0) {
				altura_mt = altura[0]
				altura_cm = altura[1]
			}


			if (peso.length >0) {
				peso_kg = peso[0]
				peso_gr = peso[1]
			}


			let tipo_discapacidad = data.datosMedicos.tipo_discapacidad.split(";")



			$.each(tipo_discapacidad,function (key,value) {

				
				$("#tipo_discapacidad option[value='"+value+"']").attr("selected",true)
			})


			$("#grupo_sanguineo").val(data.datosMedicos.grupo_sanguineo)
			$("#donante_organos").val(data.datosMedicos.donante_organos)
			$("#dador_sangre").val(data.datosMedicos.dador_sangre)
			$("#miembro_superhabil").val(data.datosMedicos.miembro_superhabil)
			$("#certificado_discapacidad").val(data.datosMedicos.certificado_discapacidad)
			$("#exposicion_riesgo_laboral").val(data.datosMedicos.exposicion_riesgo_laboral)
			$("#peso_kg").val(peso_kg)
			$("#peso_gr").val(peso_gr)
			$("#altura_mt").val(altura_mt)
			$("#altura_cm").val(altura_cm)

		}



		$("#grupo_sanguineo").select2()
		$("#donante_organos").select2()
		$("#dador_sangre").select2()
		$("#miembro_superhabil").select2()
		$("#certificado_discapacidad").select2()
		$("#tipo_discapacidad").select2()
		$("#exposicion_riesgo_laboral").select2()
		$("#datopersonal_codigo_puesto").html(data.datosPersonales.codigo_puesto)

		$("#btn_actualizar_datos_medicos").click(function() {

			let promise = parent.update($("#form_datos_medicos").serialize())

			promise.done(function(res) {

				alert("se actualizo los datos medicos")

			}).fail(function(err) {

				alert("Hubo un error al actualizar los datos medicos")
			})
		})
			
	}
}