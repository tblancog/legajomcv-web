

function Legajo(config, empleadoId) {

    var parent = this;

    this.empleadoId = empleadoId;

    this.config = config;

    this.getHtml = function () {

    	return $.ajax({ 'url': parent.config.urlFormProfile,
			 			'type': 'get',
			 			'dataType': 'html',
			 		
		});
	}


	
	this.getDataLegajo = function () {

		return $.ajax({	'url': parent.config.baseUrlApi + '/' + parent.empleadoId,
						'type': 'get',
            	        'dataType': 'json'  
    	});
    }

	this.populate = function () {

		
		let portada = new Portada(parent.config, parent.empleadoId);
		let vacunas = new Vacunas(parent.config, parent.empleadoId);
		let antecedentes = new Antecedentes(parent.config, parent.empleadoId);
		let datosPersonales = new DatosPersonales(parent.config, parent.empleadoId);
 		let datosMedicos = new DatosMedicos(parent.config, parent.empleadoId);
 		let examenMedico = new ExamenMedico(parent.config, parent.empleadoId);
 		let historiaMedica = new HistoriaMedica(parent.config, parent.empleadoId);


	
		let promises = [];

		promises.push(parent.getHtml());
		promises.push(historiaMedica.getHtml());
		promises.push(parent.getDataLegajo());
		
		$.when.apply($, promises).then( function(resHtmlMain, resHtmlHistoriaMedica, resDatos) {
			
   		    Handlebars.registerPartial("historiaMedica",resHtmlHistoriaMedica[0])

			let template = Handlebars.compile(resHtmlMain[0])

   		    let htmlCompiled = template({});

			$(parent.config.htmlContent).html(htmlCompiled)

			portada.populate(resDatos[0].datosPersonales);
			vacunas.populate(resDatos[0].vacunasAplicadas);
			antecedentes.populate(resDatos[0].antecedentes);
			datosPersonales.populate(resDatos[0]);
			datosMedicos.populate(resDatos[0]);
			examenMedico.populate(resDatos[0]);


        });
	}
}