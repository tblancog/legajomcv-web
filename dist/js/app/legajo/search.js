function select2BuscarLegajo() {

	$("#buscar_legajo").select2({

	    minimumInputLength: 2,

	    escapeMarkup: function(m) {return m},

	    language: {
	   	  inputTooShort:  function(){ return "Ingrese Legajo, documento, apellido" },
	   	  noResults: function () { return "<a href='javascript:void(0)'><i class='fa fa-plus'></i>&nbsp;NUEVO LEGAJO</a>";}
	    },
	    placeholder: 'Buscando invitado',
	    ajax: {

	        dataType: 'json',

	       
	        url: function(term){
	            return _configLegajoWeb.baseUrlApi + '/' + term.term + '/suggestLegajos';
	        },

	        delay: 250,

	        data: function(params) {
	            return {
	                term: params.term
	            }
	        },

	        processResults: function (data, page) {
	            var myResults = [];


	            jQuery.each(data, function (index, item) {
	                myResults.push({
	                    'id': item.documento,
	                    'text': item.apellido + ", " + item.nombre + "  -  Documento " + item.documento
	                });
	            });

	            return {
	                results: myResults
	            };
	        }

	    }
	}).on('change', function(e){
	    
	    var selected = jQuery(e.currentTarget).find("option:selected")
	    var documento = selected.val()

		Api_FindLegajoRequest(documento).done(function (empleadoResult) {

			let promise

			if (!empleadoResult._existe) {

				alert("no existe")
			
			} else {

				let legajo = new Legajo(_configLegajoWeb, 
										empleadoResult.legajo.id)

				legajo.populate();

			}
		
		}).fail(function (empleadoError) {

			alert("hubo un error al obtener el legajo medico!!!!")

		});
	    
	});
}


select2BuscarLegajo();
