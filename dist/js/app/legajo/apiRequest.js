function Api_FindLegajoRequest(documento, apellido) {

	apellido="sasarasa";
	
	return $.ajax({	'url': _configLegajoWeb.baseUrlApi + '/' + documento + '/' + apellido + '/find',
					'type': 'get',
                    'dataType': 'json'  
    });
}



function Api_GetDatosPersonalesRequest(empleado) {

		
	return $.ajax({	'url': _configLegajoWeb.baseUrlApi + '/' + empleado + '/datosPersonales',
					'type': 'get',
                    'dataType': 'json'  
    });
}



function Api_GetInfoImportanteRequest(empleado) {

		
	return $.ajax({	'url': _configLegajoWeb.baseUrlApi + '/' + empleado + '/infoImportante',
					'type': 'get',
                    'dataType': 'json'  
    });
}


function Api_GetDatosMedicosRequest(empleado) {

		
	return $.ajax({	'url': _configLegajoWeb.baseUrlApi + '/' + empleado + '/datosMedicos',
					'type': 'get',
                    'dataType': 'json'  
    });
}


