function Vacunas(config, empleadoId) {

    var parent = this

    this.config = config

    this.empleadoId = empleadoId

	
    this.saveVacunas = function (vacunas) {

		return $.ajax({	'url': parent.config.baseUrlApi + '/' + parent.empleadoId + '/vacunasAplicadas',
						'type': 'post',
            	        'dataType': 'json',
            	        'data': {'vacunas':vacunas}
    	});
    }

	this.populate = function (data) {

		$.each(data,function(key,obj) {
			$("#select_vacunas").append("<option value='" +obj.id + "' selected>"+obj.nombre +"</option>")
		});



		$("#select_vacunas").select2({

		    minimumInputLength: 2,

		    escapeMarkup: function(m) {return m},

		    language: {
		   	  inputTooShort:  function(){ return "Ingrese Vacuna" },
		   	  noResults: function () { return "La Vacuna no existe";}
		    },
		    placeholder: 'Buscando Vacuna',
		    ajax: {

		        dataType: 'json',

		       
		        url: function(term){
		            return _configLegajoWeb.baseUrlApi + '/' + term.term + '/suggestVacunas';
		        },

		        delay: 250,

		        data: function(params) {
		            return {
		                term: params.term
		            }
		        },

		        processResults: function (data, page) {
		            var myResults = [];


		            jQuery.each(data, function (index, item) {
		                myResults.push({
		                    'id': item.id,
		                    'text': item.nombre
		                });
		            });

		            return {
		                results: myResults
		            };
		        }

		    }
		});

		$("#btn_actualizar_vacunas").click(function() {

			let vacunas = $("#select_vacunas").val();

			
			parent.saveVacunas(vacunas)

		});
	
	}
}