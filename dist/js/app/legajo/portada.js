function Portada(config, empleadoId) {

    var parent = this

    this.config = config

    this.empleadoId = empleadoId

	this.data = function () {

		return $.ajax({	'url': parent.config.baseUrlApi + '/' + parent.empleadoId + '/portada',
						'type': 'get',
            	        'dataType': 'json'  
    	});
    }

	this.populate = function (data) {


		$("#portada_nombre_apellido").html(data.nombre + ' ' + data.apellido)
		$("#portada_legajo").html(data.legajo)
		$("#portada_gerencia").html(data.gerencia)
		$("#portada_puesto").html(data.puesto)
		$("#portada_documento").html(data.dni)
		$("#portada_cuil").html(data.cuil)
	}
}