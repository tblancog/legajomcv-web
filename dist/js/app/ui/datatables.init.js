// Settings for a basic abm options
var basicAbmDtOptions = function basicAbmDtOptions(url, id, columns) {
  return {
    ajax: url + id,
    type: 'GET',
    columns: columns,
    order: [['1', 'desc']]
  }
}

// Init datatables config
var dt
$(document).ready(function() {

  dt = $("#dt").DataTable( basicAbmDtOptions(baseUrl, meta.entity, meta.columns) )
})
