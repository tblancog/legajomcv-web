// Metainfo for the form
var meta, id, abmAction, sel2_1, sel2_2

/** Form and service helpers **/
function setUserForm(allpermissions, roles, user){

  if(user){
    setValue('nombre', user.nombre)
    setValue('apellido', user.apellido)
    setValue('userad', user.userad)
    setValue('email', user.email)
    setValue('documento', user.documento)
    setValue('cuil', user.cuil)
    setValue('legajo', user.legajo)
    setValue('sector', user.sector)
    setValue('puesto', user.puesto)
    setValue('gerencia', user.gerencia)
    setValue('matricula', user.matricula)
    setValue('firma', user.firma)
    setEstado(user.deleted_at)
  }

  // Roles
  var dataset = $('#role').data()
  var selected = user ? user.role : []

  role_s2 = createSelect2(
              $('#role'),
              roles,
              dataset.placeholder )
    role_s2.val(selected.id).trigger('change')

  // Read and write permissions
  var dataset = $('#write_permissions').data()
  var selected = user ? user.permissions : []
  sel2_1 = createSelect2(
            $('#write_permissions'),
            allpermissions,
            dataset.placeholder )
  initSelect2(sel2_1, filterPermissions(selected, false))

  var dataset = $('#read_permissions').data()
  var selected = user ? user.permissions : []
  sel2_2 = createSelect2(
            $('#read_permissions'),
            allpermissions,
            dataset.placeholder )
  initSelect2(sel2_2, filterPermissions(selected, true))

}
function filterPermissions(permissions, mode){
  if(!permissions) return []

  return permissions.filter(function(el){
    return el.is_readonly == mode
  })
}
function setValue(key, el){
  if(el){
    $('[name='+ key +']').val(el)
  }
}
function setEstado(state) {
  value = state ? '0' : '1'
  $('input:radio[name=estado]')
    .filter('[value="'+ value +'"]')
    .attr('checked', true)
}

function clearRole(){
  $('input:checkbox#role')
    .iCheck('uncheck')
}
function createSelect2(el, all, placeholder){

  return el.select2({
    data: all,
    placeholder: placeholder,
    allowClear: true,
    dropdownCssClass: 'select2-dropdown'
  })
}
function initSelect2(select2, selected){
  if(selected){
    selected = selected.map(function(el){
      return el.id
    })
  }
  select2.val(selected).trigger('change')
}
function getOptions(url, entity){
  return $.ajax({
    url: url + entity + '/list',
    method: 'GET',
    dataType: 'json'
  })
}
function getEntity(url, entity, id){
  return $.ajax({
    url: url + entity +'/'+id,
    method: 'GET',
    dataType: 'json'
  })
}
function getEntityMeta(id, action) {
  var columns = {
    default: [
      {
        title: "Nombre",
        data: "nombre",
        render: function(data, type, row, meta){
          return renderizeEdit(data, type, row, meta)
        }
      },
      {
        title: "Creado",
        data: "created_at"
      },
      {
        title: "Estado",
        data: "deleted_at",
        render: function(data, type, row, meta) {
          return row.deleted_at ? 'inactivo' : 'activo'
        }
      }]
  }

  switch (id) {
    case 'users':
      meta = {
        header: 'Usuarios',
        title: 'Usuario',
        abm: { create: 'abm/bk_usrs_alta.php', edit: 'abm/bk_usrs_edicion.php' },
        columns: [
          {
            title: "Nombre",
            data: "fullname",
            render: function(data, type, row, meta){
              return renderizeEdit(data, type, row, meta)
            }
          },
          {
            title: "Creado",
            data: "created_at"
          },
          {
            title: "Estado",
            data: "deleted_at",
            render: function(data, type, row, meta) {
              return row.deleted_at ? 'inactivo' : 'activo'
            }
          }]
      }
      break
    case 'antecedentes':
      meta = {
        header: 'Antecedentes Médicos',
        title: 'Antecedente',
        abm: { create: 'abm/bk_alta.php', edit: 'abm/bk_edicion.php' },
        columns: columns.default
      }
      break
    case 'grupos_riesgo':
      meta = {
        header: 'Grupos de Riesgo',
        title: 'Grupo',
        abm: { create: 'abm/bk_grupo_riesgo_alta.php', edit: 'abm/bk_grupo_riesgo_edicion.php' },
        columns: columns.default
      }
      break
    case 'motivos':
      meta = {
        header: 'Motivos de Consulta',
        title: 'Motivo',
        abm: { create: 'abm/bk_alta.php', edit: 'abm/bk_edicion.php' },
        columns: columns.default
      }
      break
    case 'tipos_intervencion':
      meta = {
        header: 'Tipos de intervención',
        title: 'Tipo de Intervención',
        abm: { create: 'abm/bk_alta.php', edit: 'abm/bk_edicion.php' },
        columns: columns.default
      }
      break
    case 'elementos':
      meta = {
        header: 'Elementos Utilizados',
        title: 'Elemento',
        abm: { create: 'abm/bk_alta.php', edit: 'abm/bk_edicion.php' },
        columns: columns.default
      }
      break
    case 'prestadores':
      meta = {
        header: 'Prestadores Médicos',
        title: 'Prestador',
        abm: { create: 'abm/bk_alta.php', edit: 'abm/bk_edicion.php' },
        columns: columns.default
      }
      break
      default:  break
  }
  meta.entity= id
  return meta
}
function getFormMeta(entity, action, id) {
  httpMethod = 'post'
  if(!id ) {
    path= baseUrl + entity
  } else {
    path= baseUrl + entity + '/' + id + '/update'
  }
  return {
    method: httpMethod,
    formAction: path
  }
}

function modalAltaEntity() {


  var promise = $.ajax({'url': meta.abm.create, 
                        'type':'get',
                        'dataType':'html'})

  promise.then(function(res)  {

    
    $('#modal-content').html(res)
    $('#modal-content').modal('show')

  })
}

function modalEditEntity(id) {
  var promise = $.ajax({'url': meta.abm.edit, 
                        'data': {'id':id},
                        'type':'get'
                      })
  promise.then(function(res)  {

    
    $('#modal-content').html(res)
    $('#modal-content').modal('show')
  })
}
function renderizeEdit(data, type, row) {
  return "<a onClick='modalEditEntity(" + row.id + ")' href='javascript:void(0)' class='dt-item' \
         data-id='" + row.id + "'  >" + data + "</a>"
}
function initAjaxSetup(){
  jQuery.ajaxSetup({
    cache: false,
    headers: {"Access-Control-Allow-Origin": "*",
              "Access-Control-Allow-Methods": "GET, POST, PUT, PATCH, DELETE, OPTIONS",
              "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept"
            }
  })
}
var setSubmitEnabled = function(ev){
  var selected =  $(ev.currentTarget).find('option:selected')
  if(selected.length){
    $('#formABM')
      .find('[type=submit]')
      .attr('disabled', false)
  }
}
