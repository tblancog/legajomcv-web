function Alert(accion, elemento, errors) {
  var message, color, icon, alert

  this.render= function(){
    switch(accion){
      case 'alta':
      message = elemento +" Creado"
      color = 'text-green'
      icon = "fa-check"
      break

      case 'edicion':
      message = elemento +" Actualizado"
      color = 'text-green'
      icon = "fa-check"
      break

      case 'error':
      // If there are any errors then list them
      if(errors)  {
        var message = "<ul>"
        $.each(errors, function(idx, value){
          message += "<li>"+ value +"</li>"
        })
        message += "</ul>"
      }
      color = 'text-red'
      icon = "fa-warning"
      break
    }
    return '\
          <div class="alert alert-warning alert-dismissible no-shadow">\
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>\
            <h4 class="'+ color +'"><i class="icon fa '+ icon +'"></i> Aviso</h4>\
            <strong id="msg-custom">'+ message +'</strong><br />\
          </div>\
    '
  }

}
