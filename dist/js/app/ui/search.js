var searchTxt, searchBtn

// Events
$(document).ready(function(){
  // Default select2 mesasges
  $.fn.select2.defaults.defaults['language'].searching = function(){ return 'Buscando..' };
  $.fn.select2.defaults.defaults['language'].inputTooShort = function(){ return 'nombre, usuario ad, dni...' };

  searchTxt = $("[name='buscarEmpleado']")
  searchTxt.select2(
    getSuggestOptions()
  ).on('change', function(ev){
    var selected = $(ev.currentTarget).find("option:selected")

    // Clear form
    clearFormFields()

    // Get selected user by userad
    getUser(selected.val())


  })
})

function setReadonly(fields, value){
  $.each(fields, function(idx, item){
    var element = $('[name='+ item +']');
    if( element.val() != '' ){
      element[0].readOnly = value
    }
  })
}

function getUser(q){
  getUserService(baseUrl, q)
    .done(function(user){

      if(user == 'local'){
        var alert = new Alert("error", meta.title,
                              ['El usuario ya existe en la base local'])
        $("#alert_error").html(alert.render)
          .show()
      }else{
        $("#alert_error").hide()
        setValue('nombre', user.nombre)
        setValue('apellido', user.apellido)
        setValue('userad', user.userad)
        setValue('email', user.email)
        setValue('documento', user.documento)
        setValue('cuil', user.cuil)
        setValue('legajo', user.legajo)
        setValue('sector', user.sector)
        setValue('gerencia', user.gerencia)

        /* If fields: email, documento, cuil or legajo are
         filled then set them readonly off */
        setReadonly(['email', 'documento', 'cuil', 'legajo'], true)
      }


    })
}

function getUserService(url, q){
    return $.get(url + 'users/search/'+q)
}

function clearFormFields(){
  var fields = $('form').find('input, .select2, input:checkbox')
  if(fields.length){
    $.each(fields, function(index, el){
      var el = $(el)[0]
      switch(el.type){
        case 'text':
        $(el).val("")
        break;
        case 'select-multiple':
        $(el).val("").trigger('change')
        break;
        case 'select-one':
        $(el).val("").trigger('change')
        break;
        case 'radio':
        clearRole()
        break;
      }
    });
  }

  // clear errors
  $('.form-group').removeClass('has-error')
  $('.form-control-feedback').hide()
}

function getSuggestOptions(){
  return {
    minimumInputLength: 2,
    placeholder: 'Buscar usuario..',
    ajax: {
        dataType: 'json',
        url: function(term){
          if(!term) return 0
          return baseUrl +'users/suggest/' + term.term
        },
        delay: 250,
        data: function(params) {
            return {
                term: params.term
            }
        },
        processResults: function (data, page) {
          return {
            results: data
          };
        },
    }
  }
}
