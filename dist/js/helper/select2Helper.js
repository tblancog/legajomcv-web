function select2_FromData(el, data, placeholder){
  return el.select2({
    		'data': data,
    		'placeholder': placeholder,
    		'dropdownCssClass': 'select2-dropdown'
  })
}
