function flatpickr_create(elements, options ){
  let default_options = {
    altInput: true,
    locale: 'es'
  }
  $(elements.join(", "))
    .flatpickr(
      Object.assign(default_options, options ))
}
